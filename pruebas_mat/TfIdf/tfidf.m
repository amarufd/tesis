function X = tfidf(X)
%tic;
%disp('Make TF matrix');
X=TF(X);
%disp('Make IDF vector');
I=IDF(X);
largo=size(X, 2);
for j=1:largo
    X(:, j)=X(:, j)*I(j);
  %  disp(['Make TF-IDF Matrix progres row: ',num2str(j),' of ',num2str(largo)]);
end
%toc;