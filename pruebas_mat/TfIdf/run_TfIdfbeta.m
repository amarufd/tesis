%%%%%%%%%%%%%%%%% Uno

clear all;
tic;

eval('load ../../mat/dic-n1_MITD.mat');

dic=dic(2:end,1:end);

dic=tozero(dic);

fpds=sum(dic);

fpds=length(dic)./fpds;

idf=log10(fpds);

seg=toc;
disp(['(n1) Idf en ',num2str(seg),' segundos.']);
%%%%%%%%%%%%%%%%% Dos

clear dic fpds;
tic;

eval('load ../../mat/n1-dic_WandF.mat');

dic=dic(2:end,1:end);

dic=tozero(dic);

fpend=sum(dic');
xy=size(dic);
tf=sparse(xy(1),xy(2));
fpend=fpend';
for i=1:length(fpend)
    tf(i,:)=dic(i,:)/fpend(i);
    if ((i/100-floor(i/100))==0)
        seg=toc;
        disp(['(n1) van ',num2str(i),' del Tf en ',num2str(seg),' segundos.']);
        tic;
    end
end

clear i fpend dic;
seg=toc;
disp(['(n1) Tf en ',num2str(seg),' segundos.']);
% 
% %%%%%%%%%%%%%%%%% tf*idf
% 
tic;
TfIdf=tf;

for i=1:length(idf)
    TfIdf(:,i)=tf(:,i)/idf(i);
    if ((i/100-floor(i/100))==0)
        seg=toc;
        disp(['(n1) van ',num2str(i),' del TfIdf en ',num2str(seg),' segundos.']);
        tic;
    end
end

clear tf idf i;

eval(['save ../../mat/n1-TfIdf.mat' ' TfIdf']);

seg=toc;
disp(['(n1) TfIdf en ',num2str(seg),' segundos.']);
% 
% % TfIdf=sparse(TfIdf);
% % 
% % eval(['save ../docs_amaru/review_polarity/matlab/n3/TfIdf.mat' ' TfIdf']);
% 
% %para ver el sparse completo se hace full(TfIdf)


%%%%%%%%%%%%%%%%% Uno

clear all;
tic;

eval('load ../../mat/dic-n3_MITD.mat');

dic=dic(2:end,1:end);

dic=tozero(dic);

fpds=sum(dic);

fpds=length(dic)./fpds;

idf=log10(fpds);

seg=toc;
disp(['(n3) Idf en ',num2str(seg),' segundos.']);
%%%%%%%%%%%%%%%%% Dos

clear dic fpds;
tic;

eval('load ../../mat/n3-dic_WandF.mat');

dic=dic(2:end,1:end);

dic=tozero(dic);

fpend=sum(dic');
xy=size(dic);
tf=sparse(xy(1),xy(2));
fpend=fpend';
for i=1:length(fpend)
    tf(i,:)=dic(i,:)/fpend(i);
    if ((i/100-floor(i/100))==0)
        seg=toc;
        disp(['(n3) van ',num2str(i),' del Tf en ',num2str(seg),' segundos.']);
        tic;
    end
end

clear i fpend dic;
seg=toc;
disp(['(n3) Tf en ',num2str(seg),' segundos.']);
% 
% %%%%%%%%%%%%%%%%% tf*idf
% 
tic;
TfIdf=tf;

for i=1:length(idf)
    TfIdf(:,i)=tf(:,i)/idf(i);
    if ((i/100-floor(i/100))==0)
        seg=toc;
        disp(['van ',num2str(i),' del TfIdf en ',num2str(seg),' segundos.']);
        tic;
    end
end

clear tf idf i;

eval(['save ../../mat/n3-TfIdf.mat' ' TfIdf']);

seg=toc;
disp(['(n3) TfIdf en ',num2str(seg),' segundos.']);