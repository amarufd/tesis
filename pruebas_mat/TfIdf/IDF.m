function I = IDF(X)
%tic;
[m, n]=size(X);
I=zeros(n, 1);
for j=1:n
    nz=nnz(X(:, j));
    if nz
        I(j)=log(m/nz);
    end
    %disp(j);
end
%toc;