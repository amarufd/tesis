%%%%%%%%%%%%%%%%% Uno

clear all;
tic;

eval('load ../docs_amaru/review_polarity/matlab/n3/dic_MITD.mat');

fpds=sum(dic);

fpds=length(dic)./fpds;

idf=log10(fpds);

seg=toc;
disp(['Idf en ',num2str(seg),' segundos.']);
%%%%%%%%%%%%%%%%% Dos

clear dic fpds;
tic;

eval('load ../docs_amaru/review_polarity/matlab/n3/dic_WandF.mat');

fpend=sum(dic');
xy=size(dic);
tf=sparse(xy(1),xy(2));
fpend=fpend';
for i=1:length(fpend)
    tf(i,1:end)=dic(i,1:end)/fpend(i);
    if ((i/100-floor(i/100))==0)
        seg=toc;
        disp(['van ',num2str(i),' del Tf en ',num2str(seg),' segundos.']);
        tic;
    end
end

clear i fpend dic;
seg=toc;
disp(['Tf en ',num2str(seg),' segundos.']);
% 
% %%%%%%%%%%%%%%%%% tf*idf
% 
tic;
TfIdf=sparse(xy(2),xy(1));

tf=tf';
idf=idf';

for i=1:length(idf)
    TfIdf(i,1:end)=tf(i,1:end)/idf(i);
    if ((i/100-floor(i/100))==0)
        seg=toc;
        disp(['van ',num2str(i),' del TfIdf en ',num2str(seg),' segundos.']);
        tic;
    end
end

clear tf idf i;

TfIdf=TfIdf';

eval(['save ../docs_amaru/review_polarity/matlab/n3/TfIdf.mat' ' TfIdf']);

seg=toc;
disp(['TfIdf en ',num2str(seg),' segundos.']);
% 
% % TfIdf=sparse(TfIdf);
% % 
% % eval(['save ../docs_amaru/review_polarity/matlab/n3/TfIdf.mat' ' TfIdf']);
% 
% %para ver el sparse completo se hace full(TfIdf)