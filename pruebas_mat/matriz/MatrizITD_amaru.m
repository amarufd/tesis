% clear all
% 
% eval('load ../../mat/dic-n_1.mat');
% 
% c=dir('../../n-grama_1/*.mat');
% 
% for x = 1:length(c)
%     tic;
%     eval(['load ../../n-grama_1/' c(x).name] );
%     dic=matriz_inc_ter_doc(dic,texto);
%     seg=toc;
%     disp(['Revisado documento ',num2str(x),' en ',num2str(seg),' segundos.']);
%     
% end
% toc
% 
% % c=dir('../../docs_amaru/review_polarity/matlab/n3/neg/*.mat');
% % tic
% % for x = 1:size(c)
% %     eval(['load ../../docs_amaru/review_polarity/matlab/n3/neg/' c(x).name] );
% %     dic=matriz_inc_ter_doc(dic,texto);
% %     if ((x/100-floor(x/100))==0)
% %         toc
% %         x
% %         tic
% %     end
% % end
% % toc
% 
% eval(['save ../../mat/dic-n1_MITD.mat' ' dic'] );

clear all

eval('load ../../mat/dic-n_3.mat');

c=dir('../../n-grama_3/*.mat');

for x = 1:length(c)
    tic;
    eval(['load ../../n-grama_3/' c(x).name] );
    dic=matriz_inc_ter_doc(dic,texto);
    seg=toc;
    disp(['Revisado documento ',num2str(x),' en ',num2str(seg),' segundos.']);
    
end
toc

% c=dir('../../docs_amaru/review_polarity/matlab/n3/neg/*.mat');
% tic
% for x = 1:size(c)
%     eval(['load ../../docs_amaru/review_polarity/matlab/n3/neg/' c(x).name] );
%     dic=matriz_inc_ter_doc(dic,texto);
%     if ((x/100-floor(x/100))==0)
%         toc
%         x
%         tic
%     end
% end
% toc

eval(['save ../../mat/dic-n3_MITD.mat' ' dic'] );