% OSX: addpath('/Volumes/Oters/amaru/Dropbox/Tesis/test/library','-end')
% Linux: addpath('/home/amaru/Dropbox/Tesis/test/library','-end')
% Linux: addpath('/home/josemontero/Dropbox/Tesis/test/library','-end')
clear all

c=dir('../../n-grama_1/*.mat');

dic={};
tic
for x = 1:size(c)
    tic;
    eval(['load ../../n-grama_1/' c(x).name] );
    if length(texto)>1
        texto=doc_ter_frec_conmat(texto);
        if  exist('../../mn-grama_1/')==0 %#ok<EXIST>
            mkdir('../../mn-grama_1/');
        end
        eval(['save ../../mn-grama_1/' c(x).name, ' texto'] );
    %     if ((x/100-floor(x/100))==0)
    %         toc
    %         x
    %         tic
    %     end
    end
    seg=toc;
    disp(['Revisado documento ',num2str(x),' en ',num2str(seg),' segundos.']);
end
toc

clear all

c=dir('../../n-grama_3/*.mat');

dic={};
tic
for x = 1:size(c)
    tic;
    eval(['load ../../n-grama_3/' c(x).name] );
    if length(texto)>1
        texto=doc_ter_frec_conmat(texto);
        if  exist('../../mn-grama_3/')==0 %#ok<EXIST>
            mkdir('../../mn-grama_3/');
        end
        eval(['save ../../mn-grama_3/' c(x).name, ' texto'] );
    %     if ((x/100-floor(x/100))==0)
    %         toc
    %         x
    %         tic
    %     end
    end
    seg=toc;
    disp(['Revisado documento ',num2str(x),' en ',num2str(seg),' segundos.']);
end
toc

% c=dir('../../docs_amaru/review_polarity/freeling_b/n4/neg/*.mat');
% tic
% for x = 1:size(c)
%     tic;
%     eval(['load ../../docs_amaru/review_polarity/freeling_b/n4/neg/' c(x).name] );
%     if length(texto)>1
%         texto=doc_ter_frec_conmat(texto);
%         eval(['save ../../docs_amaru/review_polarity/matlab/n4/neg/' c(x).name ' texto'] );
%     %     if ((x/100-floor(x/100))==0)
%     %         toc
%     %         x
%     %         tic
%     %     end
%     end
%     seg=toc;
%     disp(['Revisado documento negativo ',num2str(x),' en ',num2str(seg),' segundos.']);
% end
% toc
