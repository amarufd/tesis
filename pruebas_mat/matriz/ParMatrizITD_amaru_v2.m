clear all
matlabpool open local 2
%cd /home/amaru/Dropbox/MatLab/amaru_matlab/Amaru_matlab/
eval('load ../../docs_amaru/review_polarity/matlab/n3/dic_amaru_rp.mat');
cpos=dir('../../docs_amaru/review_polarity/matlab/n3/pos/*.mat');
cneg=dir('../../docs_amaru/review_polarity/matlab/n3/neg/*.mat');
%Matrix{1} para positivos y Matrix{2} para negativos
matrix{1}=sparse(length(cpos),length(dic));
matrix{2}=sparse(length(cneg),length(dic));

%cargar documentos
for x=1:length(cpos)
    eval(['load ../../docs_amaru/review_polarity/matlab/n3/pos/' cpos(x).name]);
    textos{1,x}=texto;
end

for x=1:length(cneg)
    eval(['load ../../docs_amaru/review_polarity/matlab/n3/neg/' cneg(x).name]);
    textos{2,x}=texto;
end

%recorrer documentos
tic; 
parfor par=1:2
if par==1    
    for x = 1:length(cpos)

       for grama=1:length(textos{par,x})
           posdic=find(strcmp(textos{par,x}{grama},dic));
           matrix{par}(x,posdic)=matrix{par}(x,posdic)+textos{par,x}{2,grama};               
       end   
       
       disp(['Revisado documento ',num2str(x),' del hilo ',num2str(par)]);
    end
else
    for x = 1:length(cneg)

       for grama=1:length(textos{par,x})
           posdic=find(strcmp(textos{par,x}{grama},dic));
           matrix{par}(x,posdic)=matrix{par}(x,posdic)+textos{par,x}{2,grama};               
       end   
      
       disp(['Revisado documento ',num2str(x),' del hilo ',num2str(par)]);
    end
end
end
toc;


eval(['save ../../docs_amaru/reviw_polarity/matlab/n3/dic_MITD.mat' ' matrix'] );
matlabpool close