clear all


d1=fileread('../docs_amaru/polarity_html/movie/9997.html');
d2=fileread('../docs_amaru/polarity_html/movie/9998.html');
d3=fileread('../docs_amaru/polarity_html/movie/9999.html');
dic={};

dic=actualiza_dic(dic,d1);
dic=actualiza_dic(dic,d2);
dic=actualiza_dic(dic,d3);

%com_WaF1={};
%com_WaF2={};

com_WaF1=doc_ter_frec(d1);
com_WaF2=doc_ter_frec(d2);
com_WaF3=doc_ter_frec(d3);

dic_FT=dic;
dic_FT=dic_frec_ter(dic_FT,com_WaF1);
dic_FT=dic_frec_ter(dic_FT,com_WaF2);

FT=dic_FT;

FT=FT(2:end,1:end);
a=size(FT);
for i=1:a(1,1)
    for j=1:a(1,2)
        if cellfun('isempty',FT(i,j))
            FT(i,j)={0};
        end
    end
end
FT=cell2mat(FT);
value={};
value=([value; 'neg']);
value=([value; 'pos']);
svmStruct = svmtrain(FT,value,'kernel_function','rbf','rbf_sigma',1);

clear com_WaF1 com_WaF2 com_WaF3 d1 d2 d3 dic dic_FT i j a;