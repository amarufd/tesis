clear all
cd /home/amaru/Dropbox/MatLab/amaru_matlab/Amaru_matlab/
%abrir dic_FT
eval('load ../../docs_amaru/review_polarity/matlab/n3/dic_amaru_rp.mat');
c=[dir('../../docs_amaru/review_polarity/matlab/n3/pos/*.mat'); dir('../../docs_amaru/review_polarity/matlab/n3/neg/*.mat')];
dic2=dic;
clear dic;
dic=sparse(length(c),length(dic2));

c=dir('../../docs_amaru/review_polarity/matlab/n3/pos/*.mat');

for x = 1:length(c) 
    tic;
    eval(['load ../../docs_amaru/review_polarity/matlab/n3/pos/' c(x).name]);    
    for grama=1:length(texto)
        posdic=find(strcmp(texto{grama},dic2));
        dic(x,posdic)=dic(x,posdic)+texto{2,grama};               
    end
    seg=toc;
    disp(['Revisado documento positivo ',num2str(x),' en ',num2str(seg),' segundos.']);
end

pos=length(c);

c=dir('../../docs_amaru/review_polarity/matlab/n3/neg/*.mat');

for x = pos+1:pos+length(c)
    tic;
    eval(['load ../../docs_amaru/review_polarity/matlab/n3/neg/' c(x).name] );
    for grama=1:length(texto)
        posdic=find(strcmp(texto{grama},dic2));
        dic(x,posdic)=dic(x,posdic)+texto{2,grama};               
    end
    seg=toc;
    disp(['Revisado documento negativo ',num2str(x),' en ',num2str(seg),' segundos.']);
end


eval(['save ../../docs_amaru/review_polarity/matlab/n3/dic_MITD.mat' ' dic'] );