clear all

value={};
value=([value; 'valor']);

c=dir('../docs_amaru/review_polarity/matlab/pos/*.mat');

tic
for x = 1:size(c)
    eval(['load ../docs_amaru/review_polarity/matlab/pos/' c(x).name] );
    if(~(isempty(texto)))
        value=([value; 1]);
    end
    if ((x/100-floor(x/100))==0)
        toc
        x
        tic
    end
end
toc

c=dir('../docs_amaru/review_polarity/matlab/neg/*.mat');
tic
for x = 1:size(c)
    eval(['load ../docs_amaru/review_polarity/matlab/neg/' c(x).name] );
    if(~(isempty(texto)))
        value=([value; 2]);
    end
    
    if ((x/100-floor(x/100))==0)
        toc
        x
        tic
    end
end
toc

eval(['save ../docs_amaru/review_polarity/matlab/value.mat' ' value'] );