clear all
for n = 4 : 5

%cd /home/amaru/Dropbox/MatLab/amaru_matlab/Amaru_matlab/
%abrir dic_FT
eval(['load ../../docs_amaru/review_polarity/matlab/n',num2str(n),'/dic_amaru_rp.mat']);
c=[dir(['../../docs_amaru/review_polarity/matlab/n',num2str(n),'/pos/*.mat']); dir(['../../docs_amaru/review_polarity/matlab/n',num2str(n),'/neg/*.mat'])];
dic2=dic;
clear dic;
dic=sparse(length(c),length(dic2));
dicw=sparse(length(c),length(dic2));

c=dir(['../../docs_amaru/review_polarity/matlab/n',num2str(n),'/pos/*.mat']);

for x = 1:length(c) 
    tic;
    eval(['load ../../docs_amaru/review_polarity/matlab/n',num2str(n),'/pos/' c(x).name]);    
    for grama=1:length(texto)
        posdic=find(strcmp(texto{grama},dic2));
        dic(x,posdic)=dic(x,posdic)+texto{2,grama};
        dicw(x,posdic)=dicw(x,posdic)+1; 
    end
    seg=toc;
    disp(['n',num2str(n),' - Revisado documento positivo ',num2str(x),' en ',num2str(seg),' segundos.']);
end

pos=length(c);

c=dir(['../../docs_amaru/review_polarity/matlab/n',num2str(n),'/neg/*.mat']);

for x = pos+1:pos+length(c)
    tic;
    eval(['load ../../docs_amaru/review_polarity/matlab/n',num2str(n),'/neg/' c(x-pos).name] );
    for grama=1:length(texto)
        posdic=find(strcmp(texto{grama},dic2));
        dic(x,posdic)=dic(x,posdic)+texto{2,grama};
        dicw(x,posdic)=dicw(x,posdic)+1; 
    end
    seg=toc;
    disp(['n',num2str(n),' - Revisado documento negativo ',num2str(x),' en ',num2str(seg),' segundos.']);
end


eval(['save ../../docs_amaru/review_polarity/matlab/n',num2str(n),'/dic_MITD.mat',' dic'] );
clear dic;
dic=dicw;
eval(['save ../../docs_amaru/review_polarity/matlab/n',num2str(n),'/dic_WandF.mat' ' dic'] );
clear dic dicw c texto posdic seg x dic2 grama;
end