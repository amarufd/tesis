%OLD DATA(a->1a72 b->73a189 c(emol)->189a215 c(thecl)->216a240)
%a(afp)->1a299 b(ap)->300a598 c(emol)->599a698 c(thecl)->699a798

clear all

n=1; %set n-grama
Nprueba='Alpha A';
g=num2str(n);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% Variables For %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
inicio=2;
plus=0.001;
fin=4;



%    for j=-100:100
%    for j=1:0.01:3
%    for -10:0.2:10


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% temp=fopen(['n',g,' Resultados ',Nprueba,'.txt'],'w'); 
%for n=4:5

    tic;
    disp('Loading...');


    eval(['load ../mat/n',g,'-TfIdf.mat']);
    TfIdf=full(TfIdf);
    eval('load ../mat/value.mat');
    value=cell2mat(value);
    %TfIdf=full(TfIdf);
    xdata =[TfIdf(1:299,1:end);TfIdf(599:698,1:end)];
    xgroup =[value(1:299);value(1:100)];

    ydata = [TfIdf(300:598,1:end);TfIdf(699:798,1:end)];
    ygroup =[value(300:598);value(301:400)];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%% CALCULsalvaO DE DATOS %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    sizeB=num2str((151-131)+1);
    sizeA=num2str((71-51)+1);
    x=num2str(size(xgroup,1));
    y=num2str(size(ygroup,1));
    %fprintf(temp,'The ngrama is: %s      Training with: %s      classify with: %s      Grupo A: %s      Grupo B: %s\n', g, x, y, sizeA, sizeB);
    disp(['The ngrama is: ',g,'   -   Training with: ',x,'   -   classify with: ',y]);
    clear x y g;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    seg=toc;
    disp(['Termino de cargar datos en ',num2str(seg),' segundos.']);
%     tic;
    
   [xfs xI]=cfs(xdata, xgroup);
   [yfs yI]=cfs(ydata, ygroup);
    
    
    eval('save cfsB.mat xfs xI yfs yI');
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
%     sig={};
%     b=0; %#ok<NASGU>
%     
%     
%     for j=inicio:plus:fin
%         disp(['Start training with sigma 10^',num2str(j)]);
% 
%         svmStruct = svmtrain(xdata,group,'kernel_function','rbf','rbf_sigma',10^(j));
% 
%         seg=toc;
%         disp(['Start classify, and the time for training is ',num2str(seg),' in seconds.']);
%         tic;
% 
%         species1 = svmclassify(svmStruct,txdata);
% 
%         seg=toc;
%         disp(['Start to calculate the accuracy, and the time for classify is ',num2str(seg),' in seconds.']);
%         tic;
% 
%         a=0;
%         pos=0;
%         neg=0;
%         for i=1:length(tgroup)
%             if(tgroup(i)==species1(i))
%                 if tgroup(i)==1
%                     pos=pos+1;
%                 elseif tgroup(i)==2
%                     neg=neg+1;
%                 end
%                 a=a+1;
%             end
%         end
%         %if a>=b
%             b=a;
%             fprintf(temp,'sigma: %d      aciertos: %d\n pos: %d  -  neg: %d\n', j, b, pos, neg);
%             sig=([sig; j b]);
%         %end
% 
%         seg=toc;
%         disp(['finish to calculate the accuracy in ',num2str(seg),' seconds.']);
%         tic;
% 
%     end
% 
%     seg=toc;
%     disp(['End n',num2str(n),' in ',num2str(seg),' seconds.']);
% 
%     clear i j b a TfIdf value group seg species1 svmStruct tgroup txdata xdata pos neg;
% %end
% 
% fclose(temp);