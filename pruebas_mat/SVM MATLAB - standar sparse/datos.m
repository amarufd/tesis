function [x,y,m,n,nc,labels] = datos(problemName)
%lectura y normalizaci�n de dataset identificada con nombre 'problemName'
%x resulta ser una matriz con 1 ejemplo por columna
%y resulta ser una matriz con 1 label por columna
%m es el n�mero total de ejemplos
%n es el n�me4ro de caracter�sticas o dimensi�n de los datos


if strcmp(problemName, 'appAstroparticle')
    fprintf('reading  points from the appAstroparticle problem ...\n');
    data = load('../data/appAstroparticle-shuffled-(500).data'); 
    m = size(data,1); %n�mero total de ejemplos
    n = 4; %n�mero de caracter�sticas
    x = data(:,1:n); 
    y = data(:,n+1);   
    
    nc=2;
    labels = [1 2];
    
elseif strcmp(problemName, 'appAstroparticle-80')
    fprintf('reading  points from the cod-rna-80 problem ...\n');
    data = load('../data/appAstroparticle-shuffled-(500).data'); 
    m = size(data,1)*0.8; %n�mero total de ejemplos
    n = 4; %n�mero de caracter�sticas
    x = data(1:m,1:n); 
    y = data(1:m,n+1);   
    
    nc=2;
    labels = [1 2];

elseif strcmp(problemName, 'cod-rna')
    fprintf('reading  points from the cod-rna problem ...\n');
    data = load('../data/cod-rna-shuffled-(500).data'); 
    m = size(data,1); %n�mero total de ejemplos
    n = 8; %n�mero de caracter�sticas
    x = data(:,1:n); 
    y = data(:,n+1);   
    
    nc=2;
    labels = [1 2];
    
elseif strcmp(problemName, 'cod-rna-80')
    fprintf('reading  points from the cod-rna-80 problem ...\n');
    data = load('../data/cod-rna-shuffled-(500).data'); 
    m = size(data,1)*0.8; %n�mero total de ejemplos
    n = 8; %n�mero de caracter�sticas
    x = data(1:m,1:n); 
    y = data(1:m,n+1);   
    
    nc=2;
    labels = [1 2];

elseif strcmp(problemName, 'iris')
    fprintf('reading iris dataset ...\n');
    
    %Iris Plants Data (4 numeric , 3 classes)
        %Iris-setosa:  class 1
        %Iris-versicolor: class 2
        %Iris-virginica: class 3
    data = load('../data/iris-shuffled.data'); 
    m = size(data,1); %n�mero total de ejemplos
    n = 4; %n�mero de caracter�sticas
    
    x = data(:,1:n);
    y = data(:,n+1);
    nc=3;  %posible numero de clase
    labels = [0 1 2];
    
    
elseif strcmp(problemName, 'vehicles')
    
    fprintf('reading UCI vehicle silhouettes dataset ...\n');
    
    %vehicle silhouettes (18 attributes, 4 classes)    
        %class 1 (van) 199 
        %class 2 (saab) 217
        %class 3 (bus) 218
        %class 4 (opel) 212
        %total: 846 instances 
        
    m = 846; %n�mero total de ejemplos
    n = 18; %n�mero de caracter�sticas
    data = load('../data/vehicles-shuffled.data'); 
    x = data(:,1:n); 
    y = data(:,19);
    nc=4;
    labels = [1 2 3 4];
    
elseif strcmp(problemName, 'soybean-small')
    
    fprintf('reading UCI soybean-small dataset ...\n');
    %soy-bean small dataset (35 attributes, 4 classes)    
        
    m = 47; %n�mero total de ejemplos
    n = 35; %n�mero de caracter�sticas
    data = load('../data/soybean-small-shuffled.data'); 
    x = data(:,1:n); 
    y = data(:,n+1);
    nc=4;
    labels = [1 2 3 4];
    
elseif strcmp(problemName, 'wine')
    
    fprintf('reading wine dataset ...\n');
    
    %Wine Recognition(13 attributes, 3 classes)    
        %class 1 59
        %class 2 71
        %class 3 48 
    data = load('../data/wine-shuffled.data'); 
    m = size(data,1); %n�mero total de ejemplos
    n = 13; %n�mero de caracter�sticas
    
    x = data(:,2:n+1); 
    y = data(:,1);
    nc=2;
    labels = [1 2];
    
% elseif strcmp(problemName, 'waveform22')
%     fprintf('reading 500 points from the waveform22 problem ...\n');
%     m = 500; %n�mero total de ejemplos
%     n = 21; %n�mero de caracter�sticas
%     data = load('waveform22-500points.data'); 
%     x = data(:,1:n); 
%     y = data(:,n+1);
%     nc=3;
%     labels = [1 2 3];
%   

elseif strcmp(problemName, 'waveform22')
    fprintf('reading  points from the waveform22 problem ...\n');
    data = load('../data/waveform22-shuffled-clases1-2(500).data'); 
    m = size(data,1); %n�mero total de ejemplos
    n = 21; %n�mero de caracter�sticas
    x = data(:,1:n); 
    y = data(:,n+1);   
    %nc=3;
    %labels = [1 2 3]; modificado
    nc=2;
    labels = [1 2];
    
elseif strcmp(problemName, 'waveform22-80')
    fprintf('reading  points from the waveform22 problem ...\n');
    data = load('../data/waveform22-shuffled-clases1-2(500).data'); 
    m = size(data,1)*0.8; %n�mero total de ejemplos
    n = 21; %n�mero de caracter�sticas
    x = data(1:m,1:n); 
    y = data(1:m,n+1);   
    %nc=3;
    %labels = [1 2 3]; modificado
    nc=2;
    labels = [1 2];

elseif strcmp(problemName, 'waveform41')
    fprintf('reading  points from the waveform41 problem ...\n');
    data = load('../data/waveform41-shuffled-clases1-2(500).data'); 
    m = size(data,1); %n�mero total de ejemplos
    n = 40; %n�mero de caracter�sticas
    x = data(:,1:n); 
    y = data(:,n+1);   
    %nc=3;
    %labels = [1 2 3]; modificado
    nc=2;
    labels = [1 2];
elseif strcmp(problemName, 'waveform41-80')
    fprintf('reading  points from the waveform41 problem ...\n');
    data = load('../data/waveform41-shuffled-clases1-2(1000).data'); 
    m = size(data,1)*0.8; %n�mero total de ejemplos
    n = 40; %n�mero de caracter�sticas
    x = data(1:m,1:n); 
    y = data(1:m,n+1);   
    %nc=3;
    %labels = [1 2 3]; modificado
    nc=2;
    labels = [1 2];

elseif strcmp(problemName, 'waveform41-20')
    fprintf('reading  points from the waveform41 problem ...\n');
    data = load('../data/waveform41-shuffled-clases1-2(1000).data');
    start=(size(data,1)*0.8);
    m = size(data,1)*0.2; %n�mero total de ejemplos
    n = 40; %n�mero de caracter�sticas
    x = data(start+1:(start+m),1:n); 
    y = data(start+1:(start+m),n+1);   
    %nc=3;
    %labels = [1 2 3]; modificado
    nc=2;
    labels = [1 2];
    
elseif strcmp(problemName, 'vowel')
    fprintf('UCI vowel recognition problem ...\n');
    m = 990; %n�mero total de ejemplos
    n = 2; %n�mero de caracter�sticas
    data = load('../data/vowel-shuffled.data'); 
    x = data(:,1:n); 
    y = data(:,n+1); 
    nc=2;
    labels = [1 2];

elseif strcmp(problemName, 'ecoli')
    fprintf('UCI ecoli problem: protein localization sites ...\n');
    m = 150; %n�mero total de ejemplos
    n = 7; %n�mero de caracter�sticas
    data = load('../data/ecoli-shuffled.data'); 
    x = data(:,1:n); %modificacion = q los otros
    y = data(:,n+1); 
    nc=2;
    labels = [1 2]; 
    
 elseif strcmp(problemName, 'thyroid')
     fprintf('reading thyroid-disease-ann-train ...\n');
     data = load('data\thyroid-1-2(1000).data'); 
     
     m = size(data,1); %n�mero total de ejemplos
     n = 21; %n�mero de caracter�sticas
     
     x = data(:,1:n); 
     y = data(:,n+1); 
     nc=2;
     labels = [1 2];
   elseif strcmp(problemName, 'magic')
     fprintf('reading magic 04 ...\n');
     data = load('data\magic04-shuffled-clases1-2(500).data'); 
     
     m = size(data,1); %n�mero total de ejemplos
     n = 10; %n�mero de caracter�sticas
     
     x = data(:,1:n); 
     y = data(:,n+1); 
     nc=2;
     labels = [1 2];
     fprintf('reading magic 04 listo...\n');
else 
    error('this name is not recognized ...\n');
end
 
stdevs = std(x);
meds = mean(x);
x = x - repmat(meds,m,1); %corregimos media
stdevs(find(stdevs==0)) = 1;
x = x*diag((stdevs.^-1)); %corregimos escala
x = x'; %cada columna es 1 ejemplo
y = y';  %cada columna es una etiqueta

% datat = [x;y];
% datat = datat(:,randperm(m));
% datat = datat(:,randperm(m));
% x = datat(1:n,:);
% y = datat(n+1,:);
