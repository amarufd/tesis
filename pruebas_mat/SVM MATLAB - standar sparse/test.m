function [aciertos,SV,duracion,i,out0,outn]=test(problemName,kernel,sigma,eta,S,porc,porc_it,it,tipo)%porc  0-1
   inicio=clock; 
   [x,y,m,n,nclases,labels] = datos(problemName);
   
   lim=m*porc;
   lim0=lim*porc_it;
   Xporc=x(:,(1:lim));
   Yporc=y(1:lim);
   %------------------------------------------------------------
   ps0  =  zeros(size(Xporc(:,(1:lim0)),2),size(Xporc(:,(1:lim0)),2));	% se genera el kernel total
   ps0=svmker((Xporc(:,(1:lim0)))',(Xporc(:,(1:lim0)))',kernel,sigma);

   ybin0=Yporc(1:lim0);
   ybin0(find(Yporc(1:lim0)))=-1;
   H0 =(ps0).*(ybin0'*ybin0);
   %------------------------------------------------------------ 
   ps  =  zeros(size(Xporc,2),size(Xporc,2));	% se genera el kernel total
   ps=svmker(Xporc',Xporc',kernel,sigma);

   ybin=Yporc;
   ybin(find(Yporc>1))=-1;
   H =(ps).*(ybin'*ybin);
   
   e=ones(size(Yporc));
   
   beta=zeros(size(ybin0));
   [ybin0 ,res0 ,c0 ,b0]=SVMprop(Xporc,ybin0,ps0,H0,eta,beta,[]);
  
   %evaluando B_i
   beta=zeros(size(ybin));
   indbeta=find(res0.*ybin0<S);
   beta(indbeta)=eta;
   if(strcmp(tipo,'nolineal'))
        indbeta=find(S<=res0.*ybin0 & res0.*ybin0<1);
        beta(indbeta)=-eta*((res0(indbeta).*ybin0(indbeta)-S^2+S-1)/(S-1)^2);
   end
   c=zeros(size(ybin0));
   indC=find(c0~=0);
   
   c(indC)=c0(indC);
   
   out0=sum(beta==eta);
   %cccp
   for i=1:it,
        [ybin, res, c, b]=SVMprop(Xporc,ybin,ps,H,eta,beta,c);
        beta1=beta;
       if(i==1), val1=res.*ybin; end
        beta=zeros(size(ybin));
        
        %evaluando B_i
        indbeta=find(res.*ybin<S);
        beta(indbeta)=eta;
        if(strcmp(tipo,'nolineal'))
            indbeta=find(S<=res.*ybin & res.*ybin<1);
            beta(indbeta)=-eta*((res(indbeta).*ybin(indbeta)-S^2+S-1)/(S-1)^2);
        end
        if(norm((beta1+beta)/2,2)==0 || norm(beta1-beta,2)==0)
            tol=0
        else
            tol=norm(beta1-beta,2)/norm((beta1+beta)/2,2)
        end
        i        
        if (tol<2e-1), break;     end
        
   end
   if(it==0)
       res=res0;
       ybin=ybin0;
       c=c0;
       b=b0;
   end
   %valf=res.*ybin;
   %fprintf('entre 1 y s\n');
   %sum(S<=res.*ybin & res.*ybin<1)
   %fprintf('\n\nmenores a S\n');
   %sum(res.*ybin<S)
   %fprintf('\n\n iguales o mayores a 1\n');
   %sum(res.*ybin>=1)
   
   SV=sum(c~=0);
   outn=sum(beta==eta);
   xTest=x(:,lim+1:m);

   for k=1:length(y(lim+1:m)),
        for indRaices=1:nclases,

            cRaiz=c(find(y(1:lim)==labels(indRaices)));
            xRaiz=Xporc(:,find(y(1:lim)==labels(indRaices)));
                                       %fila
                               
            solTest(indRaices,k)=cRaiz'*svmker(xTest(:,k)',xRaiz',kernel,sigma)'+b;
        end
    end
    
    [Ytest Itest]=max(solTest);
    mistTest = Itest==y(lim+1:m);
    aciertos =(1- sum(mistTest)/(length(y(lim+1:m))))*100,;
    
    duracion=etime(clock,inicio)
end