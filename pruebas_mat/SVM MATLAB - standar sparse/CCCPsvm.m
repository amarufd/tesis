function [ybin c res beta beta1]=CCCPsvm(BD,kernel,kerneloption,eta,it)
    %preparacion
    [x,y,m,n,nclases,labels] = datos(BD);

    lim=m*0.8;
    Xporc=x(:,(1:lim));
    Yporc=y(1:lim);

    ps  =  zeros(size(Xporc,2),size(Xporc,2));	% se genera el kernel total
    ps=svmker(Xporc',Xporc',kernel,kerneloption);

    ybin=Yporc;
    ybin(find(Yporc>1))=-1;
    H =(ps).*(ybin'*ybin);

    e=ones(size(Yporc));

    beta=zeros(size(ybin));
    
    %convexa
    [ybin ,res ,c ,b]=SVMprop(Xporc,ybin,ps,H,eta,beta,[]);
   
    %evaluando B_i
   % indbeta=find(res.*ybin<=-0.001);
   % beta(indbeta)=eta;
   % indbeta=find((res.*ybin>0) & (res.*ybin<=1));
    %beta(indbeta)=eta*(1-res(indbeta).*ybin(indbeta));
  % c
  % res
  %  sum(beta>0)
   
    %concava
   % for i=1:it,
    %    [ybin, res, c, b]=SVMprop(Xporc,ybin,ps,H,eta,beta,c);
     %   beta1=beta;
        
      %  beta=zeros(size(ybin));
        
       % indbeta=find(res.*ybin<=-0.001);
     %   beta(indbeta)=eta;
      %  indbeta=find((res.*ybin>0) & (res.*ybin<=1));
       % beta(indbeta)=eta*(1-res(indbeta).*ybin(indbeta));
        
      %  tol=norm(beta1-beta,2)/norm((beta1+beta)/2,2)
       % i
      %  if (tol) break;     end
        
   % end
    %   b 
    %testeo  
    
   SV=sum(c~=0)
 
   xTest=x(:,lim+1:m);

   for k=1:length(y(lim+1:m)),
        for indRaices=1:nclases,

            cRaiz=c(find(y(1:lim)==labels(indRaices)));
            xRaiz=Xporc(:,find(y(1:lim)==labels(indRaices)));
                                       %fila
                                 size(cRaiz)
    size(svmker(xTest(:,k)',xRaiz',kernel,kerneloption))
            solTest(indRaices,k)=cRaiz'*svmker(xTest(:,k)',xRaiz',kernel,kerneloption)'+b;
        end
    end
    
    [Ytest Itest]=max(solTest);
    mistTest = Itest==y(lim+1:m);
    aciertos = sum(mistTest)/(length(y(lim+1:m)));
   
end


