[x,y,m,n,nclases,labels] = datos('waveform22');
   eta=10;
   lim=m*0.8;
   Xporc=x(:,(1:lim));
   Yporc=y(1:lim);
   
   ps  =  zeros(size(Xporc,2),size(Xporc,2));	% se genera el kernel total
   ps=svmker(Xporc',Xporc','gaussian',100);

   ybin=Yporc;
   ybin(find(Yporc>1))=-1;
   H =(ps).*(ybin'*ybin);

   e=ones(size(Yporc));
   
   beta=zeros(size(ybin));
   [ybin ,res1 ,c1 ,b1]=SVMprop(Xporc,ybin,ps,H,eta,beta,[]);
  
   H1 =(ps+1).*(ybin'*ybin);
   
   options = optimset('LargeScale','off','MaxIter',5000);
[c2]=quadprog(H1,-e,[],[],ybin,0,-beta',eta*e'-beta',[],options);
b2=((c2'.*ybin)*ps*c2)/2;
res2=(ybin.*c2')*ps+b2;
[c]=quadprog(H1,-e,[],[],[],[],-beta',eta*e'-beta',[],options);


b=ybin*c;
res=(ybin.*c')*ps+b;

clear x y m n nclases labels beta  e options Xporc Yporc eta lim  ;