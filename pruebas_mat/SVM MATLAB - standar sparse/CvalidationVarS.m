%Sigma=[5e2,1e2,5e1,1e1,5];
S=[0,-0.2,-0.4,-0.6,-0.8,-1,-5,-10,-100];
%eta=[1e2,1e1,1,1e-1,1e-2,1e-3,1e-4];
paresSigEta=[5e2,1e-1;1e2,1e2;5e1,1e-2;5e1,1e-3;5e1,1e-4;1e1,1e-2;1e1,1e-3;1e1,1e-4];
etiquetas=['resultado ','           res x clase         ','               tpo script    tpo solver     tpo mean solv','        sigma ', '                eta','                   S','          iter si=1 no=0'];
vtest=zeros(length(paresSigEta)*length(S),10);
mostrar=0;
k=10;

BD='waveform22-80';dat=500;
[arch,error]=fopen('resultados/datos500propWaveform22-80-varS.dat','w'); %ojo aca

if (arch > -1),
fprintf('COMIENZA CROSS VALIDATION\n\n');
p=1;

fprintf(arch,'Cross Validation  %s  %d datos \n',BD,dat);
fprintf(arch,'%s\n',etiquetas);

inicio=clock;
 for i=1:length(paresSigEta),
       % for z=1:length(eta),
            for j=1:length(S),

                [resultado perfclase tiempos iter]=CrossValAllDistances_beta(BD,k,paresSigEta(i,2),S(j),'gaussian',paresSigEta(i,1), mostrar);

                vtest(p,1)=resultado;
                vtest(p,2)=perfclase(1);
                vtest(p,3)=perfclase(2);
                vtest(p,4)=tiempos(1);
                vtest(p,5)=tiempos(2);
                vtest(p,6)=tiempos(3);
                vtest(p,7)=paresSigEta(i,1);
                vtest(p,8)=paresSigEta(i,2);
                vtest(p,9)=S(j);
                if(iter), 
                    vtest(p,10)=1;
                else
                    vtest(p,10)=0;
                end
                
                
                fprintf(arch,'%f   [   %f   -   %f  ]   [ %f   -   %f   -   %f   ]   %f   -   %f   -   %f   -   %f ',vtest(p,:));
                fprintf(arch,'\n');
                p=p+1;
            end
        %end
 end
    etiquetas
    vtest
    
    tiempo=etime(clock,inicio);
    fprintf(arch,'\nTERMINA CROSS VALIDATION\n duracion total= %f\n',tiempo);
    fclose(arch);
    fprintf('\nTERMINA CROSS VALIDATION\nduracion total= %f\n',tiempo);
else
    disp('El archivo no pudo crear');
end
