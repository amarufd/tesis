function [aciertos,SV,i,tiempo]=test_sinO(problemName,kernel,sigma,eta,S,porc,porc_it,it)%porc  0-1
    inicio=clock;
   [x,y,m,n,nclases,labels] = datos(problemName);
   yaux=y;
   lim=m*porc;
   lim0=lim*porc_it;
   Xporc=x(:,(1:lim));
   Yporc=y(1:lim);
   %------------------------------------------------------------
   ps0  =  zeros(size(Xporc(:,(1:lim0)),2),size(Xporc(:,(1:lim0)),2));	% se genera el kernel total
   ps0=svmker((Xporc(:,(1:lim0)))',(Xporc(:,(1:lim0)))',kernel,sigma);

   ybin0=Yporc(1:lim0);
   ybin0(find(Yporc(1:lim0)>1))=-1;
   H0 =(ps0).*(ybin0'*ybin0);
   %------------------------------------------------------------ 
     
   
   
   beta=zeros(size(ybin0));
   [ybin0 ,res0 ,c0 ,b0]=SVMprop(Xporc,ybin0,ps0,H0,eta,beta,[]);
  %-----------------------------------------------------------------
   
   
   indSc=find(res0.*ybin0<S);
   indS=find(res0.*ybin0>=S);
   
  
   c=zeros(size(Xporc)-size(indSc));
  % indC=find(c0~=0 & res0.*ybin0>=S);
   
   c(indS)=c0(indS);
  %------------------------------------------------------------------------
   beta=zeros(size(Yporc));
   indbeta=find(S<=res0.*ybin0 & res0.*ybin0<1);
   beta(indbeta)=-eta*((res0(indbeta).*ybin0(indbeta)-S^2+S-1)/(S-1)^2);
   beta(indSc)=[];
   %-------------------------------------------------
   Xporc(:,indSc)=[];
   Yporc(:,indSc)=[];
   ps  =  zeros(size(Xporc,2),size(Xporc,2));	% se genera el kernel total
   ps=svmker(Xporc',Xporc',kernel,sigma);

   ybin=Yporc;
   ybin(find(Yporc>1))=-1;
   H =(ps).*(ybin'*ybin);
  %------------------------------------------------------------------------
     
   
   %----------------------------------------------------------------------
  y(indSc)=[];
   
   
   
   
   for i=1:it,
        
        
        [ybin, res, c, b]=SVMprop(Xporc,ybin,ps,H,eta,beta,c);
      beta1=beta;
       beta=zeros(size(ybin));
       indbeta=find(S<=res.*ybin & res.*ybin<1);
       beta(indbeta)=-eta*((res(indbeta).*ybin(indbeta)-S^2+S-1)/(S-1)^2);

       %-------------------------------------------------
        
        indSc=find(res.*ybin<S)
       % indS=find(res.*ybin>=S);
          
        c0=c;
        c(indSc)=[];
        %------------------------------------
        beta(indSc)=[];
        beta1(indSc)=[];
        Yporc(indSc)=[];
        ybin(indSc)=[];
        Xporc(:,indSc)=[];
        %------------------------------------
       
        y(indSc)=[];
       
        H(:,indSc)=[];
        H(indSc,:)=[];
        
        ps(:,indSc)=[];
        ps(indSc,:)=[];
        %------------------------------------
        
        if(norm((beta1+beta)/2,2)==0 || norm(beta1-beta,2)==0)
            tol=0
        else
            tol=norm(beta1-beta,2)/norm((beta1+beta)/2,2)
        end
        i        
        if ((tol<1e-4) && (length(c)==length(c0))), break;     end     
                   
        
        
   end
   beta
   beta1
   
   outn=sum(beta==eta);
   
   
   SV=sum(c~=0);
   
    
   
   xTest=x(:,lim+1:m);

   for k=1:length(yaux(lim+1:m)),
        for indRaices=1:nclases,

            cRaiz=c(find(y(1:length(c))==labels(indRaices)));
            xRaiz=Xporc(:,find(y(1:length(c))==labels(indRaices)));
                                       %fila
                                       
                                      
            solTest(indRaices,k)=cRaiz'*svmker(xTest(:,k)',xRaiz',kernel,sigma)'+b;
        end
    end
    
    [Ytest Itest]=max(solTest);
    mistTest = Itest==yaux(lim+1:m);
    aciertos = sum(mistTest)/(length(yaux(lim+1:m)));
   

    tiempo=etime(clock,inicio);

    
end
    
    
    

