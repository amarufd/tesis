function K=svmker(xi,xj,tipokernel,param)
% K=svmker(xi,xj,tipokernel,param)
%funcion que implementa un kernel donde xi,xj son los vectores de entrada
% del kernel tipokernel representa el kernel especifico y param es un
% valor q determina algun parametro del kernel, en el caso de gaussian es
% el sigma. K es el kernel resulatnte
[sxi1 sxi2]=size(xi);
[sxj1 sxj2]=size(xj);
norm2 = sparse(sxi1,sxj1);
switch tipokernel
    case 'gaussian'
    for k1=1:sxi1,      
        for k2=1:sxj1,
            norm2(k1,k2)=(xi(k1,:)-xj(k2,:))*(xi(k1,:)-xj(k2,:))';
        end
    end

    sigma=param;
    K=exp(-norm2/sigma);

    case 'nn'
        slope = param{1};
        theta = param{2};
        for k1=1:sxi1,
            for k2=1:sxj1,
                norm2(k1,k2)= (slope*(xi(k1,:)*xj(k2,:)'))+ theta;
            end
        end

        K=tanh(norm2);
        
    case 'poly'
        d = param;
        for k1=1:sxi1,
            for k2=1:sxj1,
                norm2(k1,k2)= (xi(k1,:)*xj(k2,:)')^d;
            end
        end

        K=tanh(norm2);

end
