function [perfTotal perfClase tiempos OutL SV numit] = CrossValsinO(problemName,k,eta,S,kernel,kerneloption,showTraining,porc_ini,maxit)
%  determina error de validaci�n cruzada para el modelo complex svm para N clases
%   salida
%   perfTotal: performance de validacion cruzada considerando todas las clases
%   perfClase: cellarray que contiene las performances por cada clase
%   perfClase{i}  es la performance en la clase i
%   tiempos: cellarray con estad�sticas de tpo de ejecuci�n
%   tiempos{1}: tiempo total de ejecuci�n del script
%   tiempos{2}: tiempo total de bucle de llamadas a solver   
%   tiempos{3}: tiempo medio de llamadas a solver
%
%   par�metros:
%   problemName (string) indica el nombre del dataset o problema a usar
%   opciones: (3-class) 'iris', 'wine', 'waveform22-500points', 'waveform22-1000points', 'waveform41-500points', 'waveform41-1000points','thyroid-disease-ann-train'
%   k (integer) indica el n�mero de partes en las cuales dividir el dataset
%   eta (0< real <1) relajaci�n de la hard svm
%   kernel (string) especifica el tipo de kernel: soportado solo 'gaussian'
%   kerneloption (real) par�metro del kernel: bandwidth

inicio = clock;

%Extraccion de la data y generaci�n de los CV bins
[xt,yt,m,n,nclases,labels] = datos(problemName); %lectura de las matrices de datos
nbloque = floor(m /k);



for i=1:k-1
    bloqueX{i} = xt(:,(i-1)*nbloque+1:i*nbloque);
    bloqueY{i} = yt(:,(i-1)*nbloque+1:i*nbloque);
end
bloqueX{k} = xt(:,(k-1)*nbloque+1:m);
bloqueY{k} = yt(:,(k-1)*nbloque+1:m);

for i=1:k  
    crossTrainingX{i} = [];
    crossTrainingY{i} = [];
    for j=1:k
        if i ~= j
            crossTrainingX{i} = [crossTrainingX{i} bloqueX{j}];
            crossTrainingY{i} = [crossTrainingY{i} bloqueY{j}];
        end
    end
end

%evaluaci�n del error CV
perf = [];

for l=1:nclases
    perfxClase{l} = [];
end
SV=zeros(1,k);
OutL=zeros(1,k);
numit=zeros(1,k);
for i=1:k
    inicioSolver = clock;
    x = []; y =[]; xTest = []; yTest = [];
    x = crossTrainingX{i};
    y = crossTrainingY{i};
    xTest = bloqueX{i};
    yTest = bloqueY{i};
    yoldTraining=y;
    yoldTesting=yTest;
    
    %------------------------------------------------------------
    lim0=length(x)*porc_ini;
    
    ps0 = svmker(x(:,(1:lim0))',x(:,(1:lim0))', kernel, kerneloption);
    e0 = ones(size(y));
     
    ybin0=y(1:lim0);
    ybin0(find(y(1:lim0)>1))=-1;
    H0 =ps0.*(ybin0'*ybin0);
   %------------------------------------------------------------ 
   
    %------------------------------------------------------
    fprintf('......................................................\n\n');
    beta=zeros(size(ybin0));
    [ybin0 ,res0 ,c0 ,b0]=SVMprop(x,ybin0,ps0,H0,eta,beta,[]);
    
   indSc=find(res0.*ybin0<S);
   indS=find(res0.*ybin0>=S);
   out0=length(indSc);
  
   c=zeros(size(x)-size(indSc));
  % indC=find(c0~=0 & res0.*ybin0>=S);
   
   c(indS)=c0(indS);
  %------------------------------------------------------------------------
   beta=zeros(size(y));
   indbeta=find(S<=res0.*ybin0 & res0.*ybin0<1);
   beta(indbeta)=-eta*((res0(indbeta).*ybin0(indbeta)-S^2+S-1)/(S-1)^2);
   beta(indSc)=[];
   %-------------------------------------------------
   x(:,indSc)=[];
   y(indSc)=[];
   ps  =  zeros(size(x,2),size(x,2));	% se genera el kernel total
   ps=svmker(x',x',kernel,kerneloption);

   ybin=y;
   ybin(find(y>1))=-1;
   H =(ps).*(ybin'*ybin);
  %------------------------------------------------------------------------
     
   
   %----------------------------------------------------------------------
  
   
   
   
   
   for it=1:maxit,
        
        
        [ybin, res, c, b]=SVMprop(x,ybin,ps,H,eta,beta,c);
      beta1=beta;
       beta=zeros(size(ybin));
       indbeta=find(S<=res.*ybin & res.*ybin<1);
       beta(indbeta)=-eta*((res(indbeta).*ybin(indbeta)-S^2+S-1)/(S-1)^2);

       %-------------------------------------------------
        
        indSc=find(res.*ybin<S);
       % indS=find(res.*ybin>=S);
          
        c0=c;
        c(indSc)=[];
        %------------------------------------
        beta(indSc)=[];
        beta1(indSc)=[];
        y(indSc)=[];
        ybin(indSc)=[];
        x(:,indSc)=[];
        %------------------------------------
       
   
       
        H(:,indSc)=[];
        H(indSc,:)=[];
        
        ps(:,indSc)=[];
        ps(indSc,:)=[];
        %------------------------------------
        
        if(norm((beta1+beta)/2,2)==0 || norm(beta1-beta,2)==0)
            tol=0
        else
            tol=norm(beta1-beta,2)/norm((beta1+beta)/2,2)
        end
        it        
        if ((tol<1e-4) && (length(c)==length(c0))), break;     end     
                   
        
        
   end
   
   outn=sum(beta==eta);
   OutL(1,i)=out0-length(indSc);
   numit(1,i)=it;
   SV(1,i)=sum(c~=0);
   SV
   yoldTraining=y;
%%%%%%%%
    

    if showTraining == 1
        clear Y I mist etrain sol; 
        for k=1:length(yoldTraining),
            for indRaices=1:nclases,

                cRaiz=c(find(yoldTraining==labels(indRaices)));
                xRaiz=x(:,find(yoldTraining==labels(indRaices)));
                sol(indRaices,k)=cRaiz'*svmker(x(:,k)',xRaiz',kernel,kerneloption)'+b{indRaices} ;
            end
        end

        [Y I]=max(sol);
        mist = I==yoldTraining;
        perfTraining = sum(mist)/(length(yoldTraining));
        fprintf(1,'Training performance %f ',perfTraining);

        for l=1:nclases
            if length(find(yoldTraining==labels(l))) ~= 0
                etrain(l) = sum(mist(find(yoldTraining==labels(l))))/length(find(yoldTraining==labels(l)));
                fprintf(1,'%f ',etrain(l));
            end
        end
        fprintf(1,'\n');
    end
    
    clear Ytest Itest mistTest solTest; 
    
    for k=1:length(yoldTesting),
        for indRaices=1:nclases,

            cRaiz=c(find(yoldTraining==labels(indRaices)));
            xRaiz=x(:,find(yoldTraining==labels(indRaices)));
    
            solTest(indRaices,k)=cRaiz'*svmker(xTest(:,k)',xRaiz',kernel,kerneloption)'+b ;
        end
    end
    
    [Ytest Itest]=max(solTest);
    mistTest = Itest==yoldTesting;
    perf(i) = sum(mistTest)/(length(yoldTesting));%relacion
    
    for l=1:nclases
        if length(find(yoldTesting==labels(l))) ~= 0
            etestactual = sum(mistTest(find(yoldTesting==labels(l))))/length(find(yoldTesting==labels(l)));
            perfxClase{l} = [perfxClase{l} etestactual];
        end
    end

    tiempoSolver(i) = etime(clock,inicioSolver);
end


perfTotal = mean(perf);%promedio de la matriz
fprintf(1,'Testing performance %f ',perfTotal);

for i=1:nclases
    perfClase(i) = mean(perfxClase{i});
    fprintf(1,'%f ',perfClase(i));
end
fprintf(1,'\n');

tiempoScript = etime(clock,inicio);
tiempoSolverMedio = mean(tiempoSolver); 
tiempoSolverTotal = sum(tiempoSolver);
tiempos(1)=tiempoScript; tiempos(2)=tiempoSolverTotal; tiempos(3)=tiempoSolverMedio;

fprintf(1,'total script time elapsed (secs): %f\n', tiempoScript);
fprintf(1,'total solver time elapsed (secs): %f\n', tiempoSolverTotal);
fprintf(1,'mean solver time (secs): %f\n', tiempoSolverMedio);

