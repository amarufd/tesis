Sigma=[5e2,1e2,5e1,1e1,5e0,1e0,1e-1,5e-1,1e-2,5e-2];
%Sigma=[1e1,5e0,1e0,1e-1,5e-1,1e-2,5e-2];
eta=[5e2,1e2,5e1,1e1,5,1,5e-1,1e-1,5e-2,1e-2];
S=[0,-0.3,-0.6,-0.9];


etiquetas=['resultado ','           res x clase         ','               tpo script    tpo solver     tpo mean solv','        sigma ', '                eta', '                S', '                                                         diferencia out 0-n k1....kn','                                                            SV k1....kn','                                                            numero de it k1....kn'];
vtest=zeros(length(Sigma)*length(eta)*length(S),39);
mostrar=0;
k=10
porc_inicial=0.3
maxit=50

BD='appAstroparticle-80';dat=500;
[arch,error]=fopen('resultados/SVM-standar-appAstroparticle.dat','w'); %ojo aca
%[arch,error]=fopen('resultados/pruebaalg.dat','w');
if (arch > -1),
fprintf('COMIENZA CROSS VALIDATION\n\n');
p=1;

fprintf(arch,'Cross Validation   %s  %d datos subset inicial= %d  tol<1e-1 maxit= %d\n',BD,dat,porc_inicial,maxit);
fprintf(arch,'%s\n',etiquetas);

inicio=clock;
 for i=1:length(Sigma),
        for z=1:length(eta),
            for j=1:length(S),
           

                [resultado perfclase tiempos Out SV it]=CrossVal(BD,k,eta(z),S(j),'gaussian',Sigma(i), mostrar,porc_inicial,maxit);

                vtest(p,1)=resultado;
                vtest(p,2)=perfclase(1);
                vtest(p,3)=perfclase(2);
                vtest(p,4)=tiempos(1);
                vtest(p,5)=tiempos(2);
                vtest(p,6)=tiempos(3);
                vtest(p,7)=Sigma(i);
                vtest(p,8)=eta(z);
                vtest(p,9)=S(j);
                vtest(p,10)=Out(1);
                vtest(p,11)=Out(2);
                vtest(p,12)=Out(3);
                vtest(p,13)=Out(4);
                vtest(p,14)=Out(5);
                vtest(p,15)=Out(6);
                vtest(p,16)=Out(7);
                vtest(p,17)=Out(8);
                vtest(p,18)=Out(9);
                vtest(p,19)=Out(10);
                vtest(p,20)=SV(1);
                vtest(p,21)=SV(2);
                vtest(p,22)=SV(3);
                vtest(p,23)=SV(4);
                vtest(p,24)=SV(5);
                vtest(p,25)=SV(6);
                vtest(p,26)=SV(7);
                vtest(p,27)=SV(8);
                vtest(p,28)=SV(9);
                vtest(p,29)=SV(10);
                vtest(p,30)=it(1);
                vtest(p,31)=it(2);
                vtest(p,32)=it(3);
                vtest(p,33)=it(4);
                vtest(p,34)=it(5);
                vtest(p,35)=it(6);
                vtest(p,36)=it(7);
                vtest(p,37)=it(8);
                vtest(p,38)=it(9);
                vtest(p,39)=it(10);
                
                
                fprintf(arch,'%f   [   %f   -   %f  ]   [ %f   -   %f   -   %f   ]   %f   -   %f   -   %f   [ %f   -   %f   -   %f   -   %f    -   %f    -   %f    -   %f    -   %f    -   %f    -   %f    ] [ %f   -   %f   -   %f   -   %f    -   %f    -   %f    -   %f    -   %f    -   %f    -   %f    ] [ %f   -   %f   -   %f   -   %f    -   %f    -   %f    -   %f    -   %f    -   %f    -   %f    ] ',vtest(p,:));
                fprintf(arch,'\n');
                p=p+1;
            end
        end
 end
    
    
    tiempo=etime(clock,inicio);
    fprintf(arch,'\nTERMINA CROSS VALIDATION\n duracion total= %f\n',tiempo);
    fclose(arch);
    fprintf('\nTERMINA CROSS VALIDATION\n duracion total= %f\n',tiempo);
else
    disp('El archivo no pudo crear');
end
