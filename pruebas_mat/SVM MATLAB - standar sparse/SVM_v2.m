function [c,b,ybin,ps]=SVM_v2(x,y,kernel,kerneloption,eta)
%los valores de la funcion son los q retorna


%- mm=size(x,2);
%- nn=size(x,1);
% 
%- ps  =  sparse(mm,mm);	% se genera el kernel total
ps=svmker_v2(x,x,kernel,kerneloption);
% beq=1*n
ybin=y;
ybin(find(y>1))=-1; %#ok<FNDSB>
H =ps.*(ybin'*ybin);

e=ones(size(y));

%optimeset es nativa de matlab
options = optimset('LargeScale','off','MaxIter',2000); %#ok<NASGU>

%quadprog es nativo de matlab
H=H;
f=-e;
A=[];
b=[];
Aeq=ybin;
beq=0*e;
lb=0*e';
ub=eta*e;
x0=[];
option=[];
[c,FVAL,EXITFLAG]=quadprog(H,f,A,b,Aeq,beq,lb,ub,x0,option); %#ok<NASGU,ASGLU>
% [c,FVAL,EXITFLAG]=quadprog(H,-e,[],[],ybin,0*e,0*e',eta*e',[],[]); %#ok<NASGU,ASGLU>
%                         (H,f ,A ,b ,Aeq ,beq,lb  ,ub    ,x0,options)
fprintf('......................................................\n\n');
%w=(y'.*c)'*x';
%convex=x*c/2;
b = ((c'.*ybin)*ps*c)/2; 
