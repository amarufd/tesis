function [c,b,ybin,ps]=SVM(x,y,kernel,kerneloption,eta)
%los valores de la funcion son los q retorna


mm=size(x,2);
nn=size(x,1);

ps  =  sparse(mm,mm);	% se genera el kernel total
ps=svmker(x',x',kernel,kerneloption);

ybin=y;
ybin(find(y>1))=-1;
H =ps.*(ybin'*ybin);

e=ones(size(y));

%optimeset es nativa de matlab
options = optimset('LargeScale','off','MaxIter',2000);

%quadprog es nativo de matlab
[c,FVAL,EXITFLAG]=quadprog(H,-e,[],[],ybin,0,0*e',eta*e',[],[]);
fprintf('......................................................\n\n');
%w=(y'.*c)'*x';
%convex=x*c/2;
b = ((c'.*ybin)*ps*c)/2; 
