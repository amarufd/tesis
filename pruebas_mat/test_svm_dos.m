clear all

eval('load ../docs_amaru/review_polarity/matlab/all.mat')
%TfIdf=full(TfIdf);
xdata = [TfIdf(1:400,1:end);TfIdf(1001:1400,1:end)];
group = [value(1:400);value(1001:1400)];

txdata = [TfIdf(401:500,1:end);TfIdf(1401:1500,1:end)];
tgroup = [value(401:500);value(1401:1500)];

sig={};
tic
    svmStruct = svmtrain(xdata,group,'kernel_function','rbf','rbf_sigma',10^(6));

    species1 = svmclassify(svmStruct,txdata);

    a=0;
    for i=1:length(tgroup)
        if(tgroup(i)==species1(i))
            a=a+1;
        end
    end
 toc   
    sig=([sig; a]);

    clear i a;