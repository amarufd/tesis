clear all

dic={};
c=dir('n-grama_1/*.mat');
for x = 1:size(c)
    tic;
   eval(['load n-grama_1/' c(x).name] );
   if length(texto)>1 
        diccionario=texto';
        dic=[dic;diccionario]; %#ok<AGROW>
        dic=sort(dic);
        dic=unique(dic);
%         if ((x/100-floor(x/100))==0)
%             toc
%             x
%             tic
%         end
   end
   seg=toc;
   disp(['n1 - Revisado documento ',num2str(x),' en ',num2str(seg),' segundos.']);
end

 
dic=dic'; %#ok<NASGU>

eval(['save mat/dic-n_1.mat ' 'dic'] );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



clear all


dic={};
c=dir('n-grama_3/*.mat');
for x = 1:size(c)
    tic;
   eval(['load n-grama_3/' c(x).name] );
   if length(texto)>1 
        diccionario=texto';
        dic=[dic;diccionario];
        dic=sort(dic);
        dic=unique(dic);
%         if ((x/100-floor(x/100))==0)
%             toc
%             x
%             tic
%         end
   end
   seg=toc;
   disp(['n3 - Revisado documento ',num2str(x),' en ',num2str(seg),' segundos.']);
end

dic=dic';

eval(['save mat/dic-n_3.mat ' 'dic'] );