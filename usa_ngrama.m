%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% N_GRAMA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('inicia N-GRAMA')

clear all
dic={};
x=1;
ngrama=1;
folder='e';
folders=['n-grama_' int2str(ngrama)];
f=dir([folder,'/']);

for y = 3:size(f)
    c=dir([folder,'/', f(y).name,'/*.mat']);
for x = 1:size(c)
    if  exist([folders,'/'])==0 %#ok<EXIST>
    mkdir([folders,'/']);
    end
   eval(['load ' folder '/' f(y).name '/' c(x).name] );
   texto=n_grama(texto,ngrama);
   eval(['save  ' folders '/'  c(x).name ' texto']);
%   x=x+1;
end
end