
Habeas Corpus presentado por el hijo de Nora de Cortiñas: Deberá decidir la Cámara Federal de Casación Penal



En diciembre de 2012, Nora de Cortiñas, Madre de Plaza de Mayo-Línea Fundadora, presentó un segundo de hábeas corpus para saber qué pasó con su hijo Carlos Gustavo, quien desapareció el 15 de abril de 1977, a los 24 años de edad. La Sala A de la Cámara del Crimen rechazó el recurso por "extemporáneo", pero los camaristas Alfredo Barbarosch y Mariano Scotto consideraron efectuar una "excepción de la regla". La resolución está en manos de Cámara Federal de Casación Penal.

La Cámara Federal de Casación Penal deberá resolver si confirma o revoca el archivo de un pedido de la más conocida de las Madres de Plaza de Mayo-Línea Fundadora, Nora Cortiñas, de habeas corpus presentado por la desaparición de su hijo durante la última dictadura militar. Carlos Gustavo Cortiñas, de 25 años, fue secuestrado cerca de la estación Castelar, al oeste de la ciudad de Buenos Aires, el 15 de abril de 1977, nunca más apareció y no hay testimonios de su paso por un centro clandestino de detención.

La búsqueda rutinaria en registros y documentos que generó la acción de habeas corpus presentado por su madre dio resultados negativos, por lo que, al no haberse podido establecer su paradero, en primera instancia se dispuso el archivo del expediente.

Sin embargo, la Cámara Nacional en lo Penal se pronunció por dejar sin efecto esa decisión, encomendándole su revisión al tribunal de Casación.

Los camaristas Alfredo Barbarosch y Mariano Scotto consideraron que "en el presente caso y en atención a que la discusión se plantea en el ámbito de un pedido de habeas corpus cabe hacer excepción de la regla", según la cual las decisiones sobre las cuestiones procesales son inapelables.

El archivo de la causa, adujeron "pone fin a la pretensión de la parte, lo que podría generar un agravio irreparable que debe ser analizado por el superior en función de la posibilidad de que se encuentre afectado un derecho que exige tutela inmediata".
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

