
Recortan previsión de crecimiento económico para Alemania



Los principales institutos económicos de Alemania recortaron su previsión de crecimiento para el país durante 2013, al colocarlo en torno al 0,8 por ciento, según adelantaron hoy medios locales de prensa.

El dato, que se dará a conocer oficialmente mañana, supone un retroceso en relación con el 1,0 por ciento previsto anteriormente.

No obstante, el informe de los institutos es más optimista que el del gobierno, el cual proyecta un crecimiento del 0,4 por ciento para este año por lo efectos de la crisis en la Eurozona.

En tanto, para 2014 se prevé un crecimiento de 1,9 por ciento del producto interno bruto germano.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

