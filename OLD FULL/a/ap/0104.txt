
En huelga de hambre 84 reos en cárcel de Estados Unidos en Guantánamo



Más de la mitad de los detenidos en la cárcel de Estados Unidos en la base naval de Guantánamo permanecen en huelga de hambre, confirmaron hoy autoridades militares.

Desde 2002, la Casa Blanca mantiene un centro de internamiento en esa instalación ubicada en territorio cubano contra la voluntad del pueblo y gobierno de la isla caribeña.

Un total de 84 de los 166 prisioneros siguen sin ingerir alimentos, en protesta por las irregularidades y abusos cometidos en su contra y las condiciones de su detención durante más de una década, sin que se les impute acusación oficial alguna a la mayoría de ellos.

Según las autoridades castrenses, a 16 de los reos se les alimenta de manera forzosa y otros cinco son tratados en un centro hospitalario, informó la cadena televisiva CNN.

Uno de los abogados defensores de los presos dijo este lunes que resulta difícil que los detenidos se mantengan tranquilos ante la falta absoluta de esperanza de que cambien las condiciones inhumanas que sufren en ese centro de internamiento.

El ayuno comenzó el pasado 6 de febrero con seis prisioneros pero se unieron más huelguistas que rechazan medidas como el confinamiento por tiempo indefinido, los registros a sus pertenencias y confiscación de copias del Corán, el libro sagrado de los musulmanes.

Tras asumir su primer mandato en 2009, el presidente Barack Obama firmó una orden ejecutiva para cerrar la prisión de Guantánamo en menos de un año, pero la cárcel permanece en funciones.

Existen numerosas denuncias sobre el empleo de técnicas crueles en esa instalación como la privación del sueño, encierros de los prisioneros desnudos en habitaciones con bajas temperaturas e interrogatorios extenuantes.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

