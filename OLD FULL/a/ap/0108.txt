
España: La estafa de las preferentes



Miles de personas salen desde hace más de un año a las calles de las ciudades españolas para pedir justicia. Son parte de los 700.000 afectados por las participaciones preferentes, una estafa bancaria pionera que les ha dejado sin ahorros. 52 entidades financieras, algunas rescatadas con dinero público como Bankia y Novagalicia, les ocultaron la letra pequeña y les vendieron estos productos de forma irregular. Ahora su dinero está paralizado y no pueden recuperarlo.

La mayor parte de las víctimas de esta trampa son personas de avanzada edad, algunos incluso analfabetos y enfermos, jubilados que deseaban conservar en un sitio seguro sus ahorros. Confiaron en la palabra de directores y empleados de las oficinas bancarias de toda la vida, que les engatusaron con información falsa y poco clara, y muchos adquirieron las preferentes sin leer el contrato.

Les prometieron que su dinero estaba seguro, tenía rentabilidad garantizada y podrían recuperarlo cuando lo desearan, sin embargo, cuando quisieron hacerlo la entidad se negó a dárselo. Descubrieron que habían contratado un producto muy complejo de naturaleza perpetua. Los bancos y cajas no fijaban una fecha para su devolución y, si lo hacían, era en un plazo que se extendía décadas y décadas, incluso hasta el año 3000.

El origen de este masivo engaño se sitúa en 2007, año en el que las entidades financieras comenzaron a vender estos productos a pequeños ahorradores después de que los inversores internacionales los rechazaran por inseguros. Era una manera de evitar problemas de solvencia y así continuar con la expansión del crédito y fomentar la burbuja inmobiliaria. Se calcula que mediante esta treta los bancos y cajas han llegado a obtener más de 32.000 millones de euros. Una práctica de sobra conocida y consentida por el Banco de España y la Comisión Nacional del Mercado de Valores, que investigó las preferentes y encontró irregularidades en el 80 de los casos. Para Oscar Serrano, abogado del Colectivo Ronda, “la cuestión es que se ha convertido a los ahorradores en accionistas o especuladores sin ninguna preparación”.

A Jaume Font, jubilado de 71 años, le colocaron 29.000 euros en participaciones preferentes en el año 2002. Todavía recuerda que cuando le ofrecieron el producto anotó en un papel “si lo necesito lo puedo recuperar”. “Mi confianza era total, ni pregunté” afirma. A otros como Bartolomé ni siquiera se les avisó de que adquirían el producto, les movieron el dinero de sitio sin firma ni autorización. Al padre de Pedro, enfermo de alzheimer, le hicieron firmar con su huella dactilar.

Algunos casos se encuentran en los juzgados y en ocasiones se ha condenado a la entidad a devolver el dinero al cliente, sin embargo son muy pocos los que se resuelven por vía judicial. Cientos de miles de personas esperan todavía una solución justa, ya que contemplan la que el Gobierno y las entidades financieras les proponen como una segunda estafa. Les han ofrecido canjear sus ahorros por acciones de dudoso valor y quieren convencerles de que se sometan a un arbitraje que no es universal, si no que sólo beneficiaría al 15 de los clientes engañados.

Varias organizaciones sociales apoyan la causa. La Plataforma de Afectados por las Preferentes ofrece asesoramiento legal gratuito y ADICAE es una asociación que ha interpuesto demandas contra todas las entidades involucradas en el escándalo. Su presidente, Manuel Pardos, considera que “la solución viable, adecuada y eficaz al problema de las participaciones preferentes tiene que ser una solución colectiva”.

Los afectados convocan manifestaciones de forma periódica y se congregan frente a las sedes de las entidades bancarias en actitud de protesta. Con la impotencia a cuestas reclaman justicia y manifiestan su indignación ante uno de tantos engaños que este sistema financiero ha decidido llevar a cabo. De momento los estafados no han dejado de luchar por lo que es suyo.

Marta González Borraz es periodista.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

