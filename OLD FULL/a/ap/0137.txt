
Las finanzas británicas empeoraron en 2013



Las finanzas de los británicos empeoraron en abril a un ritmo más rápido que el mes previo, debido a una baja en los niveles de salarios, un aumento del costo de vida y nuevos ajustes por parte del gobierno, según un informe del grupo Markit.

La entidad indicó que su índice de finanzas personales cayó de 39,3 puntos en marzo a 37,7 puntos en abril.

El 32 de las familias británicas admitió que sus finanzas personales empeoraron este mes, mientras sólo un 8 reportó una mejoría.

Del total de las familias del país consultadas, unas 1.500, el 42 dijo que espera que sus finanzas empeoren en los próximos 12 meses, comparado con el 27 que opinó lo contrario.

"El reporte de abril da cuenta de un empeoramiento en la situación financiera de muchos británicos, impulsado por las presiones salariales y el alto costo de vida", declaró Tim Moore, autor del reporte.

Según Moore, esa tendencia a la baja perjudicará la confianza del consumidor en los próximos meses.

El gasto de consumidores, que genera dos tercios del PIB británico, es vital para que la economía logre crecer tras más de dos meses de estancamiento.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

