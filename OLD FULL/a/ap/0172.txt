
Relaciones de consumo en Brasil



Dos aspectos son esenciales en el debate sobre el Plan Nacional de Consumo y Ciudadanía que Dilma Rousseff creó en marzo de 2013 a fin de tratar la protección del consumidor como política de Estado. El primero es su urgencia debido a los abusos impunes que la esfera del lucro comete contra la esfera de la colectividad. El segundo es la sospecha en torno a que Rousseff refuerce la formación de los brasileros como consumidores en vez de cómo ciudadanos. Así, el gobierno federal nos protege prioritariamente en las relaciones de consumo, en detrimento de las relaciones ciudadanas. Hay que tener cuidado para no confundir la esfera del consumismo con la de la ciudadanía.

La propuesta política del Plan Nacional de Consumo y Ciudadanía es mejorar la calidad de los productos que circulan y se consumen en territorio nacional y de servicios, además el Plan tiene el fin de fomentar el perfeccionamiento y la transparencia de las relaciones de consumo a través de la creación de tres Comités Técnicos (Consumo y Regulación, Consumo y Turismo, Consumo y Posventa). Su primer paso es dentro de un mes, componer una lista de productos sobre los cuales los consumidores deberán tener una respuesta inmediata del fabricante ante sus demandas y quejas mientras estén en garantía. De dos, una: o los brasileros nos concientizamos de nuestro papel activo en la mejora de estas relaciones, o el plan será solo un balcón con unos pocos grandes anuncios.

Finalmente el Gobierno Federal decidió dar un incentivo al trabajo de los mecanismos municipales de Protección al Consumidor (PROCON) que han sido incapaces de contener los abusos cometidos por la falsedad de aquellos que las empresas prometen a los consumidores. Esta política se da la mano con la de fortalecimiento de las agencias reguladoras, que se crearon con las “concesiones” de empresas de gestión pública en los años 1990. Noticia de la Agencia Brasil (Luciene Cruz. Governo federal lança Plano Nacional de Consumo e Cidadania, 15 de marzo de 2013). Informa que las quejas principales se refieren a la “telefonía celular (9,17), a los bancos comerciales (9,02), a los servicios de tarjeta de crédito (8,23), a la telefonía fija (6,68) y al área financiera (5,17)”.

En realidad el Plan merece más elogios que críticas. Es una política que se demoró por capricho pero que llegó para quedarse como política de Estado. Aumenta la exigencia de los consumidores referente a la calidad de los productos y del servicio que compran y contratan, a la claridad de información, al cumplimiento de los plazos, a la satisfacción del cliente, a los canales de atención. Mientras Brasil mejora sus relaciones de consumo a fin de evitar insatisfacciones y trastornos de sus consumidores, algunos critican el deterioro de estas relaciones en Inglaterra, donde se descubrió la venta de carne de caballo en los supermercados en empaques cuya etiqueta anunciaba contener carne bovina.

Es por esta y otras razones que es muy poco lo del Primer Mundo que sirve al Tercer Mundo. Esta afirmación no guarda sin embargo una buena dosis de escarnio ideológico.

Mientras las prácticas de mercado profundizan sus métodos de abordar y conquistar al consumidor (por ejemplo la distribución de panfletos casa por casa representa un tiempo de publicidad mayor que el de los propios programas de televisión), los gestores públicos legítimos dan el primer paso desde el Estado para regular las relaciones de consumo en Brasil. Su apuesta, con todo debe ser tan sofisticada al punto de incluir en este Plan modernizador el comercio electrónico, que merecería un estudio aparte debido a la importancia del tema. Esto se debe a que nuestros correos electrónicos se comercializan por Internet antes de que sus buzones de entrada se saturen de spams, y la publicidad que nos corresponde de websites de nuestro interés se direcciona de acuerdo con la pesquisa de gustos encomendada por los brownsers (navegadores con los cuales accesamos a Internet).

Es indiscutible que la calidad de los productos y servicios necesita mejorar en Brasil, sin embargo este avance no ocurrirá si se deja de lado nuestra formación como ciudadanos y nuestra participación activa en las resoluciones del país. Es inadmisible que a la menor lluvia, barrios enteros queden temporalmente sin energía eléctrica o que en horas pico la velocidad de la banda ancha de algunos servicios de Internet se reduzca drásticamente. Ejemplos como éstos son una afrenta a la dignidad del brasilero no solo como consumidor sino también como ciudadano.

La crítica que hago es que Brasil se prepare tan competitivamente en el comercio mundial y tan satisfactoriamente en las relaciones internas de consumo, sin formar buen a sus jóvenes aprendices, pequeños emprendedores y otros brasileros en la búsqueda de sentido público de ciudadanía. Podríamos pasar de una posición de “defensa” o “protección” del consumidor como víctima constante, a otra en que el ciudadano sea artífice resuelto de una “brasilidad” nueva y ejemplar.

Fuentes:
- http://www.redebrasilatual.com.br/temas/cidadania/2013/03/produto-essencial-com-problema-tera-de-ser-trocado-imediatamente
- http://www.sul21.com.br/jornal/2013/03/produtos-essenciais-deverao-ser-trocados-imediatamente-exige-dilma/
- http://www.valor.com.br/brasil/3047098/dilma-anuncia-plano-para-melhorar-relacoes-com-o-consumidor-no-pais
- http://blog.justica.gov.br/inicio/tag/presidenta-dilma-roussef/
- http://agenciabrasil.ebc.com.br/noticia/2013-03-23/plano-nacional-de-consumo-e-cidadania-tera-capitulo-especial-para-tarifas-bancarias
- http://agenciabrasil.ebc.com.br/noticia/2013-03-15/governo-federal-lanca-plano-nacional-de-consumo-e-cidadania.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

