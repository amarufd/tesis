
Panamá: Deuda, guerra y Cerro Colorado



La deuda panameña está creciendo a una velocidad que para la mayoría de los observadores pronto nadie podrá controlar. El actual gobierno – y sus asesores en Wall Street - sostiene que la deuda no constituye un riesgo en la medida en que la economía (medido por el producto interno bruto) crece a un ritmo igual o superior. Ambos crecen a una tasa anual del 10 por ciento. Obviamente, cuando la economía se desinfle y deje de crecer, Panamá ya no será un cliente interesante para los prestamistas. Nadie nos querrá prestar. Lo que no consideran los ideólogos incrustados en los puestos gubernamentales es que el país quedará trabado con una deuda gigantesca cuyo ‘servicio’ no se podrá pagar.

En el último lustro ha llegado a Panamá una inversión de capital muy fuerte. El mismo ha sido básicamente en el sector especulativo de la construcción, turismo y la banca. También se han hecho inversiones en el sector energético (represas hidroeléctricas) y en la minería. La mayor parte de esa inversión se realiza bajo la forma de capitales que entran y salen, sin mayores controles financieros (y menos políticos). La banca panameña cuenta con una cartera de casi 90 mil millones de dólares. La plaza bancaria panameña se ha duplicado en menos de una década. El año pasado creció en un 10 por ciento.

El proceso descrito es llamado una “burbuja”. Sobre la base de un ciclo favorable de crecimiento el gobierno se abre -de par en par- a los negocios sin contemplar su viabilidad a mediano y largo plazos. Cuando el ciclo toma una dirección descendiente el país se queda varado como un barco cuando es atrapada por la marea baja. Este fenómeno es muy conocido por Panamá que lo ha experimentado a lo largo de su historia. Combinado con el mal manejo de la deuda (externa e interna) que ha adquirido el gobierno panameño, especialmente en los últimos años, puede conducir el país a una guerra civil. Veamos por qué.

Según algunos autores que escribían cuando el capitalismo conoció su época dorada (1948-1973) y crecía a escala global, la deuda y la inflación son maneras de retardar y resolver temporalmente conflictos sociales. El endeudamiento era una necesidad ante la falta de políticas que impulsaran un crecimiento sostenible y productivo.

¿Puede Panamá hacer eso?

En el caso de Panamá, la nacionalización del Canal (en 2000) y los crecientes ingresos (de 2001 hasta el presente) de su operación han creado un globo especulativo espectacular. El crecimiento del PIB en el último lustro le permitió a Martinelli endeudar al país a un ritmo de mil millones de dólares al año. En la actualidad, la deuda supera los 14 mil millones de dólares. El gobierno tiene que pagarle anualmente a sus acreedores más de mil millones de dólares en intereses. Lo equivalente al presupuesto nacional de educación (que atiende en forma deficiente a 800 mil estudiantes panameños).

La deuda de $14 mil millones no incluye los contratos "llave en mano" que ha firmado Martinelli por otra suma de 3.4 mil millones de dólares. La deuda totalizaría 18 mil millones de dólares. Hay quienes ven una luz al final del túnel al señalar que la deuda interna se duplicó para colocarse ya en 3 mil millones de dólares. En realidad, esto significa que ante una recesión la debacle de los inversionistas locales será aún más dura. A pesar del banquete y del baile de gala que celebran los especuladores actualmente, el gobierno de Martinelli acaba de anunciar que necesita a corto plazo otros 700 millones de dólares para balancear el presupuesto nacional.

Al término de la expansión del Canal - calculado entre 2014 y 2015 - se desinflará el globo y Panamá tendrá dificultades para pagar las deudas. Todas las inversiones que ha realizado Martinelli son de servicios, no generan riqueza ni empleos productivos. Las carreteras se pagan si transportan mercancías y trabajadores. Igual, las avenidas, calles y Metro en la ciudad de Panamá. En una recesión no hay producción de mercancías y menos comercio.

Entonces se hablará de austeridad. ¿Para quién? ¿Otra vez los trabajadores? Estos han sido despojados de todo y no tienen con que seguir siendo victimizados.

Hay voces que hablan de una posible salvación: La explotación de la mina de cobre en Petaquilla que rendirá varios miles de millones al año.

Pero sólo le dará al fisco el 4 por ciento en concepto de impuesto. Esa participación del gobierno representaría 100 millones de dólares al año.

Incluso menos, si First Quantum (nueva propietaria de la empresa Panama Cobre) sale con la suya de incrementar al máximo las ganancias que se llevarán al extranjero.

La otra apuesta de los especuladores que gobiernan actualmente al país es la mina de Cerro Colorado, en la Comarca Ngobe-Buglé. Si el mercado de metales preciosos se sostiene esta mina podría duplicar los ingresos que generaría Panama Cobre en Petaquilla. El efecto político de esta iniciativa, sin embargo, sería una guerra civil y la probable pérdida del poder político de la clase rentista que controla desde hace 20 años el gobierno.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

