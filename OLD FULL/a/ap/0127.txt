
Jerga narco distorsiona habla mexicana



La jerga de la delincuencia empezó a contaminar el lenguaje cotidiano de los mexicanos, un fenómeno que para algunos expertos se debe a la incorporación de términos del crimen cuando la prensa local reporta las noticias de actualidad.

Como un Rey Midas al revés, el narco todo lo que toca lo vuelve basura, lo "mediatiza, lo trivializa y lo deshumaniza": y eso es lo que está sucediendo con el lenguaje, alertan los expertos.

Marcela Turati y Daniela Rea, reporteras que han cubierto numerosos casos sobre violencia del narcotráfico, afirman que "los periódicos se convirtieron en contadores de muertos y nosotros, los periodistas, en corresponsales de guerra en nuestra tierra". "En las redacciones se hablaba de 'narcos' y 'capos', y el lenguaje estilizado del asesinato llegó para quedarse", afirman.

Entre los nuevos término empleados por la prensa en la cobertura noticiosa de la violencia figuran "levantones" (secuestros), "encajuelados" (cadáveres abandonados en el baúl de un auto) o "encobijados (cuerpos envueltos en frazadas).

"El crimen ha terminado por imponer su lenguaje, al lograr que se vuelvan de uso común palabras" como estas, afirmó el viceministro de Prevención y Participación Ciudadana Roberto Campa.

La espiral violenta ha dejado una estela de horror que se refleja en 83.000 muertos, 27.000 desaparecidos y unos 250.000 desplazados pero su impacto en la cultura y la lengua cotidiana todavía no ha sido evaluado.

Algunos expertos acusan de este fenómeno a los periodistas, que trabajan bajo una gran presión, en forma apresurada y bajo las exigencias de las nuevas tecnologías. O a los usuarios de Twitter o Facebook, donde no importa cómo se estructuren los mensajes, sino que se conviertan en "virales".

Otros creen que son los dueños de los medios o los directivos, quienes planifican los periódicos y los órganos informativos, los verdaderos responsables, por estar más guiados por la necesidad de vender que por la búsqueda de la verdad.

"Los periódicos han hecho una cobertura epidérmica de muertos y casquillos (balas percutidas) en lugar de contar historias de personas, del narcotráfico, de víctimas y de victimarios", afirma el periodista Javier Valdez, coordinador del semanario Riodoce y autor de los libros "Miss narco" y "Los morros del narco". La especialista Sara García Silberman, coautora del libro "Violencia y Medios. Seguridad pública, noticias y construcción del miedo", estima que los medios por sí solos no alientan este problema pero sí "juegan un rol instrumental clave en la reproducción de los valores que propician la injusticia social y la inseguridad".

"En ese contexto, los medios podrían favorecer una cultura de seguridad y confianza que sustituya a la del miedo", dijo.

El general colombiano Oscar Naranjo, asesor externo en materia de seguridad del presidente Enrique Peña Nieto, sugirió a los voceros del gobierno mexicano que eviten términos del narcotráfico al informar sobre el combate al crimen.

"La idea es evitar una percepción en la ciudadanía de que los criminales son poderosos e incluso héroes imbatibles y al mismo tiempo generar más confianza en las instituciones de seguridad y procuración de justicia", afirmó Campa.

Periodistas involucrados en el tema consideran sin embargo que también es importante que los reporteros y directivos de los medios colaboren en este cambio.

"El reto para hacer un nuevo periodismo es humanizarlo y regresarlo a la calle. Solo así podremos concientizar a la gente de que este fenómeno del narcotráfico es una forma de vida que está presente en todos los ámbitos", afirma Valdez. "Es necesario salirnos de las oficinas, del confort de las conferencias de prensa", concluyó.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

