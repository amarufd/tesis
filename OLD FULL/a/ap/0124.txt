
Inundados: Asambleístas platenses tomaron el Concejo Deliberante



Al grito del “que se vayan todos” más de 300 vecinos irrumpieron ayer en el Concejo Deliberante platense. Minutos antes los concejales del Frente para la Victoria, junto al bloque “Nacional y Popular” (La Cámpora y alakistas) se habían negado a aprobar una interpelación para que el Intendente Pablo Bruera se presentara en el Cuerpo Deliberativo para dar las explicaciones correspondientes sobre las responsabilidades políticas de la inundación. Por otra parte, la Justicia confirmó que hubo adulteración de actas de defunción.

Ayer alrededor de las 19:30 mientras el Concejo Deliberante platense celebraba su primera sesión del año, alrededor de 300 vecinos y vecinas irrumpieron en el recinto tras haberse movilizado previamente a la Municipalidad para exigir respuestas ante el temporal que azotó la ciudad el pasado 2 de abril.

Los manifestantes forman parte de algunas de las asambleas que se conformaron en la región tras la inundación y habían resuelto realizar una protesta colocando velas alrededor de la sede municipal.

El primer planteo que realizaron ni bien interrumpieron la sesión fue el pedido de renuncia del intendente Pablo Bruera, y además reclamaron que se esclarezca la cantidad de fallecidos, ya que las cifras oficiales sólo muestran 52, pero hay datos –varios ya judicializados– que permiten afirmar que hubo alrededor de 100 muertos.

Minutos antes de la intervención de los vecinos, los concejales del Frente para la Victoria, junto al bloque “Nacional y Popular” (La Cámpora y Alakistas) se habían negado a aprobar una propuesta de interpelación para que el Intendente Bruera se presentara en el Cuerpo Deliberativo para dar las explicaciones correspondientes sobre las responsabilidades políticas de la inundación.

Las asambleas vecinales vienen reclamando además del esclarecimiento de la cantidad de víctimas, subsidios para los damnificados, asistencia en mercadería y medicamentos para los barrios afectados, la realización de las obras postergadas de infraestructura hidráulica, entre otras demandas.

Patotas y amenazas

“Hay asambleas infiltradas, nos atacaron en la asamblea de Tolosa. Vos sabes como se maneja Control Urbano, nos mandan patotas. Vos sos responsable de lo que nos pase”, le recriminaron los vecinos al Presidente del Concejo Deliberante, Javier Pacharotti.

Por su parte, el titular del cuerpo legislativo aseguró “no saber” quien estaba a cargo de la Comuna en momentos de la inundación, cuando el Jefe Comunal se encontraba de vacaciones en Brasil, respuesta que enardeció mucho más a los vecinos.

A su vez, los asambleístas denunciaron que los punteros en los barrios los amenazaban y que no distribuyeron la mercadería que llegaba de Nación. “No existió ni un sólo plan. No sabían donde iban las cosas que llevaban a la Facultad de Periodismo. La Presidenta habla de coordinación y no hay una mierda de coordinación. Se pelean entre ustedes y se están cagando para ver quien queda mejor parado para octubre y todo el pueblo está sufriendo las penurias”, aseguró uno de los miembros de la asamblea de Tolosa.

Golpes de los concejales

Mientras se desarrollaba de la protesta, el Concejal del Frente para la Victoria Juan Pedro Chaves increpó y golpeó a algunos de los asambleístas que ingresaron al recinto. El legislador, que desde el año pasado se agrupó dentro de las filas del bruerismo viene del armado del exgobernador de la Provincia, Felipe Solá.

Tanto los legisladores oficialistas como el presidente del cuerpo Javier Pacharotti, tuvieron que salir escoltados por la policía y patovicas que aparecieron para frenar a los manifestantes, que se terminaron retirando cerca de las 22.

El domingo se volverá a realizar en Plaza Moreno una nueva reunión entre las asambleas que se siguen desarrollando en distintos barrios de la ciudad.

Confirman adulteración de actas de defunción

Por otra parte, ayer el Juzgado en lo Contencioso Administrativo N° 1, que conduce Luís Arias, identificó irregularidades en certificados de defunción, lo cual confirmaría la existencia de más víctimas de las que reconocieron los Gobiernos municipal, provincial y nacional. Reproducimos el comunicado del Juzgado:

Comunicamos que en el día de la fecha la causa N° 27.057 caratulada “Defensoría Oficial de Responsabilidad Juvenil s/ Habeas Data”, ha sido sorteada por la Suprema Corte en éste Juzgado en lo Contencioso Administrativo N° 1.

Que en función de lo peticionado por el Dr. Julián Axat (actor en la causa) en el día de la fecha se ha secuestrado en el Registro de las Personas la documentación correspondiente a los decesos acaecidos entre el día 2 y el día 10 de abril del corriente año a fin de cotejar los datos de las víctimas del temporal ocurrido en La Plata.

Que del análisis de la documentación secuestrada se observa, en principio, lo siguiente:

1) En diversos casos no ha tomado intervención la autoridad judicial competente, y los cuerpos fueron entregados mediante acta policial a sus familiares sin orden judicial, conforme lo exige el art. 97 de la Ley 14.078, la cual –en algunos casos - fue suplida con un sello del Ministerio de Justicia y Seguridad de la Provincia.

2) En otros, el Fiscal en Turno ha dispuesto la entrega de los cuerpos sin haber ordenado las correspondientes autopsias de las víctimas, conforme lo exigen las normas vigentes.

3) En otros, se han certificado defunciones por causas no traumáticas, cuando en realidad existen elementos de prueba colectados que demuestran en principio, la falsedad de la causa, puesto que el deceso ocurrió de un modo traumático, por inmersión y en la vía pública.

4) Existen casos que no han sido incorporados al listado oficial y que, de la documentación secuestrada surge, en principio, que los mismos se produjeron como consecuencia del temporal (Reyes, y Fernández). Con lo cual, el listado de víctimas computadas hasta el presente con la información oficial del Poder Ejecutivo, la relevada por éste Juzgado, y la que surge del Registro de las Personas, hasta el momento, asciende al número de cincuenta y siete (57).

5) Se han depurado –suprimiendo los errores- y unificado los listados respectivos, conforme a lo señalado en el punto anterior, en un único registro de víctimas provisorias que se adjunta al presente.

En lo sucesivo, y en la medida que la causa permanezca radicada en éste Juzgado, se analizará la restante documentación con las pruebas requeridas por el Dr. Axat y se brindará la información correspondiente.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

