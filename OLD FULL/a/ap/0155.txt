
Panorama económico de Vietnam



Continúa estabilización del mercado del oro / Arranca construcción de fábrica de Panasonic / Promueven inversiones sudcoreanas en provincia vietnamita / Amplia presencia en la primera Feria de turismo / Se eliminarán bancos ineficientes.

Continúa estabilización del mercado del oro
El Banco Estatal de Vietnam (BEV) vendió 25 mil 700 de los 26 mil taeles de lingotes de oro en la séptima subasta en Hanoi, como parte de su política de estabilización del mercado de ese metal valioso.
En la sesión la víspera el costo máximo licitado para un tael fue de 38 millones 920 mil dong (unos mil 850 dólares).
Luego de siete ofertas de oro, la entidad financiera vendió 183 mil 900 taeles para equilibrar los precios doméstico y mundial.

Arranca construcción de fábrica de Panasonic
Con una inversión de 19 millones de dólares, comenzó la construcción de una fábrica de Panasonic Eco Solutions Vietnam en el parque industrial Vietnam - Singapur II en la provincia sureña de Binh Duong.
Una vez en operación en abril del próximo año, la manufactura tendrá una capacidad de producción anual de 27 millones de unidades de dispositivos de cableado y 12 millones de interruptores de circuito.
Según lo programado, tal cifra se duplicará en 2018.

Promueven inversiones sudcoreanas en provincia vietnamita
La provincia centrovietnamita de Thanh Hoa ofrecerá condiciones favorables a las empresas sudcoreanas para que desplieguen sus inversiones y negocios en la localidad.
El presidente del Comité Popular provincial, Trinh Van Chien, reiteró ese compromiso al hablar en un seminario la víspera sobre la promoción de inversiones de empresas sudcoreanas.
Van Chien prometió también el mejoramiento del entorno inversionista en la localidad, así como del sistema de infraestructura y la capacitación de recursos humanos.
En el marco del coloquio, se firmó un memorando sobre inversión y comercio entre el Instituto provincial de Planificación – Arquitectura, la Compañía de equipos escolares Hong Duc y las empresas sudcoreanas de Heerim Architecs & Planners y de Cloz.
Las autoridades provinciales de Thanh Hoa y de la ciudad sudcoreana de Seongnam rubricaron un un acta de cooperación en la economía, ecología, cultura, salud, deportes, educación, turismo y tecnología y desarrollo infraestructural.
De acuerdo con el documento, Seongnam enviará este año expertos a esta provincia vietnamita para respaldarla en la planificación de una zona de alta tecnología, en el distrito de Tho Xuan.
En la actualidad, Thanh Hoa ocupa el sexto lugar nacional en la captación de Inversión Extranjera Directa con ocho proyectos, por valor de 16 mil millones de dólares.

Amplia presencia en la primera Feria de turismo
Más de 340 empresas nacionales y extranjeras se presentarán en la primera Feria Internacional de Turismo Vietnam 2013 (VITM), que se inaugurará mañana en Hanoi, informó el presidente del comité organizador, Vu The Binh.
La cita es una buena oportunidad para promocionar la marca nacional con esperanza de convertir al país en uno de los mejores destinos turísticos regionales, expresó el vocero.
Bajo el lema "Explorando el delta del río Rojo - origen de la cultura vietnamita", el evento impulsará los esfuerzos de ampliar los mercados, subrayó.
Agregó que durante la feria, que tendrá lugar hasta el domingo venidero, las agencias de viajes locales brindarán información sobre sus productos turísticos, y al mismo tiempo ofrecerán ventas promocionales.

Se eliminarán bancos ineficientes
De 2013 a 2015, el Banco Estatal de Vietnam (BEV) erradicará o fusionará instituciones crediticias ineficientes en el país, se conoció hoy en Hanoi mediante un comunicado oficial.
El BEV exigió a los bancos en el país revisar sus actividades internas con el fin de garantizar una operación sana del sistema financiero nacional.
Dichas medidas se aplicarán también a fondos de ahorro, filiales de bancos extranjeros, entidades de transacción y oficinas representativas de instituciones crediticias.
Los planes específicos de la reestructuración serán publicados el próximo día 30.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

