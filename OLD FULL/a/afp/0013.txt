MADRID (AFP)
                    La policía vasca arrestó este viernes en San Sebastián a seis jóvenes independentistas contra los cuales había una orden de arresto desde el martes y que se habían congregado, apoyados por cientos de personas, en el bulevar de esta ciudad.
Unas 400 personas se reunieron en el Buleva, cuando la policía intervino, este viernes por la mañana, indicó un portavoz de la Ertaintza.
"Seis de las personas buscadas fueron detenidas, los otros dos no estaban en el lugar", así como "dos de las personas concentradas por atentado a agentes de las fuerzas públicas", agregó dicho portavoz.
El tribunal madrileño de la Audiencia Nacional lanzó el 16 de abril una orden de arresto contra ocho jóvenes condenados a seis años de cárcel por pertenecer al movimiento de la juventud independentista vasca Segi.
Pocos días antes, el 8 de abril, esos militantes habían sido condenados por el Tribunal Supremo, que confirmó su condena pronunciada por la Audiencia Nacional en 2011, y anuló la condena de otros ocho.
Segi, prohibido en España -donde es considerado como el vivero de ETA- pero no en Francia, había anunciado su autodisolución el 15 de junio de 2012 y realizado su último mitin el 24 de ese mes.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  