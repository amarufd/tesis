BOSTON, EEUU (AFP)
                    Boston era escenario el viernes de un gigantesca operativo policial para capturar a un joven de 19 años de origen checheno sospechoso del atentado del lunes en el maratón junto con su hermano, abatido en la madrugada.
Toda la población de Boston (noreste de EEUU) y sus suburbios, es decir cerca de un millón de personas, recibieron la orden de permanecer en sus casas. El servicio de transporte público fue suspendido y las escuelas cerradas.
Más de 9.000 policías, muchos con protección y armamento de combate, recorrían las calles de Watertown, en el surburbio oeste de Bostón, donde tuvo lugar buena parte del operativo para capturar a los supuestos responsables del atentado el lunes en el maratón que dejó tres muertos y más de 180 heridos.
Los individuos, dos hermanos de origen checheno, fueron identificados como Tamerlan Tsarnaev, de 26 años, quien murió en el enfrentamiento con la policía, y Dzhojar Tsarnaev, de 19, objeto de una verdadera cacería humana.
Boston era escenario el viernes de un gigantesca operativo policial para capturar a un joven de 19 años de origen checheno sospechoso del atentado del lunes en el maratón junto con su hermano, abatido en la madrugada.Un policía murió y otro resultó herido en el operativo, que se desarrolló entre el jueves por la noche tarde y el viernes por la madrugada en el Instituto Tecnológico de Massachusetts (MIT) en Cambridge, cerca de Boston, y Watertown, e incluyó una persecución en auto, explosiones y tiroteos.
La policía se refirió al sospechoso prófugo como un hombre "armado y peligroso" y lo definió como un "terrorista que ha venido a matar gente". Se teme que esté en posesión de explosivos.
"Quédense en sus casas y no abran la puerta a nadie salvo a un policía adecuadamente identificado", declaró el gobernador Deval Patrick, en una orden que la mayoría de la población siguió, ya que Boston parecía una ciudad fantasma.
En cuanto al sospechoso fallecido, las autoridades señalaron que murió en un hospital de la zona al que fue trasladado tras haber recibido varios disparos y resultar herido por una explosión.
El secretario de Estado norteamericano, John Kerry, se negó a "especular" sobre el vínculo que podría tener el atentado con el origen checheno de los dos sospechosos, que pasaron su infancia en Kirguistán.
De su lado, el presidente checheno, Ramzan Kadyrov, declaró que los Tsarnaev eran desconocidos en Chechenia y que el drama es culpa de los servicios secretos estadounidenses.
"No conocemos a los Tsarnaev, no han vivido en Chechenia, han vivido y estudiado en Estados Unidos. Lo que pasó en Boston es culpa de los servicios secretos estadounidenses", sostuvo, citado por la agencia pública rusa Ria Novosti.
En efecto, los hermanos vivían desde hacía años en Cambridge y ambos manifestaron en las redes sociales que eran musulmanes creyentes.
El FBI (Oficina Federal de Investigaciones) había difundido el jueves por la tarde fotos y videos de esos dos hombres tras analizar miles de imágenes registradas en la zona donde se produjo el doble atentado.
Los individuos habían sido denominados simplemente como "Sospechoso Uno" y "Sospechoso Dos". Aparecían en las imágenes con una gorra, uno blanca y el otro negra, y llevaban mochilas.
Según el FBI, el sospechoso de gorra blanca dejó su mochila en el lugar de la segunda explosión "algunos minutos" antes del estallido cerca de la línea de la llegada del maratón.
Tras la publicación de las fotos, las autoridades intensificaron los controles en la frontera con Canadá. Y el clamor por ver las imágenes fue tan grande que, minutos después de que el FBI las subiera a su página web, ésta colapsó.
Horas antes de la conferencia de prensa del FBI, el presidente Barack Obama había prometido dar con los autores del atentado, al encabezar un servicio ecuménico en la catedral de la Santa Cruz de Boston, repleta con unas 2.000 personas.
De acuerdo con la investigación del FBI, los autores del atentado utilizaron bombas caseras, ollas a presión con clavos y metralla en su interior que dejaron una docena de amputados, además de los tres muertos.
El FBI halló en un techo de un hotel en la zona del atentado trozos de una olla a presión. También se encontraron en la escena del crimen fragmentos de una mochila de color oscuro en la que habrían ocultado las bombas caseras.
Más de 100 de los 183 hospitalizados inicialmente por el doble atentado ya fueron dados de alta. Sin embargo, una decena de personas siguen en estado crítico y necesitan nuevas operaciones para salvar sus vidas.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  