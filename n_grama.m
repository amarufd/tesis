function [ngrama] = n_grama(texto, ngram)
ngrama={};
flag=0;
ng='';
for i=1:(length(texto)-ngram+1)
    if ngram>1 && length(texto)<=(length(texto)+ngram)
        for x=0:(ngram-1)
            if flag==0
                ng=([ng char(texto(i+x))]);
                flag=1;
            else
                ng=([ng ' ' char(texto(i+x))]);
            end
        end
        ngrama=([ngrama; ng]);
        ng='';
        flag=0;
    elseif ngram==1
        ngrama=[ngrama; texto(i)];
    end
end
ngrama=ngrama';
end