function ev = cEval(varargin)

%CEVAL - continuous evaluation (for regression)
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2104.2008
%
%IN:  t: truth (seq)data obj.
%     p: prediction (seq)data obj.
%OUT: ev: evaluation

if nargin == 0 % - Default constructor
        ev.truth = [];
        ev.pred = [];
        ev = class(ev, 'cEval');
else
        if isa(varargin{1}, 'cEval')
                ev = varargin{1};
        else
                switch length(varargin)
                case 1
                        ev.truth = get_y(varargin{1});
                        ev.pred = [];
                case 2
                        ev.truth = get_y(varargin{1});
                        ev.pred =  get_y(varargin{2});
                otherwise
                        error('Too many input arguments');
                end
                ev = class(ev, 'cEval');
        end
end
