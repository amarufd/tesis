function L = corrcoefLoss(e)
%SQUARELOSS - squared law loss for continuous evaluation (for regression)
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 10032012
%
%IN:  e: cEval (cont. eval.) object
%OUT: L: average loss

    C = corrcoef(e.truth, e.pred);  
    % -- Output a loss. Elements of C are between 0 and 1
    L = 1 - abs(C(1,2));  % - Should be symmetric anyway
end