function [L l] = squareLoss(e, r, w)

%SQUARELOSS - squared law loss for continuous evaluation (for regression)
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2104.2008
%
%IN:  e: cEval (cont. eval.) object
%     r: range of input data. Default is [0 1]
%     w: weights per element (optional, e.g. for boosting purposes)
%OUT: L: average loss
%     l: element-wise loss vector 
%
% From: "Improving regressors using boosting techniques", Harris Drucker, 1997

% CHANGELOG
%-----------
% 30042008 ===> Removed the division by max. error. This is NOT a good way to compare
%               errors. Instead, to insure that  0<=L<=1 we insist that all labels are
%               in the range [0 1]. This means you need to labelNormalise your data
%               before training AND you need to post-process (prune/flatten) your
%               prediction results
% 01052008 ===> Added possibility of specifying the range in which the
%               regression values should lie. This is crucial for keeping
%               each element of L <= 1 while allowing regression values
%               to be in an arbitrary (though real) space

if nargin < 2
        r = [0 1];
end
R = r(2) - r(1);    % - Width of range

if length(e) > 1
	% -- Recursive option
	for i = 1:length(e)
			[L(i), l{i}] = radialSquareLoss(e(i), r);
	end
else
	% -- If no weights given, everything is equal
	if nargin < 3
			w = ones(size(e.pred))/length(e.pred);
	end

	l = ((e.pred - e.truth)/R).^2;

	L = l'*w;
end
