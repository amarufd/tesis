function r = mSvr(varargin)

%MSVR - constructor of mSvr object
%
% AUTHOR:       M.F. Valstar
% CREATED:      2104.2008
%
%Ways to call constructor and order of input variables:
% A: to create an empty new object: []
% B: to copy a mSvr object: [mSvr]
% C: new object with
%     1: k: kernel 
%
%OUT: r: regressor object
%
%Typical use: featureselect, parameteroptimisation, train, test. Or use crossvalidate.
%
% CHANGELOG
% --------------
% 20120319 ==> fixed setting r.k when calling mSvr with more than 1 argument
%

if nargin == 0 % - Default constructor
        r.r = [];
        r.r.epsilon = 0.01;
        r.k = 'rbf';
        r.p = [1 inf r.r.epsilon];  % - List of parameter values
        r.pNames = {'sigma', 'C', 'epsilon'};  % - List of parameter names
        r = class(r, 'mSvr');
else
        if isa(varargin{1}, 'mSvr')
                r = varargin{1};
        else
                switch length(varargin)
                case 1
                        r.r = [];
                        r.r.epsilon = 0.01;
                        r.k = varargin{1};
                        r.p = [1 inf r.r.epsilon];  % - List of parameter values
                        r.pNames = {'sigma', 'C', 'epsilon'};  % - List of parameter names
                case 2
                        r.r = [];
                        r.k = varargin{1};
                        r.p = varargin{2};
                        r.pNames = {};
                case 3
                        r.r = [];
                        r.k = varargin{1};
                        r.p = varargin{2};
                        r.pNames = varargin{3};
                otherwise
                        error('Too many input arguments');
                end
                r = class(r, 'mSvr');
        end
end
