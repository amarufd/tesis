function r = setSigma(r, k)

%SETSIGMA - set the kernel sigma value of a mSvr to k

r.r.rbf = k;