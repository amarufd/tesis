function r = setC(r, C)

%SETC - set the kernel sigma value of a mSvr to k

r.r.C = C;