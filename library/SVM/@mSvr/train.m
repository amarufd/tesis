function r = train(r, d)

%TRAIN - trains an mSvr regressor object
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 0204.2008
% 
%IN:  r: mSvr object
%     d: data object. Can have multiple (continuous) labels
%OUT: r: trained mSvr object

% "libsvm_options:\n"
%	"-s svm_type : set type of SVM (default 0)\n"
%	"	0 -- C-SVC\n"
%	"	1 -- nu-SVC\n"
%	"	2 -- one-class SVM\n"
%	"	3 -- epsilon-SVR\n"
%	"	4 -- nu-SVR\n"
%	"-t kernel_type : set type of kernel function (default 2)\n"
%	"	0 -- linear: u'*v\n"
%	"	1 -- polynomial: (gamma*u'*v + coef0)^degree\n"
%	"	2 -- radial basis function: exp(-gamma*|u-v|^2)\n"
%	"	3 -- sigmoid: tanh(gamma*u'*v + coef0)\n"
%	"	4 -- precomputed kernel (kernel values in training_instance_matrix)\n"
%	"-d degree : set degree in kernel function (default 3)\n"
%	"-g gamma : set gamma in kernel function (default 1/num_features)\n"
%	"-r coef0 : set coef0 in kernel function (default 0)\n"
%	"-c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)\n"
%	"-n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)\n"
%	"-p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)\n"
%	"-m cachesize : set cache memory size in MB (default 100)\n"
%	"-e epsilon : set tolerance of termination criterion (default 0.001)\n"
%	"-h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)\n"
%	"-b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)\n"
%	"-wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)\n"
%	"-v n : n-fold cross validation mode\n"
%	"-q : quiet mode (no outputs)\n"
%   gamma = 1/(2*sigma^2);

    if isa(d, 'seqdata')
         d = data(d);
    end
    sigma = r.p(1);
   
    c = r.p(2);
    if c == Inf
        c = 9999;
    else
        c = round(c);
        c = max(1, c);
    end
    eps = r.p(3);
    gamma = 1/(2*sigma^2);
    
    kernel = r.k;
    switch kernel
        case 'linear'
            kernelNr = 0;
            libsvm_opt = sprintf('-s 3 -t %u -c %u -b 0 -p %f',kernelNr,c,eps);
        case 'rbf'
            kernelNr = 2;
            fprintf(1, '\tTraining SVR with kernel %s, sigma=%f, C=%u, epsilon=%f\n', kernel, sigma, c, eps);
            libsvm_opt = sprintf('-s 3 -t %u -g %u -c %u -b 0 -p %f',kernelNr,gamma,c,eps); 
        case 'intersection'
            kernelNr = 5;
            libsvm_opt = sprintf('-s 3 -t %u -c %u -b 0 -p %f',kernelNr,c,eps); 
        otherwise
            error('Go and implement it yourself, you psoli!');
    end
    
    m = svmtrain(get_y(d),get_x(d),libsvm_opt);
    r.r = m;
end
