function d = display(r)

%DISPLAY - displays an mSvr object
%
% AUTHOR:       M.F. Valstar
% CREATED:      1301.2010
%
%IN:  r: adaBoostReg object
%OUT: d: string

if length(r) > 1
        d = sprintf('\nArray of %u mSvr objects\n', length(r));
        disp(d);
else
        d = sprintf('\nmSvr object\n\nKernel:\t\t%s\nC:\t\t\t%f\nSigma:\t\t%f\nEpsilon:\t%f\n', r.k, r.p(2), r.p(1), r.p(3));
        disp(d);
end