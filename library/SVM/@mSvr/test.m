function [p prob] = test(r, d, id)

%TEST - tests a mSvr
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2104.2008
%
%IN:  r: trained mSvr object
%     d: data/seqdata object. Can have only a single label
%     id: optional. identifier of the preloaded regression model
%OUT: p: mSvr output 
%     prob: probability



    if isa(d, 'seqdata')    
        p = seqdata;
        prob = seqdata;
        y = get_y(d);
        if size(y,1) > 1 && size(y,2) > 1
            error('Input data label should have single dimension!');
        end
        if size(y,2) > 1
            y = y'; % - svmpredict expects vector, not column vector
        end
        [predicted_label, ~, prob_est] = svmpredict(y,get_x(d),r.r);        
        p = seqdata(zeros(numel(predicted_label), 1), predicted_label, get_I(d));
        prob = seqdata(zeros(numel(prob_est), 1), prob_est, get_I(d));
    else
        if exist('id','var' )
            [predicted_label, ~, prob] = svmpredict_mod_multiple( get_y(d),get_x(d), id );               
        else
            [predicted_label, ~, prob] = svmpredict(get_y(d),get_x(d),r.r,'-b 0');        
        end
        p = data([], predicted_label);
    end
end
