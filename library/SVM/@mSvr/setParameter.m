function r = setParameter(r, n, v)

%SETPARAMETER - sets parameters of a mSvr object
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2204.2008
%
%IN:  r: regressor object
%     n: parameter indices
%     v: parameter values
%OUT: r: regressor with updated parameter values

r.p(n) = v;