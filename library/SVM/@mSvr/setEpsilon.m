function r = setEpsilon(r, p)

%SETEPSILON - set the kernel sigma value of a mSvr to k

r.r.epsilon = p;