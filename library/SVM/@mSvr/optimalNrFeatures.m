function bestNrF = optimalNrFeatures(s, d, etype, f)
   % -- Find the optimal number of features using SVM. 
   %
   %IN:  s: mSvr(not really used)
   %     d is seq. data object
   %     etype is the error type, e.g. 'square'
   %     f is the set of ordered features

    n_I = nrSequences(d);
    I_1 = 1:2:(n_I-1);
    I_2 = 2:2:n_I;
    
    d1 = sequenceSubset(d, I_1);
    d2 = sequenceSubset(d, I_2);
   
    minError = 1;  
    bestNrF = 1;
    
    n = length(f);
    fig = figure;
    hold on;
    i = 0;
    while 2^(i-1) < n 
        nf_i = 2^i;
        nf_i = min(nf_i, n);    % - Don't try to use more features than we have
        f_i = f(1:nf_i);

        % -- Get score for this set of features
        err = featSetError(d1, d2, f_i, etype);
        scatter(nf_i, err);
        fprintf(1, '\\n\t\tError for using %u selected features:\t%.4f\n\n', length(f_i), err);
        if err < minError
            minError = err;
            bestNrF = nf_i;
        end
        i = i + 1;
    end
end

function score = featSetError(d1, d2, sf, etype)
% -- calculates score for a certain set of features
% -- Test 1
    reg = regressor(mSvr);
    paramopt_reduce_data = 4;   % - Reduce even more for parameter optimisation
    d1 = featureSubset(d1, sf);
    d2 = featureSubset(d2, sf);
    
    reg = parameteroptimisation(reg, d1, 5, etype, paramopt_reduce_data);
    reg = train(reg, d1);
    v = test(reg, d2);
    
    % -- Train on d1, test on d2

	
	Y = get_y(d2);
	rr = [min(Y), max(Y)];
	
	v = clipLabels(v, rr);
	ev1 = ceval(d2, v);
	sc1 = squareLoss(ev1, rr);
	fprintf(1, '\n\tTest 1 for dist has square loss %.4f\n', sc1);


    % -- Test 2
    reg = regressor(mSvr);
    reg = parameteroptimisation(reg, d2, 5, etype, paramopt_reduce_data);
    reg = train(reg, d2);
    v = test(reg, d1);
    
    % -- Train on d2, test on d1
	Y = get_y(d1);
	rr = [min(Y), max(Y)];
	v = clipLabels(v, rr);
	ev2 = ceval(d1, v);
	sc2 = squareLoss(ev2, rr); 
	fprintf(1, '\n\tTest 2 for dist has square loss %.4f\n', sc2);

    score = mean([sc1 sc2]);

end