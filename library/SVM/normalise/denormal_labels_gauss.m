function labels = denormal_labels_gauss( lab_norm, m, s )
% lab = denormal_labels_gauss( lab_norm, m, s )
%

% denormalise from std = 1
labels = lab_norm * s;

% add mean
labels = labels + m;

