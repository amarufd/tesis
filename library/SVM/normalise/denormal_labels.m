function lab_denorm = denormal_labels( labels, t, scl )
% lab_denorm = denormal_labels( labels, t, scl )


lab_denorm = labels - t;
lab_denorm = lab_denorm / scl;

