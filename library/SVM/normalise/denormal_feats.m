function [ x, m, s ] = denormal_feats( xn, m, s )

x = xn .* repmat( s, size(xn, 1), 1);
x = x + repmat( m, size(x,1), 1);
