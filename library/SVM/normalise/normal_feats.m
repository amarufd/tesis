function [ xn, m, s ] = normal_feats( x, m, s )
% [ xn, m, s ] = normal_feats( x, m, s )
%
% IN:
% x (the features) is a required input
% m and s are optional. If input, they are applied to the data (typically,
% m and s are computed during training and applied to test data)

if  nargin < 2
    m = mean( x );
end
if nargin < 3
    s = std( x );
end

s(s==0) = 1;
xn = x - repmat( m, size(x,1), 1);
xn = xn .* repmat(1./s, size(x, 1), 1);

