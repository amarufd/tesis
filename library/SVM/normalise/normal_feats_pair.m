function [ x_tr, x_test ] = normal_feats_pair( x_tr, x_test )
% function [ x_tr, x_test ] = normal_feats( x_tr, x_test )

% check if the right format (might still not be the right one, but if it is wrong it will say)
if size( x_tr, 2 ) ~= size( x_test, 2 )
    error('number of features not consistent');
end

% init.
startup_work( 'michel' );
startup_work( 'spider' );

% normalise the training data
d_tr = data( x_tr, [] );
[ d_tr, m, s] = normal( d_tr );
x_tr = get_x( d_tr );

% normalise the test data as well (using tr. data norm. info)
d_test = data( x_test, [] );
d_test = normal( d_test, m, s );
x_test = get_x( d_test );

% cov( x_tr( :, 2 ) )
