function [ lab_norm, t, scl ] = normal_labels( labels, lab_range, t, scl )
% [ lab_norm, t, scl ] = normal_labels( labels, range, t, scl )
% like the one at @data, but without spider involved

if nargin == 1
    lab_range = [-1,1];
end
if nargin <= 2

    % normalise the training labels
    scl = ( lab_range(2)-lab_range(1) )/(max( labels ) - min( labels ));
    lab_norm = labels * scl;
    t = lab_range(2) - max( lab_norm );
    lab_norm = lab_norm + t;

else
    
    % normalise the test labels accordingly
    lab_norm = labels * scl;
    lab_norm = lab_norm + t;
    
end