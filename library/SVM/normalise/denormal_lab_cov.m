function cov_pred_denorm = denormal_lab_cov( cov_pred, scl )

cov_pred_denorm = cov_pred / (scl^2) ;

