function [ lab_norm, m, s ] = normal_labels_gauss( labels, m, s )
% [ lab_norm, m, s ] = normal_labels_gauss( labels, m, s )
%
% m and s are optional

% remove mean
if nargin == 1
    m = mean( labels );
end
lab_norm = labels - m;

% normalise to std = 1
if nargin <= 2
    s = std( lab_norm );
end
lab_norm = lab_norm / s;