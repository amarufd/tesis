function [ cl, norminfo ] = train_SVM_withNorm( x, y, numExs )
% no feature selection for now!


if exist( 'numExs','var')
    
    indpos = find( y == 1 );
    indneg = find( y == -1 );
    numExs_pos = round( numExs / 3 );
    
    tempPos = randperm( length( indpos ), numExs_pos );
    indpos = indpos( tempPos );
    
    tempNeg = randperm( length( indneg ), 2*numExs_pos );
    indneg = indneg( tempNeg );
    
    x = [ x( indpos, : ); x( indneg, : ) ];
    y = [ y( indpos ); y( indneg ) ];

end



% -- NORMALISE features and labels
[ xn, mf, sf ] = normal_feats( x );
% [ yn, ml, sl ] = normal_labels_gauss( y );
norminfo.feat.mf = mf;
norminfo.feat.sf = sf;
% norminfo.lab.ml = ml;
% norminfo.lab.sl = sl;


% -- TRAIN
cl = mSvm;
d_x = data( xn, y );
cl = parameteroptimisation( cl, d_x, 3 );
cl = train( cl, d_x );



