function [ ypred, pr_est ] = test_SVM_withNorm( class_txt, norminfo, x )


% -- NORMALISE TEST
xn = normal_feats( x, norminfo.feat.mf, norminfo.feat.sf );

% -- test
d_test = data( xn, zeros( size( xn, 1 ), 1 ));
[ ypred, pr_est ] = test( class_txt, d_test );
ypred = get_y( ypred );
