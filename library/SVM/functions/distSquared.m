function D2 = distSquared(X,Y)
% %
% nx	= size(X,1);
% ny	= size(Y,1);
% %
% 
% % these steps could be memory optimized
% D2 = (sum((X.^2), 2) * ones(1,ny)) + (ones(nx, 1) * sum((Y.^2),2)');
% D2 = D2 - 2*X*Y';

 D2 = -2*X*Y';
 D2 = bsxfun(@plus, D2, sum((X.^2),2));
 D2 = bsxfun(@plus, D2, sum((Y.^2),2)');

end