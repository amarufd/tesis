function [D Dc] = condense(d, I)

% Condense -- condenses D+1 points (instances with indices I in set X) to their mean, returning reduced sets
% X and Y (reduced by D instances)
%  AUTHOR:	M.F. Valstar
%  CREATED:	31012007
%
%IN:  d: original data
%     I: set of hyperdrops, indices of the data points [cell array]

% CHANGELOG
%-----------
% 27022007 ===> Changed calls to subset and the get_x parts to speed things up 

% -- Empty condensed data..
Dc = data();
n = length(I);
Irem = 1:nrSamples(d);	% -- remaining instances
for i = 1:n
	Irem = setdiff(Irem, I{i});
	% -- Mean should be computed from original data
%  	mx = mean(get_x());
	di = subset(d, I{i});
	mx = mean(di.X);
	y = di.Y(1);
%  	y = get_y(subset(d, I{i}(1)));
	% -- Update condensed data
	Dc = cat(Dc, data(mx, y));
end

% -- resulting data is original data with the hyperdrop data removed and the condensed hyperdrop data added
Drem = subset(d, Irem);
D = cat(Drem, Dc);
