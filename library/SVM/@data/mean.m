function m = mean(d)

%MEAN - returns mean of data
%
% AUTHOR:	M.F. Valstar
% CREATED:	1005.2007
%
%IN:  d: spider data object
%OUT: m: mean [1 x dim]

m = mean(d.X);