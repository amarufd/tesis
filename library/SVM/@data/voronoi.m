function h = voronoi(d, dim)

%VORONOI - front-end for spider data
%
% AUTHOR:	M.F. Valstar
% CREATED:	0507.2007
%
%IN:  d: spider data
%     dim: dimensions to show. Default is [1 2]
%OUT: h: handles to line objects

if nargin < 2
	dim = [1 2];
end

x = d.X(:,dim(1));
y = d.X(:,dim(2));
figure
h = voronoi(x', y');
hold on;
I = find(d.Y==1);	% -- Find the positive examples
scatter(d.X(I,dim(1)), d.X(I,dim(2)), 'rs'); % -- And plot them red