function [y l] = condense(x, t)

%CONDENSE - spider data condensation method. This is NOT the condense step of the encapsulation algortithm!
%			Instead, it's an alternative algorithm for data-reduction
%
% AUTHOR:	M.F. Valstar
% CREATED:	2003.2007
%
%IN:  x: data
%     t: condensation target
%OUT: y: condensed data
%     l: final condensation level

[X Y l] = condensation(get_x(x), get_y(x), t, round(nrSamples(x)/10), 0);
y = data(X, Y);