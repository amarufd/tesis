function N = voronoiNeighbours(d, I)

%VORONOINEIGHBOURS - returns a cell array of voronoi-neighbours. 
%
% AUTHOR:	M.F. Valstar
% CREATED:	0507.2007
%
%IN:  d: data (spider data object) of n elements and D dimensionality
%     I: optionally, specify a number of points to find the neighbours for
%OUT: N: cell array of length n containing all voronoi neighbours: the points of adjacent Voronoi cells
%
% See also: VORONOI, VORONOIN, DELAUNEY

% TODO
%------
% 05072007 ===>	Add check to see whether we have found all neighbours so can
%				stop searching
if nargin < 2
	I = 1:n;
end

[n D] = size(d.X);

% -- Get all vertices and, more importantly, which points use what vertices
[V C] = voronoin(d.X);

% -- Initialise neighbour array
for p = 1:n
	N{p} = [];
end

% -- Find for each point which points are neighbours.
% basically, we check which points share the same lines (that is, two vertices)
% For D-dimensions, they should share the same hyperplane, thus D-vertices

% -- for each point in d
for p = I
	v_p = C{p};
	for c = setdiff(1:n, p)
		v_c = C{c};
		if shares(v_p, v_c, D)
			N{p}(end+1) = c;
%  			if allNeighboursFound(v_p, N{p}, C)
%  				break
%  			end			
		end
	end
end


function b = shares(v1, v2, D)
% Given a D-dimensional space, checks whether two sets of vertices share
% the same hyperplane, i.e., D vertices
%
%OUT: b: 1 if they share, 0 if not

if length(intersect(v1, v2)) >= D
	b = 1;
else
	b = 0;
end

function b = allNeighboursFound(v_p, N, C)
nb = [];
for n = N
	nb = union(nb, C{n});
end
if isempty(setdiff(v_p, nb))
	b = 1;
else
	b = 0;
end 