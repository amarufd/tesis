function [d Irem Idrops c] = condense(d, p, m, plotornot)

%CONDENSE 	  - reduces the size of the input data by means of condensation
%				This is to be called if sample size/dimensionality is fairly small
%				In cas of large chunks of data, use encapsulation for cheeky ways
%				of dealing with size problems
%
% AUTHOR:	M.F. Valstar
% CREATED:	0907.2007
%
%IN:  d: data
%     p: stop criterium: percentage to condense to 0<p<1
%     m: stop criterium: absolute max. nr. of rounds to attempt
%     condensation. Default is 10% of nr instances of d
%     plotornot
%OUT: d: condensed (reduced) data
%     Irem: indices of surviving instances
%     Idrops = cell array of hyperdrop instance indices 
%     c: final condensation level

% CHANGELOG
%-----------
% 05072007 ===>	This is version 2 of condensation. Check v1 for the old version
% 				This version uses voronoi diagrams to find encapsulated cells
% 11072007 ===> Read on the website of qhull
%               (http://www.qhull.org/html/index.htm#when) that the max nr 
%               dimensionality is 7 for voronoi. So this implementation 
%               uses 7 dimensions. This is suboptimal (again) but to do 
%               it differently probably means writing my own shit

[n D] = size(d.X);
c = 1;	% -- Current condensation level
Irem = 1:n; Idrops = [];
N = 100;	% -- The number of points to check for encapsulation per iteration

if nargin < 2
	p = 0.001;
end
if nargin < 3
	m = round(0.1*n);
end
fprintf(1, '\nStarting condensation on %u-dimensional data with %u instances', D, n);

% -- Remove duplicates
fprintf(1, '\n\tRemoving duplicates...');
[d Irem Idrops] = removeDuplicates(d, Irem, Idrops);
nn = size(d.X, 1);
fprintf(1, '\t%u instances removed\n', n-nn);

cnt = 1; done = 0;
while ~done
	[d Irem Idrops] = removeDrops(d, N, Irem, Idrops);
	c_old = c;
	c = size(d.X, 1)/n;
	fprintf('Encapsulation condensation level is %f\n', c);
	if c < c_old	% -- We have improvement
		cnt = 1;	% -- So reset counter
	else
		cnt = cnt + 1;
	end
% -- Stop criterium
	if c < p || cnt > 3 || size(d.X,1) <= D
		done = 1
	end
end




function [d Irem Idrops] = removeDrops(d, k, Irem, Idrops)
% -- Iteration to remove drops that are encapsulated
[n D] = size(d.X);
I = ceil(n*rand(1,k));
N = voronoiNeighbours(data(d.X(:,1:7)), I);
b = isMonoClass(d, N);

% -- Now remove the points that are encapsulated (i.e., have monoclass voronoi neighbours)
I_die = find(b == 1);
I_live = find(b == 0);
Idrops = horzcat(Idrops, Irem(I_die));
Irem = setdiff(Irem, Irem(I_die));
d = data(d.X(I_live,:), d.Y(I_live));

function [d, Irem, Idrops] = removeDuplicates(d, Irem, Idrops)
s = std(d);
D = normal(d);
for i = 1:size(d.X,1)
	dist = distance(D, data(D.X(i,:), D.Y(i)));
    dist(i) = 1;% -- Make sure we don't throw away ourself
	I_die = find(dist==0);
	Idrops = union(Idrops, Irem(I_die));
	Irem = setdiff(Irem, Irem(I_die));
end
d = data(d.X(Irem,:), d.Y(Irem,:));

function I = closest(d, dt)
% -- Returns the indices of the nearest neighbours of d to dt
D = size(get_x(d), 2);
y = distance(d, dt);
[v Id] = sort(y);
I{1} = Id(1:D+1);

function d = distance(D, dt)
% -- Euclidian distance of all elements in set X to point x
Xd = (D.X - repmat(dt.X, size(D.X,1), 1)).^2;
d = sum(Xd, 2);