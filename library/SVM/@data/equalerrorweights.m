function w = equalerrorweights(d);

%EQUALERRORWEIGHTS - returns a set of weights such that sum(wp) == sum(wn), spider data version
%
% AUTHOR:   M.F. Valstar
% CREATED:  25012007
%
%IN:  d: spider data object
%OUT: w: weight, per label w e <0,1>
y = get_y(d);
ns = length(y);
w = zeros(ns,1);
P = length(find(y == 1));
N = length(y)-P;
 
w(find(y == 1)) = N/P;
w(find(y ~= 1)) = 1;

w = w./sum(w);