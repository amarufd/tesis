function y = binarylabels(x)
% -- turns two arbitrary labels into binary labels, with the lowest label assigned the positive class

Y = get_y(x);
low = min(Y);
high = max(get_y(x));

if (length(find(Y == low)) + length(find(Y == high))) < length(Y)
	error('Probably more than two classes in input data!');
end
if low == high
	warning('Unary labeled data: assigning all labels to positive class!');
	Y(:) = 1;
else
	Y(find(get_y(x)==low))= 1;
	Y(find(get_y(x)==high)) = -1;
end
y = data(get_x(x), Y);