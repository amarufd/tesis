function [d sf] = selectFeatures(d, m, s)

%SELECTFEATURES - reduce data using a specified method
%
%IN: d: data to select features from
%    m: method: 'fwdSelectGBoost' or 'gboost' supported
%    s: classifier to use in case of fwdSelectGboost
%OUT: d: reduced data
%     sf: indices of selected features

error('This function is obsolete. Use reduceDimensionality instead!');

switch m
    case 'fwdSelectGBoost'        
        if ~exist('s', 'var')
            error('Need to specify what classifier to use!');
        end
        % -- Order features according to gentleboost
        of = gentleBoostOrder(d);

        % -- Re-order data
        d = data(d.X(:,of), d.Y);

        % -- Find optimal nr of features
        %nopt = optimalNrFeatures(d, etype, f);
        nopt = optimalNrFeatures(d, s);
        d = data(d.X(:,1:nopt), d.Y);
        sf = of(1:nopt);
    case 'gboost'
        [d sf] = gentleBoost(d);
    otherwise
        error('Unknown method: %s', m);
end