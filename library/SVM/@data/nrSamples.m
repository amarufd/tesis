function n = nrSamples(d)

%NRSAMPLES - returns the number of samples of d
%
% AUTHOR:	M.F. Valstar
% CREATED:	25012007
%
%IN:  d: data object 
%OUT: n: nr of instances in d

% CHANGELOG
%-----------
% 23022007 ===> Removed get_x function call, instead addressing data directly. Speeds
%				up computations considerably
% 30072007 ===> Added support for arrays of data. Returns the sum of nrSamples of all 
%				elements of the array
if length(d) > 1
	n = 0;
	for i = 1:length(d)
		n = n + nrSamples(d(i));
	end
else
	n = size(d.X, 1);
	if n == 0
    	n = size(d.Y, 1);
	end
end