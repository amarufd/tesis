function [d I] = unique(d)

%unique - removes all duplicates from a data object. Output is in same order as input
%
% AUTHOR:	M.F. Valstar
% CREATED:	3007.2007
%
%IN:  d: data object 1
%OUT: d: data object with unique data entries
%     I: indices of original data that are unique
%
% See also: data/merge


if length(d) > 1
    for i = 1:length(d)
    	[d(i), I{i}] = unique(d(i));
    end
else
	[v I] = unique(d.X, 'rows');
	% -- Initialise empty dataset
	d = data(d.X(I,:), d.Y(I));
end
