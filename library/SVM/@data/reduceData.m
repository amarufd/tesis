function d = reduceData(d, f)

%REDUCEDATA - reduce data by a factor f
%
% AUTHOR:   M.F. Valstar
% CREATED:  1511.2007
%
%IN:  d: data
%     f: integer factor to reduce with, so |d|=|d|/f
%     R: regression (bool)
%OUT: d: reduced data
%
%NB: we try to re-balance the data in the process if this is a
%classification problem.

if nargin < 3
	R = 0;
end

N = nrSamples(d);
[p I_p] = nrPositive(d);    % - nr and indices of positive elements in d
[n I_n] = nrNegative(d);    % - nr and indices of negative elements in d

N_n = floor(N/f);

I = ones(1, N_n);

if length(unique(get_y(d))) > 2
	% -- Regression data
	r = randperm(N);
	I = r(1:N_n);
else
	if p < N_n/2
		% -- Include all positive elements
		I(1:p) = I_p;
		r = randperm(n);
		I(p+1:end) = I_n(r(1:N_n-p));
	else
		% -- Get half of the elements from pos, half from neg class
		h = round(N_n/2);
		r = randperm(p);
		I(1:h) = I_p(r(1:h));
		r = randperm(n);
		I(h+1:end) = I_n(r(1:N_n-h));
	end
end
d = data(d.X(I,:), d.Y(I,:));
