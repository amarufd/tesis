function [d c] = specialise(d, n, i)

%SPECIALISE - returns a specialised dataset: the ith combination of 1vs1 classes
%
% AUTHOR:	M.F. Valstar
% CREATED:	0404.2007
%
%IN:  d: data
%     n: nr of classes in data
%     i: the ith combination
%OUT: d: specialised set
%     c: The combination used

S = combination2(1:n)
c = S(i,:);

I = find(d.Y == c(1) | d.Y == c(2));
d = subset(d, I);