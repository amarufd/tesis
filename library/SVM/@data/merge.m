function d = merge(d, d_m)

%merge - merges two spider data objects. Unlike cat, only elements of d2 that are not equal 
% to the elements of d1 are added
%
% AUTHOR:   M.F. Valstar
% CREATED:  2707.2007
%
%IN:  d: data object
%     d_m: data object to add
%OUT: d: merged data object
%
% See also: data/cat

% CHANGELOG
%-----------
% 27072007 ===> Updated merging: Now checks for duplicate data
% 21092007 ===> Updated merging: faster duplicate checking by using 'unique'

d = unique(cat(d, d_m));