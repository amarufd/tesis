function d = balanceBinaryLabelData(d, r, m)

%BALANCEDATA -  Attempts to create a set of equal amount or active/inactive sequences
%                Operates on binarly labeled data with -1, 1 labeling 
%
% AUTHOR:   M.F. Valstar
% CREATED:  06.11.2008
%
%IN:  d: data with only a single column of binary labels 
%     r: ratio of neg/pos examples
%     m: method to use: choice of 'oversample' or 'undersample'
%OUT: d: set with, if possible, as many positive as negative samples.
%Positive samples have priority and will all be included

Y = get_y(d);
if size(Y, 2) ~= 1
        error('Input data must have exactly a single column of label data');
end

if isempty(length(find(Y == 1)))
        error('Input data does not contain elements of the active class');
end

if ~exist('r', 'var')
    r = 1;
end

% -- Find subset of positive examples
I_act = find(d.Y == 1);
n_act = numel(I_act);
I_nonact = setdiff(1:nrSamples(d), I_act);
n_nonact = numel(I_nonact);

switch m
    case 'undersample'  % -- Note that this implementation is only correct if n_act < n_non-act
        n_nonact_trg = min(length(I_nonact), r*n_act); % - Max. double the nr of positive samples
        I = union(I_act, I_nonact(randperm(n_nonact_trg)));
        d = subset(d, I);
    case 'oversample'
        if n_act < n_nonact
            dif = n_nonact - n_act;
            I = I_act(ceil(rand(dif, 1)*n_act));
        else
            dif = n_act - n_nonact;
            I = I_nonact(ceil(rand(dif, 1)*n_nonact));
        end
        d = data(vertcat(d.X, d.X(I,:)), vertcat(d.Y, d.Y(I,:)));
    otherwise
        error('Unknown data balancing method: %s', m);
end