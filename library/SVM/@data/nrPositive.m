function [n I] = nrPositive(d)

%NRPOSITIVE - returns the nr of positive elements of d and their indices
%
% AUTHOR:   M.F. Valstar
% CREATED:  1511.2007
%
%IN:  d: data
%OUT: n: nr of positive elements in d
%     I: the indices of positive elements in d

I = find(d.Y == 1);
n = length(I);
