function d = subsample(d, f)
%SUBSAMPLE - subsample data (form of data reduction, but not random in any
%way, and no class balancing performed in any way either). 
%PRE: d is data, and f is a subsample factor, f \in {2,3,4...N}

    if (f-round(f)) ~= 0
        error('Subsample factor must be an integer >= 2, aborting');
    end
    if f < 2
        error('No point in performing subsampling with a subsample factor < 2, aborting');
    end
    
    d = data(d.X(1:f:end, :), d.Y(1:f:end));

end