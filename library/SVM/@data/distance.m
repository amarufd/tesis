function d = distance(D, dt)

%DISTANCE - computes the distance between a set of data D and a single data instance dt
%
% AUTHOR: M.F. Valstar
% CREATED: 2302.2007
%
%IN:  D: dataset
%     dt: one datum
%OUT: d: distance of datum to every instance of D

maxSize = 3000;
N = nrSamples(D);

if N < maxSize
	Xd = (D.X - repmat(dt.X, size(D.X,1), 1)).^2;
else
	n = ceil(N/maxSize);
	Xd = zeros(size(D.X));
	cnt = 0;
	for i = 1:n-1
		ind = cnt+1:cnt+maxSize;
		Xd(ind,:) = (D.X(ind,:) - repmat(dt.X, length(ind), 1)).^2;
		cnt = cnt+maxSize;
	end
	ind = cnt+1:N;
	Xd(ind,:) = (D.X(ind,:) - repmat(dt.X, length(ind), 1)).^2;
end

d = sqrt(sum(Xd, 2));