function [part1, part2] = divide(dat, perc, balanced, random)

%data.divide -- divides a data object into two partitions. 
%
% AUTHOR:	S. Kaltwang
% CREATED:	02032012
%
%IN:  dat             -- spider data object
%	  perc=0.5        -- percentage of number of samples in part1 (0 < perc < 1)
%     balanced=true   -- if data of different classes should be balanced
%     random=true     -- divide samples randomly (otherwise: first samples in part1, then part2)
%
%OUT: part1, part2    -- indeces of samples in part1 and part2

if nargin<2 || isempty(perc)
    perc = 0.5;
end

if nargin<3 || isempty(balanced)
    balanced = true;
end

if nargin<4 || isempty(random)
    random = true;
end

if ~random
    rand_h = @(x) 1:x; % <-- overwrite random permutation with ascending order
else
    rand_h = @randperm;
end

assert(isa(dat,'data'));

if balanced
    labels = get_y(dat);
    assert(~isempty(labels));
    l_list = unique(labels)';
    num_l= size(l_list,2);

    part1 = [];
    part2 = [];
    for act_l=1:num_l    
        fin = find(labels==l_list(act_l));
        num = sum(labels==l_list(act_l));
        num1 = round(num*perc);
        perm = rand_h(num); 
        perm = fin(perm);
        part1=[part1; perm(1:num1)];
        part2=[part2; perm(num1+1:end)];
    end

    part1 = sort(part1);
    part2 = sort(part2);
else
    num = get_dim(dat);
    perm=rand_h(num);
    num1 = round(num*perc);

    part1=perm(1:num1);
    part2=perm(num1+1:end);
end
        
end
