function [D, m, s] = normal(d,m,s)

% Normalizes the feature matrix of a data object, all columns to mean 0 
% and standard deviation 1...
%IN:  d: data object
%     m: mean to normalise with [OPT]
%     s: std to normalise with [OPT]
%OUT: y: normalized vector/matrix
%     m: old mean
%     s: old std

% CHANGELOG
%-----------
% 10052007 ===> removed get_x and get_y calls: not necessary for a method

if  nargin < 2
    m = mean(d.X);
end
if nargin < 3
    s = std(d.X);
end

% -- Compensate for std(x) = 0
s(s==0) = 1;
x = d.X - repmat(m, size(d.X,1), 1);
y = x.*repmat(1./s, size(x, 1), 1);

D = data(y,d.Y);