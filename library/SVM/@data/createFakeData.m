function d = createFakeData(d, n)

%CREATEFAKEDATA - dirty little function that creates data
%               for a non-existent class, making sure there
%               is no overlap with the real data

if length(find(d.Y>0)) == 0 % - No positiveclass
        fc = 1;
else
        if length(find(d.Y <=0)) == 0 % - No negative class
                fc = -1;
        else
                warning('Data has elements of both classes already');
        end
end

if nargin < 2
        n = 100;
end

s = std(d); 
m = mean(d);

fd = data(repmat(m, n,1) + 10*repmat(s,n, 1).*rand(n,length(s)), fc*ones(n, 1));
d = cat(d, fd);
I = randperm(nrSamples(d));
d = data(d.X(I,:), d.Y(I,:));
