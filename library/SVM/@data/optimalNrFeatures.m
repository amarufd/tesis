function bestNrF = optimalNrFeatures(d, s)
   % -- Find the optimal number of features using SVM. 
   %
   %PRE: d contains ordered feature data (i.e. sorted in descending order of
   %importance)
   %     d is a sequence data object
   %     s is a classifier to use for evaluation

    I = randperm(nrSamples(d));
	h = round(nrSamples(d)/2);
    I_1 = I(1:h);
    I_2 = I(h+1:end);
    nf = size(d.X, 2);
    
    d1 = subset(d, I_1);
    d2 = subset(d, I_2);
   
    maxScore = 0;  
    bestNrF = 1;
    done = 0;
    step = round(nf/5);
    minim = round(0.1*nf);
    maxim = nf;
    fig = figure;
    hold on;
    i = 0;
    while ~done 
        for nf_i = minim:step:maxim
            i = i + 1;
            f_i = 1:nf_i;
        
            % -- Get score for this set of features
            score = featSetError(s, d1, d2, f_i);
            scatter(nf_i, score);
            drawnow;
            fprintf(1, '\n\t\Score for using %u selected features:\t%.4f\n\n', nf_i, score);
            if score > maxScore || (score == maxScore && nf_i < bestNrF)	% - Prefer low-dimensional results
                maxScore = score;
                bestNrF = nf_i;
            end
        end
        if step == 1
            done = 1;
        else
            newStep = round(step/10);
            minim = max(bestNrF-step+newStep, 1);
            maxim = min(bestNrF+step-newStep, nf);
            step = newStep;
        end
    end
    close(fig);
end

function score = featSetError(s, d1, d2, sf)
    
% -- calculates score for a certain set of features
% -- Test 1
    s = parameteroptimisation(s, featureSubset(d1, sf), 5);
    s = train(s, featureSubset(d1, sf));
    [R2, u] = test(s, featureSubset(d2, sf));
    u = u(:,1);% - Take only probability of first class (pos)
    
    % -- Test on d2
	ob = getErrorEst(s);
    switch ob    % - Which loss function to use
        case 'f1'
            ev = bineval(d2, R2);
            sc1 = fmeasure(ev);
        case '2afc'
            sc1 = Calc2AFC(u, get_y(d2));
        case 'crate'
            ev = bineval(d2, R2);
            sc1 = crate(ev);
        otherwise
            error('Loss function not implemented');
    end
        
	fprintf(1, '\n\tTest 1 for dist has %s score %.4f\n', '2afc', sc1);


    % -- Test 2
    
    s = parameteroptimisation(s, featureSubset(d2, sf), 5);
    s = train(s, featureSubset(d2, sf));
    [~, u] = test(s, featureSubset(d1, sf));
    u = u(:,1);% - Take only probability of first class (pos)
    
    % -- Train on d2, test on d1
    switch ob    % - Which loss function to use
        case 'f1'
            ev = binEval(d1, R2); % - Continuous eval. object
            sc2 = fmeasure(ev);
        case '2afc'
            sc2 = Calc2AFC(u, get_y(d1));
        case 'crate'
            ev = binEval(d1, R2); % - Continuous eval. object
            sc2 = crate(ev);
        otherwise
            error('Loss function not implemented');
    end
        
	fprintf(1, '\n\tTest 2 for dist has %s score %.4f\n', '2afc', sc2);

    score = mean([sc1 sc2]);

end