function d = binarise(d, i)

%BINARISE - returns a binarised dataset: the ith class is positive, all others negative
%
% AUTHOR:	M.F. Valstar
% CREATED:	0404.2007
%
%IN:  d: data
%     i: value range to declare positive (e.g. [1 2])
%OUT: d: binarised data

% CHANGELOG
%-----------
% 16032012 ===>     added support for binarizing continuous targets, now a range for the
%                   positive class is supported
% 16032012 ===>     added support for classes derived from data (otherwise the returned
%                   class will always be @data)

% for backward compatibility extend scalar to range
if size(i,2) < 2
    i(2) = i(1);
end

y = -ones(size(d.Y));
y(d.Y >= i(1) & d.Y <= i(2)) = 1;
% instead of creating a new data object, set_y of previous object
d = set_y(d, y);