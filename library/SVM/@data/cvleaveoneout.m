function cv = cvleaveoneout(d)

%CVLEAVEONEOUT - returns the leave-one-out cross validation sets for data
%
% AUTHOR:   M.F. Valstar
% CREATED:  1911.2007
%           Imperial College London
%
%IN:  d: seqdata
%OUT: cv: struct with .train and .test inclusion indices of sequences

n = nrSamples(d);
A = 1:n;
for i = 1:n
        cv(i).test = i;
        cv(i).train = setdiff(A, i);
end
