function d = labelSubset(d, l)

%LABELSUBSET - returns a subset of data with multiple labels, for instance
%           a subset of regression targets. Not to be confused with mcSubset or 
%           classSubset, which have different functions
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 0204.2008
%
%IN:  d: data
%     l: indices of labels to retain
%OUT: d: data

d = data(d.X, d.Y(:,l));
