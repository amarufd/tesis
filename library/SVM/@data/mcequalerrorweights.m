function w = mcequalerrorweights(d, n, mctype);

%MCEQUALERRORWEIGHTS - returns a set of weights such that sum(wp) == sum(wn), spider data version
%
% AUTHOR:   M.F. Valstar
% CREATED:  25012007
%
%IN:  d: spider data object
%     n: nr of classes
%     mctype: 1vs1 or 1vsall
%OUT: w: weights, array of, [instances x class_combinations] per label w e <0,1>

if strcmp(s.type, '1vs1')
	s = mcSvm(n, '1vs1');
	N = nrClassifiers(s);
	for 