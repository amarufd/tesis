function y = classSubset(x, c)

%CLASSSUBSET - returns the subset of data that contains class c
%
% AUTHOR:	M.F. Valstar
% CREATED:	1904.2007
%
%IN:  x: original data object
%     c: class to get data for
%OUT: y: new data set

i = find(x.Y == c);
y = data(x.X(i,:), x.Y(i,:));