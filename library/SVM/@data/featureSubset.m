function d = featureSubset(d, f)

% -- Return subset of features f
%IN:  d: data
%     f: index of features to keep in returned data
%OUT: d: data with subset of features

d = data(d.X(:,f), d.Y);