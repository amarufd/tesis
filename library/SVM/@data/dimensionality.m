function n = dimensionality(d)

%DIMENSIONALITY -- returns dimensionality of d

n = size(d.X, 2);