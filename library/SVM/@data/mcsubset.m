function y = mcsubset(x, I)

%MCSUBSET - returns the subset of data that constitutes a mc-data subset
%
% AUTHOR:	M.F. Valstar
% CREATED:	0902.2007
%
%IN:  x: data [spider data object]
%     I: the combination of classes to return the subset for
%OUT: y: subset of data, with original labels

if length(I)~=2
	error('illegal class combination: mcsubset returns a subset of exactly two classes.');
end

I = sort(I);	% - Make sure the classes are ordered low-high

ind = find(get_y(x)==I(1)|get_y(x)==I(2));
y = subset(x, ind);