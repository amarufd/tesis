function of = gentleBoostOrder(d)

vectorised = 1; % - Use vectorised gboost
w = equalerrorweights(d);
of = gentleBoostOrder(d.X, d.Y, w, vectorised);