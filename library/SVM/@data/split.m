function D = split(d, n)

%SPLIT - splits data in n (roughly) equal sets, making sure all classes are equally distributed
%
% AUTHOR:	M.F. Valstar
% CREATED:	1907.2007
%
%IN:  d: data obj.
%     n: nr of sets to divide in
%OUT: D: an array of n non-overlappint data objects whose union is d
%
%See also: CVNFOLDS

cv = cvNfolds(d, n);
for i = 1:length(cv)
	I = cv(i).test;
	D(i) = data(d.X(I,:), d.Y(I));
end