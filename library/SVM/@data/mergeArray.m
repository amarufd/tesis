function d = mergeArray(D, I)

% -- Merge data from array D per the row-wise components of I

% CHANGELOG
%-----------
% 27072007 ===> Updated merging: Now checks for duplicate data

for i = 1:size(I,1)
	i1 = I(i,1);
    i2 = I(i,2);
	d(i) = merge(D(i1), D(i2));
end