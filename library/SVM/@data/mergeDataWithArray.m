function Z = mergeDataWithArray(Z, x)

%MERGE - merges data x to each element of an array of data Z
%
% AUTHOR:	M.F. Valstar
% CREATED:	1907.2007
%
%IN:  Z: array of data
%     x: data to add to each element of Z
%OUT: Z: updated array of data

% CHANGELOG
%-----------
% 27072007 ===> Updated merging: Now checks for duplicate data

if length(Z) < 2 
	error('Z must be an array of data');
end

for i = 1:length(Z)
	Z(i) = merge(Z(i), x);
end