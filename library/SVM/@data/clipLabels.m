function d = cliplabels(d, r)

%CLIPLABELS - clips the values of the labels of d to the values r = [r_min r_max]
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 0105.2008
%
%IN:  d: data
%     r: range of data 
%OUT: d: clipped data

if r(2) < r(1)
        error('Range should be specified [min max]');
end

d.Y(d.Y<r(1)) = r(1);
d.Y(d.Y>r(2)) = r(2);
