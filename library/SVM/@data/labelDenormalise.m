function d = labelDenormalise(d, T, S)

%LABELDENORMALISE - maps the labels of a normalised range back to the original space
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 0804.2008
%
%IN:  d: data
%     T: translation used to go to normalised range (returned by labelNormalise)
%     S: scale used to go to normalised range (returned by labelNormalise)
%OUT: d: denormalised data


d.Y = (d.Y-T)./S;
