function y = noise(d, n)

%NOISE - Adds (or subtracts) random white noise. Later maybe more types of noise to be impl.
%
% AUTHOR:	M.F. Valstar
% CREATED:	2803.2007
%
%IN:  d: spider data
%     n: noise level
%OUT: y: data with added noise

x = get_x(d);
% - Create random values
r = rand(size(x)) - ones(size(x))/2;
% - Multiply random values by the mean of data (normalisation) and by the noise level
N = r*n;
% - Noisify the data
y = data(x+N, get_y(d));