function fig = show(d, I, fig)


if nargin < 2
    I = 1:2;
end

fig = figure;
hold on;
I_p = find(d.Y(:,1) == 1);
I_n = find(d.Y(:,1) == -1);
scatter(d.X(I_p,I(1)), d.X(I_p, I(2)), 'rs');
scatter(d.X(I_n,I(1)),d.X(I_n,I(2)), 'b');
hold off
