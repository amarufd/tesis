function s = mc2oao(s)

%MC2OAO - filters mc-label coding to aco-coding
%
%          version for data

s.Y = mc2oao(s.Y);
