function [y s a] = gentleBoost(x, w)

%GENTLEBOOST - gentleboost version for spider data
%
% AUTHOR:	M.F. Valstar
% CREATED:	2003.2007
%
%IN:  x: input data
%     w: sample weights (optional, default is equalerrorweights)
%OUT: y: subset of data with selected features only
%     s: the selected features (uniquely nonsorted)
%     a: GentleBoost classifier
X = get_x(x);
Y = get_y(x);

[nrs nrf] = size(x);

if nargin < 2
	w = equalerrorweights(x);
end

[s a] = gentleBoost_2(X, Y, w);
s = uniqueNonSort(s);
y = subset(x, 1:nrSamples(x), s);