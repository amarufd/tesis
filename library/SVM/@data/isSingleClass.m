function b = isSingleClass(d, a)

%ISSINGLECLASS - returns whether a data object has only a single
%               class (which makes it rather unsuitable for 
%               training classifiers)
%
%IN:  d: data
%     a: optionally, the label dimension to use (default is the first)

if nargin < 2
    d = data(d.X, d.Y(:,1));
else
    d = data(d.X, d.Y(:,a));
end

y = d.Y;
b =  ~any(y ~= y(1));
