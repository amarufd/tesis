function D = arrayChange(D, i, d)

% Replace the index i of a data array D with d

D(i) = d;