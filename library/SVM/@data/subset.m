function d = subset(d, Iind, If)

%SUBS - returns the subset of a function
%
% AUTHOR:	M.F. Valstar
% CREATED:	25012007
%
%IN:  d: data object 1
%     Iind: subset indices of instances
%     If: subset indices of features [OPT] DEFAULT = all
%OUT: d: subset of data

% CHANGELOG
%-----------
% 27022007 ===> Removed get_x/get_y calls to speed things up 

if nargin < 3
	If = 1:size(d.X, 2);
end

%  d = data(get_x(d, Iind, If), get_y(d, Iind));
if isempty(d.Y)
    d = data(d.X(Iind, If));
else
    d = data(d.X(Iind, If), d.Y(Iind,:));
end