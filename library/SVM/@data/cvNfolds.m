function sets = cvNfolds(d, n)

%CVNFOLDS -- builds from a data object random train/test sets. 
%
% AUTHOR:	M.F. Valstar
% CREATED:	28062007
%
%Each train set consists of 1 subject of the index and all other subjects for the testset
%IN:  d: spider data object
%	  n: number of folds in test set
%OUT: sets: struct containing testsessions and trainsessions

% CHANGELOG
%-----------
% 24072007 ===> MICHEL: Added copy of output, to be analogous with the 
%               seqdata version
% 22042008 ===> MICHEL: Added regression datat type setting r

% -- Initialise random seed
%rand('state',sum(100*clock));
% 24072012 ===> MICHEL: removed the odd class balancing in cv-Nfolds. That
% really should be done outside of this function.


    R = randperm(nrSamples(d));
    s = floor(nrSamples(d)/n);
    cnt = 0;
    for i = 1:n-1
        sets(i).test = sort(R(cnt+1:cnt+s));
        sets(i).train = setdiff(1:nrSamples(d), sets(i).test);
        cnt = cnt+s;
    end
    sets(n).test = sort(R(cnt+1:end));
    sets(n).train = setdiff(1:nrSamples(d), sets(n).test);
end

% else
% 
%     % -- First divide all classes...
%     C = splitClasses(d);
%     for i = 1:length(C)
%         if length(C{i}) < n
%             error('Not possible to make cv fold, as there are not enough examples of at least one class');
%         end
%     end
%     % -- Then split each randomly in n sets...
%     S = permuteClasses(C,n);
% 
%     % -- Now concatenate the data...
%     sets = catClasses(S, 1:nrSamples(d));
% end
% 
% 
% function C = splitClasses(d)
% % C contains the indices of the classes
% % classes are indexed not by classname but by classnr... i.e., there could be missing classes
% D = unique(d.Y);
% n = length(D);
% for i = 1:n
% 	I = find(d.Y == D(i));
% 	C{i} = I;
% end
% 
% function S = permuteClasses(C, n)
% for c = 1:length(C)
% 	I = randperm(length(C{c}));	% -- Randomly permute the data index of instances belonging to class c
% 	it = length(I)/n;
% 	for f = 1:n
% 		if f < n   
% 			S{c,f} = C{c}(I(round((f-1)*it)+1:round(f*it)));
% 		else
% 			S{c,f} = C{c}(I(round((f-1)*it)+1:length(I)));
% 		end
% 	end
% end
% 
% function sets = catClasses(S, A)
% % -- Concatenate data over all classes
% % A is all instances
% [m n] = size(S);
% for f = 1:n
% 	sets(f).test = [];
% 	sets(f).train = [];
% 	for c = 1:m
% 		sets(f).test = union(sets(f).test, S{c,f});
% 	end
% 	sets(f).train = setdiff(A, sets(f).test);
% end