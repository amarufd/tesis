function d1 = cat(d1, d2)

%CAT - concatenates two spider data objects
%
% AUTHOR:	M.F. Valstar
% CREATED:	25012007
%
%IN:  d1: data object 1
%     d2: data object 2
%OUT: d3: concatenated data object

% CHANGELOG
%-----------
% 23022007 ===> Removed get_x function call, instead addressing data directly. Speeds
%				up computations considerably
% 18112007 ===> Made it filtering function, so output is d1, not d3 (memory
%               reasons)
d1 = data(vertcat(d1.X, d2.X), vertcat(d1.Y, d2.Y));