function [y s a] = gentleBoost_cv(d)

%GENTLEBOOST_CV - gentleboost using separate train/test data to learn classifier
%
% AUTHOR:	M.F. Valstar
% CREATED:	0308.2007
%
%IN:  d: data
%OUT: y: subset of data with selected features only
%     s: the selected features (uniquely nonsorted)
%     a: GentleBoost classifier

cv = cvNfolds(d, 2);

x1 = subset(d, cv(1).train);
x2 = subset(d, cv(2).train);

w1 = equalerrorweights(x1);
w2 = equalerrorweights(x2);

[s1 a] = gentleBoost_cv(get_x(x1), get_y(x1), w1, get_x(x2), get_y(x2), w2);
[s2 a] = gentleBoost_cv(get_x(x2), get_y(x2), w2, get_x(x1), get_y(x1), w1);
s = uniqueNonSort(horzcat(s1, s2));
y = subset(d, 1:nrSamples(d), s);