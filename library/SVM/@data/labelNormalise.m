function [d, t, s] = labelNormalise(d, r, T, S)

%LABELNORMALISE - maps the labels of d to lie within range r
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 0804.2008
%
%IN:  d: data
%     r: optional: range. Default is [0,1]
%     T: translation specified
%     S: scale specified
%OUT: d: normalised data
%     t: applied translation (so the shift from the original min to min(r)
%     s: applied scale transformation (so if the data was in [0 0.5] then s = 2)

if nargin < 2
        r = [0 1];
end

if nargin < 3
        R = r(2)-r(1);
        % -- First scale
        s = ((max(d.Y) - min(d.Y))/R).^-1;
        d.Y = d.Y*diag(s);

        % -- Then translate what's left
        t = r(1)-(min(d.Y));
        d.Y = d.Y+repmat(t, size(d.Y,1), 1);
else
        % -- Do inverse of normalisation above
        d.Y = d.Y-repmat(T, size(d.Y,1), 1);
        d.Y = d.Y/diag(S);
        %d.Y = d.Y.*S+T;
end
