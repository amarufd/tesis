function [y s a] = gentleBoost(x, w, maxN)

%GENTLEBOOST - gentleboost version for spider data
%
% AUTHOR:	M.F. Valstar
% CREATED:	2003.2007
%
%IN:  x: input data
%     w: sample weights (optional, default is equalerrorweights)
%     maxN: maximum nr of boost rounds
%OUT: y: subset of data with selected features only
%     s: the selected features (uniquely nonsorted)
%     a: GentleBoost classifier

% CHANGELOG
%-----------
% 05112007 ===> Added the maxN max boost rnds par

X = get_x(x);
Y = get_y(x);

[nrs nrf] = size(x);

if nargin < 2
	w = equalerrorweights(x);
end
if nargin < 3
    maxN = 0;
end

vectorised = 1; % - Use vectorised gboost

[s a] = gentleBoost(X, Y, w, vectorised, maxN);
s = uniqueNonSort(s);
y = subset(x, 1:nrSamples(x), s);
