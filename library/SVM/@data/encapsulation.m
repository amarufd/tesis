function [D Irem Idrops P] = encapsulation(d, p, m, plotornot)

%ENCAPSULATION - reduces the size of the input data by means of condensation
%
% AUTHOR:	M.F. Valstar
% CREATED:	18012007
%
%IN:  d: data
%     p: stop criterium: percentage to condense to 0<p<1
%     m: stop criterium: absolute max. nr. of rounds to attempt
%     condensation. Default is 10% of nr instances of d
%     plotornot
%OUT: D: condensed (reduced) data
%     Irem: indices of surviving instances
%     Idrops = cell array of hyperdrop instance indices 
%     P: final condensation level

% CHANGELOG
%-----------
% 26012007 ===> Changed input from x,y to d : spider data object
% 26012007 ===>	Condensation now removes the entire encapsulated hyperdrop, no mean replacement
% 31012007 ===> Removed condense subfunction from this file, is now a spider data method, operating
%				on spider data and a CELL array of (possible multiple) hyperdrop indices
% 31012007 ===>	Changest sub function 'closest' to output CELL arrays for compliance with condense
% 31012007 ===>	Changest sub function 'closest' to output CELL arrays for compliance with condense
% 11052007 ===> Added default value of maxIt m

if nargin < 3
    m = round(0.1*(nrSamples(d)));
end
if nargin < 4
	plotornot = 0;
end

[n D] = size(get_x(d));

M = 0; % Current round nr...
N = n; % Current sample size
Irem = 1:N;	% Original indices
D = d; % current data set
Dhd = data; % Empty hyperdrop-data init
Idrops = [];
P = 1; % Current condensation level

if p == 1
	return;
end


rand('state',sum(100*clock));
if plotornot
	fig = figure;
	fig2 = figure;
end

while P>p && M < m
	% -- Select a random sample xs
	is = Irem(ceil(length(Irem)*rand));
	% -- Compute the distance of all elements in Drem (the remainder of ORIGINAL data) to xs
	I = closest(subset(d, Irem), subset(d,is));
	% -- Redo indexing: these are the indices of the candidate hyperdrop in ORIGNAL data set
	Ih = Irem(I{1});

	if plotornot
		hold on;
		x = get_x(d, Ih, 1:2);
		scatter(x(:,1), x(:,2), 'gs');
	end
	
	if ismonolabel(subset(d, Ih))
		fprintf(1, '\tHyperdrop found... checking encapsulation...\n');
		% Dc is candidate condensed data. Drem is the original data part and dh the hyperdrop centre
		% -- As we are condensing on a subset of d, make sure we use an applicable index 
		[Dc dh] = condense(subset(d, Irem), I);
		Ie = closest(Dc, dh);
		if plotornot
			hold on;
			x = get_x(Dc, Ie{1}, 1:2);
			scatter(x(:,1), x(:,2), 'ms');
		end
		if ismonolabel(subset(Dc,Ie{1})) && get_y(subset(d, is)) == get_y(subset(Dc, Ie{1}(1)))
			% -- We have a valid hyperdrop! Save it, and adjust Irem
			fprintf(1, '\t\tHyperdrop is encapsulated, condensing molecules\n');
			% -- Save the Idrop instance indices. Be carefull to take this from the original instance indices
			Idrops{end+1} = Ih;
			Dhd = cat(Dhd, dh);
			% -- Replace the instances constituting the encapsulated hyperdrop with their mean
			Irem = setdiff(Irem, Ih);
			if plotornot
				plot2class(subset(d, Irem), [1:2], fig);
			end
		else
			fprintf(1, '\t\tHYPERDROP NOT ENCAPSULED! Hyperdrop not condensed\n');
		end
	end
	N = length(Irem);
	M = M + 1;
	P = (N/n);
	fprintf(1, 'Round %u: Condensation level is %f\n', M, P);
end

% -- Return the remainder of the original data plus the means of the hyperdrops...
D = cat(subset(d, Irem), Dhd);

function I = closest(d, dt)
% -- Returns the indices of the nearest neighbours of d to dt
D = size(get_x(d), 2);
y = distance(d, dt);
[v Id] = sort(y);
I{1} = Id(1:D+1);

function d = distance(D, dt)
% -- Euclidian distance of all elements in set X to point x
X = get_x(D); x = get_x(dt);
Xd = (X - repmat(x, size(X,1), 1)).^2;
d = sum(Xd, 2);


function a = ismonolabel(b)
% -- Checks if a set of labels b all have the same class
B = get_y(b);
if length(unique(B)) == 1
	a = 1;
else
	a = 0;
end	