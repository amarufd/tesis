function s = std(d)

%STD - returns standard deviation of data
%
% AUTHOR:	M.F. Valstar
% CREATED:	1005.2007
%
%IN:  d: spider data object
%OUT: s: std [1 x dim]

s = std(d.X);