function [D, t, s] = normalMinMax(d,t,s)

% Normalizes the feature matrix of a data object, all columns to lie
% in the range of [-1, 1]
%IN:  d: data object
%     t: translation to apply [OPT]
%     s: scaling to apply [OPT]
%OUT: y: normalized vector/matrix
%     t: computed translation
%     s: computed scaling factor

if nargin < 3
	mn = min(d.X);
	mx = max(d.X);
	df = mx-mn;
	df(df==0) = 1;
	s = 2./df;
end

% -- First apply scaling
x = d.X.*repmat(s, size(d.X, 1), 1);
% -- Now apply translation
if  nargin < 2
    t = min(x)+1;
end
x = x - repmat(t, size(x,1), 1);

D = data(x,d.Y);