function b = isMonoClass(d, N)

%ISMONOCLASS - returns whether a collection of points is of the same class
%
% AUTHOR:	M.F. Valstar
% CREATED:	0507.2007
%
%IN:  d: spider data object
%     N: cell array of length n collections of points
%OUT: b: boolean vector of length n, 1 if all points were of the same class, 0 if not

n = length(N);
b = zeros(1, n);
for i = 1:n
	y = unique(d.Y(N{i}));
	if length(y) == 1
		b(i) = 1;
	else
		b(i) = 0;
	end
end