function d = combine(d, d1)

%COMBINE - combines the data from two sets, coming from the same distribution but
%           having different features (so, each element is from one entity, but
%           is described differently
%
% AUTHOR:   M.F. Valstar
% CREATED:  1411.2007
%
%IN:  d: first set of data
%     d1: second set of data
%OUT: d: data with both features from d and d1 (grouped in that order)

d.X = horzcat(d.X, d1.X);
