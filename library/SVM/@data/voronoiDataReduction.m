function [d Irem Idrops c] = encapsulation(d, p, m, s)

%ENCAPSULATION - reduces the size of the input data by means of encapsulation
%
% AUTHOR:	M.F. Valstar
% CREATED:	18012007
%
%IN:  d: data
%     p: stop criterium: percentage to condense to 0<p<1
%     m: stop criterium: absolute max. nr. of rounds to attempt
%     condensation. Default is 10% of nr instances of d
%     s: chunk size: size of data to perform condensation on at one time
%OUT: d: condensed (reduced) data
%     Irem: indices of surviving instances
%     Idrops = cell array of hyperdrop instance indices 
%     c: final condensation level

% CHANGELOG
%-----------
% 05072007 ===>	This is version 2 of encapsulation. Check v1 for the old version
% 				This version uses voronoi diagrams to find encapsulated cells
% 09072007 ===>	This is now a wrapper function for condense.m The idea is to 
%				Feed condense chunks of distance-sorted data, to restrain the 
%				maximum size. Added parameters that control chunk-size

[n D] = size(d.X);
c = 1;	% -- Current condensation level
Irem = 1:n; Idrops = [];
N = 100;	% -- The number of points to check for encapsulation per iteration

if nargin < 2
	p = 0.001;
end
if nargin < 3
	m = round(0.1*n);
end
if nargin < 4
	s = round(0.01 * n);
end

fprintf(1, 'Starting encapsulation on %u-dimensional data with %u instances', D, n);

%  % -- Remove duplicates
%  fprintf(1, '\n\tRemoving duplicates...');
%  [d Irem Idrops] = removeDuplicates(d, Irem, Idrops);
%  nn = size(d.X, 1);
%  fprintf(1, '\t%u instances removed\n', n-nn);

cnt = 1; done = 0; c_old = c;
while ~done
%  	[d Irem Idrops] = condenseChunk(d, s);
	[d Irem Idrops] = condense(data(d.X(:,1:6), d.Y(:)), s);
	c = size(d.X, 1)/n;
	fprintf('\n\nEncapsulation condensation level is %f\n', c);
	[done, c_old, cnt] = stopcriterium(c, c_old, p, cnt);
end

function [done, c_old, cnt] = stopcriterium(c, c_old, p, cnt)
if c < c_old	% -- We have improvement
	cnt = 1;	% -- So reset counter
else
	cnt = cnt + 1;
end
% -- Stop criterium
if c < p || cnt > 3
	done = 1
end

	
	
function [d Irem Idrops] = condenseChunk(d, s)
% -- This function randomly picks an instance, finds s nearest neighbours, and
% performs condensation on that chunk

% !!! Something wrong with the data returned !!!
[n D] = size(d.X);
% -- Select a random sample xs
is = ceil(n*rand);
% -- Compute the distance of all elements in Drem (the remainder of ORIGINAL data) to xs
I = closest(d, data(d.X(is,:), d.Y(is)));
[d_c Irem Idrops c] = condense(data(d.X(I(1:s),:), d.Y(I(1:s))));
%  d = data(d.X(Irem



function I = closest(d, dt)
% -- Returns the indices of the nearest neighbours of d to dt
D = size(get_x(d), 2);
y = distance(d, dt);
[v I] = sort(y);



function d = distance(D, dt)
% -- Euclidian distance of all elements in set X to point x
X = get_x(D); x = get_x(dt);
Xd = (X - repmat(x, size(X,1), 1)).^2;
d = sum(Xd, 2);