function d = array(D, I)

% build an array of length(I) elements from data D

for i = 1:length(I)
	d(i) = data(D.X(I{i},:), D.Y(I{i}));
end