function d = downsample(d, f)
% - Down-sample d by a factor f in a structural manner

    n_ori = size(d.X, 1);
    I = round(1:f:n_ori);

    d.X = d.X(I,:);
    d.Y = d.Y(I,:);
end