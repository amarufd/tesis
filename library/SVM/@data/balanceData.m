function d = balanceData(d)
% -- Balance data 50/50
    
    np = sum(d.Y==1);
    nn = sum(d.Y==-1);
    
    if np < nn
       Ip = find(d.Y==1);
       In = find(d.Y==-1);
       r = randperm(nn);
       In = In(r(1:np));
    else
        Ip = find(d.Y==1);
        r = randperm(np);
        Ip = Ip(r(1:nn));
        In = find(d.Y==-1);
    end
    I = union(Ip, In);
    d = data(d.X(I,:), d.Y(I));
end