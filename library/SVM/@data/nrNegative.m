function [n I] = nrNegative(d)

%NRNEGATIVE - returns the nr of negative elements of d and their indices
%
% AUTHOR:   M.F. Valstar
% CREATED:  1511.2007
%
%IN:  d: data
%OUT: n: nr of negative elements in d
%     I: the indices of negative elements in d

I = find(d.Y == -1|d.Y == 0);
n = length(I);
