function [y b] = positiveClasses(d)

%POSITIVECLASSES - returns for a sequence which classes are positive
%               if s is an array, it returns a cell and a binary matrix
% AUTHOR:   M.F. Valstar
% CREATED:  2211.2007
%
%IN:  s: data obj
%OUT: y: (cell) of active classes
%     b: binary representation of y

if length(d) == 1
    [y b] = positives(d.Y);
else
    for i = 1:length(d)
        [Y, B] = positiveClasses(d{i});
        y{i} = Y;
        b(i,:) = B;
    end
end

function [y b] = positives(d)

d(find(d<0)) == 0;
b = sum(d)>0;
y = find(b == 1);
