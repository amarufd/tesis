function n = nrParameters(r)

%NRPARAMETERS - returns how many parameters the regressor has that can be optimised
%
%IN:  r: regressor object
%OUT: n: nr of parameters

if isa(r.regr, 'mSvr')
    k = getKernel(r.regr);
    switch k
        case 'rbf'
            n = 3;
        case 'linear'
            n = 2;
        case 'intersection'
            n = 2;
    end
end
