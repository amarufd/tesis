function [r p e V] = optimise(r, d1, d2, m)

% switchboard for different optimisers. Depends on the nr of parameters to optimise and
% whether the parameters are indep/dep

n = nrParameters(r);
D = dependentParameters(r);

if D 
        switch(n)
        case 1 
                [r p e V] = optimise1dep(r, d1, d2, m);
        case 2
                [r p e V] = optimise2dep(r, d1, d2, m);
        case 3
                [r p e] = optimise3dep(r, d1, d2, m);
                V = [];
        otherwise 
                error('No more than 3 dependent parameters supported');
        end
else
        [r p e] = optimiseNIndep(r, d1, d2, n, m);
        V = [];
end

