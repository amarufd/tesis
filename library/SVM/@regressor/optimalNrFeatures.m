function bestNrF = optimalNrFeatures(s, d, etype, f)
% Interface function for different regressors
  %IN:  s: regressor
   %     d is seq. data object
   %     etype is the error type, e.g. 'square'
   %     f is the set of ordered features
   %OUT: optimal nr of features
   
   bestNrF = optimalNrFeatures(s.regr, d, etype, f);


end