function [r p e V] = parparameteroptimisation(r, d, n, m, f, D)
%PARAMETEROPTIMISATION - optimises the parameters for a regressor object
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2104.2008
%
%IN:  r: regressor object
%     d: data/seqdata
%     n: n-folds to do parameter optimisation over
%     m: loss function (e.g. 'linear', 'square', 'radial', or 'exp')
%     f: data reduction factor: default is 1 (no reduction)
%     D: optionally, force the number of parameters to optimise. For SVR
%     the order is [sigma, C, epsilon]
%OUT: r: regressor object with optimal parameters
%     p: median of optimal parameters per fold
%     e: optimal error vector (length n, so per fold)

% -- Default values
if nargin < 3
	n = 3;
end
if nargin < 5
    f = 1;
end

%CV = cvNfolds(d,n, 1);  % - Make sure to set that we use regression data (the 1)

% -- Trouble-shoot single-class data. This should never happen in reality
if isSingleClass(d)
        warning('Data is all of one class. Creating fake data for other class.');
        d = createFakeData(d);
end

% -- Allocate memory for results
%h = waitbar(0, 'Parameter optimsation');
%for i = 1:n
%    waitbar(i/n, h);
%    fprintf(1, '\nOptimizing parameters for SVM: fold %u of %u\n\n',i,n);
%    i1 = CV(i).train; i2 = CV(i).test;
%    if isa(d, 'seqdata')
%        dd1 = data(subset(d,i1));
        % -- Reduce data. Do that here to not subsample entire
        % sequences
%        dd1 = subset(dd1, 1:f:nrSamples(dd1));
%        dd2 = data(subset(d,i2));
if f > 1
	d = reduceData(d, f);	% - N.B. Set regression to true
end
N = nrParameters(r);
if nargin < 6
	D = dependentParameters(r);
end

if D 
        switch(N)
        case 2
                [r p e V] = paroptimise2dep(r, d, n, m);
        case 3
                [r p e] = paroptimise3dep(r, d, n, m);
                V = [];
        otherwise 
                error('No more than 3 dependent parameters supported');
        end
else
        [r p e] = paroptimiseNIndep(r, d, n, N, m);
        V = [];
end


%end
%close(h);
drawnow;

r = setParameter(r, 1:nrParameters(r), p);
