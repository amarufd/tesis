function [r p e V] = parameteroptimisation(r, d, n, m, f, D)
%PARAMETEROPTIMISATION - optimises the parameters for a regressor object
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2104.2008
%
%IN:  r: regressor object
%     d: data/seqdata
%     n: n-folds to do parameter optimisation over
%     m: loss function (e.g. 'linear', 'square', 'radial', or 'exp')
%     f: data reduction factor: default is 1 (no reduction)
%     D: optionally, force the number of parameters to optimise. For SVR
%     the order is [sigma, C, epsilon]
%OUT: r: regressor object with optimal parameters
%     p: median of optimal parameters per fold
%     e: optimal error vector (length n, so per fold)



% -- Default values
if nargin < 3
	n = 3;
end
if nargin < 5
    f = 1;
end
if f > 1
	d = reduceData(d, f);	% - N.B. Set regression to true
end
N = nrParameters(r);
if nargin < 6
	D = dependentParameters(r);
end

if D 
        switch(N)
        case 1 
                [r p e V] = optimise1dep(r, d1, d2, m);
        case 2
                [r p e V] = optimise2dep(r, d, n, m);
        case 3
                [r p e] = optimise3dep(r, d, n, m);
                V = [];
        otherwise 
                error('No more than 3 dependent parameters supported');
        end
else
        [r p e] = optimiseNIndep(r, d, n, N, m);
        V = [];
end