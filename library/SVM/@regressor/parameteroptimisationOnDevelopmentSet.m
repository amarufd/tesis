function [r p e V] = parameteroptimisationOnDevelopmentSet(r, d1, d2, m)
%PARAMETEROPTIMISATION - optimises the parameters for a regressor object
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2104.2008
%
%IN:  r: regressor object
%     d1: training data/seqdata
%     d2: development data/seqdata
%     m: loss function (e.g. 'linear', 'square', 'radial', or 'exp')
%OUT: r: regressor object with optimal parameters
%     p: median of optimal parameters per fold
%     e: optimal error vector (length n, so per fold)


N = nrParameters(r);
D = dependentParameters(r);

if D 
        switch(N)
        case 1 
                [r p e V] = optimise1depOnDevelopmentSet(r, d1, d2, m);
        case 2
                [r p e V] = optimise2depOnDevelopmentSet(r, d1, d2, m);
        case 3
                [r p e] =   optimise3depOnDevelopmentSet(r, d1, d2, m);
                V = [];
        otherwise 
                error('No more than 3 dependent parameters supported');
        end
else
        [r p e] = optimiseNIndepOnDevelopmentSet(r, d1, d2, N, m);
        V = [];
end
