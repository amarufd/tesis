function [r v e] = optimiseIndep(r, d1, d2, n, m)

%OPTIMISEINDEP - parameter optimisation for n mutually independent variables
%Optimises the C and the karg parameter. Can be modified for other params
%
% Based on optimise for mSvm
%
%IN:  r: mSvr object
%     d1: train data
%     d2: test data
%     n: nr of parameters
%     m: loss function to use
%OUT: r: mSvr with optimal parameters
%     v: values of optimal parameters [karg, C]
%     e: error with best parameters

% TODO
%------
% 22042008 ===> Make this algorithm parameter work for n parameters

error('Not finished implementing this yet');
plotornot = 1;
if plotornot
    fig = figure;
end
optscore = 1;   % - Working with error now

% -- We'll assume k and C are independent and start with optimising for k This is wrong!
C = inf;    % - Default value for C


kdone = 0;
cnt = 0;
x_log = [0:4]; x = []; f = []; y = [];
resolution = 1;
while ~kdone 
        cnt = cnt+1;
        for i = 1:length(x_log)
                x(end+1) = exp(x_log(i));
                y(end+1) = evaluation(r, x(end), C, d1, d2, m);
                % -- Find best performance
                if y(end) < optscore
                        fprintf(1, '\tNew best performance. Was %1.3f, now is %1.3f (%1.3f difference)\n', optscore, y(end), y(end)-optscore);
                        optscore = y(end);
                        optkarg = x(end);
                end
        end
        % -- Change resolution and scope
        resolution = resolution/5;
        x_log = newscope(resolution, log(optkarg), 2);
        % -- Stop condition 
        if cnt > 3 || optscore < 0.0000001
                kdone = 1;
        end
end
fprintf(1, 'Best k-argument is %2.3f with performance value %1.4f\n\n', optkarg, optscore);
v(1) = optkarg;
f_k = optscore;

% -- Now for C
if plotornot
    fig2 = figure;
end
f = [];
k = v(1);

% -- We already have values for inf.
fprintf(1, '\n\n');
x_log = [0:6]; x = []; f = []; y = [];
cnt = 0;
Cdone = 0;
resolution = 1;
optCarg = inf;
% -- To overcome frequent occurrence where C = 1000 performs equally as C = inf, increase perf of C = inf marginally
optscore = optscore + 0.00001;
while ~Cdone
        cnt = cnt+1;
        for i = 1:length(x_log)
                x(end+1) = exp(x_log(i));
                y(end+1) = evaluation(r, optkarg, x(end), d1, d2, m);
                % -- Find best performance
                if y(end) < optscore
                        fprintf(1, '\tNew best performance. Was %1.3f, now is %1.3f (%1.3f difference)\n', optscore, y(end), y(end)-optscore);
                        optCarg = x(end);
                end
        end
        % -- Change resolution and scope
        resolution = resolution/5;
        x_log = newscope(resolution, log(optkarg), 2);
        % -- Stop condition
        if cnt > 3 || optscore < 0.00001 || optCarg == inf
                Cdone = 1;
        end
end

fprintf(1, 'Best C-argument is %2.3f with performance value %1.4f\n\n', optCarg, optscore);
v(2) = optCarg;

% -- The first column of optp contains the unsigned prediction
r = setParameter(r, 1:2, v);
e = optscore;


function f = evaluation(s, k, C, d1, d2, m)
	s = setParameter(s, 1:2, [k C]);
	s = train(s, d1);
	
% -- So, how good is this prediction?
	R2 = test(s,d2); 
        ev = cEval(d2, R2); % - Continuous eval. object
% -- Optimum is highest possible F-measure on test data
        switch m    % - Which loss function to use
        case 'linear'
                f = linearLoss(ev);
        case 'square'
                f = squareLoss(ev);
        case 'exp'
                f = expLoss(ev);
        end
	fprintf(1,'k = %3.2f\tC = %u\tAverage %s loss on test data data: %f\n', k, C, m, f);

function x = newscope(res, x_0, n)
% -- returns n new values left and right of x_0, in log, with a spread of res between each value
x = (x_0-n*res):res:(x_0+n*res);
x = setdiff(x, x_0);    % - Remove self


