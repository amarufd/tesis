function d = dependentParameters(r)

%DEPENDENTPARAMETERS - returns whether the parameters of a regressor are dependent
%
%IN:  r: regressor object
%OUT: d: 1 for dependent, 0 for independent parameters

if isa(r.regr, 'mSvr')
        d = 1;
end
