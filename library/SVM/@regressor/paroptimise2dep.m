function [r v e V] = paroptimise2dep(r, d, n, m, difmin)

%OPTIMISE2DEP - parameter optimisation for 2 mutually dependent variables
%Optimises the C and the karg parameter. Can be modified for other params
%
% Based on optimise for mSvm
%
%IN:  r: mSvr object
%     d: data
%     n: nr folds
%     m: loss function to use
%     difmin: minimum increase between rounds
%OUT: r: mSvr with optimal parameters
%     v: values of optimal parameters 
%     e: error with best parameters

	if nargin < 5
		difmin = 0.001;
	end


	plotornot = 0;
	if plotornot
		fig = figure;
	end

	e_min= 1;   % - Working with error now
    n_d = nrSamples(d);
	% -- Assume that we have two parameters. Don't know what they mean, necessarily
	l1 = 1;
	r1 = 100;
	n_steps = 5;
	l2 = l1; r2 = 100;
	done = 0;   % - We've just started!

	V = []; E_scale = [];
	
	CV = cvNfolds(d,n, 1);  % - Make sure to set that we use regression data (the 1)

	while ~done
			% -- Get parameter search space (exponential search space, btw)
			[S1 S2] = searchSpace(l1, r1, l2, r2, n_steps);
            
            % -- Prepare data structures for parallel processing
            n_1 = size(S1,1);
            n_2 = size(S1,2);
            n_t = n_1*n_2;
            cnt = 0;
            P = zeros(n_t,2);
            e = ones(n_t, 1);
            for i = 1:n_1
				for j = 1:n_2
                    cnt = cnt + 1;
                    P(cnt,:) = [S1(i,j), S2(i,j)];
                end
            end
            
			for i = 1:n_t
                p1 = P(i,1);
                p2 = P(i,2);
                e_par = zeros(n_d, 1);
                res = cell(n,1);
                ind = cell(n,1);
                parfor fold = 1:n
                    I1 = CV(fold).train; ind{i} = CV(fold).test;
                    if isa(d, 'seqdata') 
                        d1 = data(sequenceSubset(d, I1));
                        d2 = data(sequenceSubset(d, ind{i}));
                    elseif isa(d, 'subjdepdata') 
                        d1 = data( subjectSubset(d, I1));
                        d2 = data( subjectSubset(d, ind{i}) );
                    else
                        d1 = subset(d, I1);
                        d2 = subset(d, ind{i});
                    end
%                     e_folds(fold) = evaluation(r, p1, p2, d1, d2, m);
                     res{i} = evaluation(r, p1, p2, d1, d2, m);
                end
                e_par(ind{i}) = res{i};
                e(i) = mean(e_par);
                fprintf(1,'k = %3.2f\tC = %u\tAverage %s loss on test data: %f\n', p1, p2, m, e(end));	
            end
            [e_best i_best] = min(e);
            if isempty(E_scale)
                E_scale = e_best;
                v = [P(i_best,1), P(i_best,2)];
            else
                if e_best < E_scale(end)    % - Only update if results are better
                    v = [P(i_best,1), P(i_best,2)];
                end
                E_scale(end+1) = e_best;
            end
            
			if length(E_scale) > 1
					fprintf(1, '\n\nFinished for this scale. Best value was %1.4f (impr. of %1.4f over previous scale) for parameters %f,%f\n\n', E_scale(end), E_scale(end-1) - E_scale(end), P1, P2);
			else
					fprintf(1, '\n\nFinished for first scale. Best value was %1.4f for parameters %f,%f\n\n', E_scale(end), v(1), v(2));
			end
			done = stopCondition(E_scale, difmin);
			if done
					fprintf(1, 'Stop condition met.\n');
            else
                % -- Reduce scale, get new searchSpace parameters
                [l1 r1 l2 r2] = newSearchParameters(S1(1,:), S2(:,1)', v(1), v(2), n_steps);
			end
	end

	% -- The first column of optp contains the unsigned prediction
	r = setParameter(r, 1:2, v);
	e = e_min;
end

function f = evaluation(s, k, C, d1, d2, m)
% - returns error for every test example
	s = setParameter(s, 1:2, [k C]);
    rr = [min(d1.Y), max(d1.Y)];
	s = train(s, d1);
	
% -- So, how good is this prediction?
	R2 = test(s,d2); 
    R2 = clipLabels(R2, rr);
    d2 = clipLabels(d2, rr);
    ev = cEval(d2, R2); % - Continuous eval. object
% -- Optimum depends on loss function
    switch m    % - Which loss function to use
        case 'linear'
            [F f] = linearLoss(ev, rr);
        case 'square'
            [F f] = squareLoss(ev, rr);
        case 'exp'
            [F f] = expLoss(ev, rr);
        case 'radial'
            [F f] = radialLoss(ev);
        case 'radialSquare'
            [F f] = radialSquareLoss(ev);
            otherwise
                error('Loss function not implemented');
    end

end

function [S1 S2] = searchSpace(l1, r1, l2, r2, n)
% l = left boundary, r is right boundary, n is nr of steps in search space, 1 denotes par. 1

% -- Convert to exp. scale
l1_e = log(l1);
r1_e = log(r1);
l2_e = log(l2);
r2_e = log(r2);

s1 = exp(l1_e:(r1_e-l1_e)/(n-1):r1_e);
s2 = exp(l2_e:(r2_e-l2_e)/(n-1):r2_e);

[S1 S2] = meshgrid(s1, s2);
end

function I = findClosest(S, p)
% - Finds the nearest hit
d = inf;
for i = 1:length(S)
        d_i = abs(S(i)-p);
        if d_i < d
                d = d_i;
                I = i;
        end
end
end

function [L1 R1 L2 R2] = newSearchParameters(S1, S2, p1, p2, n)

I1 = findClosest(S1, p1);
I2 = findClosest(S2, p2);

if I1 == 1
        % -- Extend edge to the left
        L1 = exp(log(S1(1)) - (log(S1(2))-log(S1(1))));
        R1 = S1(2);
else
        if I1 == length(S1)
                L1 = S1(end-1);
                R1 = exp(log(S1(end)) + (log(S1(end))-log(S1(end-1))));
        else
                L1 = S1(I1-1);
                R1 = S1(I1+1);
        end
end

if I2 == 1
        % -- Extend edge to the left
        L2 = exp(log(S2(1)) - (log(S2(2))-log(S2(1))));
        R2 = S2(2);
        else
                if I2 == length(S2)
                        L2 = S2(end-1);
                        R2 = exp(log(S2(end)) + (log(S2(end))-log(S2(end-1))));
                else
                        L2 = S2(I2-1);
                        R2 = S2(I2+1);
        end
end
% -- Now we included the old left/right points. We don't want to compute
%    values for the same parameters twice
l1_e = log(L1);
l2_e = log(L2);
r1_e = log(R1);
r2_e = log(R2);

s1 = exp(l1_e:(r1_e-l1_e)/(n+1):r1_e);
s2 = exp(l2_e:(r2_e-l2_e)/(n+1):r2_e);
L1 = s1(2); R1 = s1(end-1);
L2 = s2(2); R2 = s2(end-1);
end

function B = stopCondition(e, m)
%B = done of not (1 means we're done)
% -- Stop condition: e hasn't improved for this new scale
% m: mindif between rounds
if length(e) == 1
        B = 0;
else
        if  (e(end) - e(end-1)) < -m
                B = 0;
        else
                B = 1;
        end
end
end