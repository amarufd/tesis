function [r v e V] = optimise3dep(r, d, n, m, difmin)

%OPTIMISE3DEP - parameter optimisation for 3 mutually dependent variables
%Optimises the epsilon, C and the karg parameter. Can be modified for other params
%
% Based on optimise for mSvm. Assumes that the 3d parameter can be
% optimised starting high and finding a global minimum by decreasing its
% value.
%
%IN:  r: mSvr object
%     d1: train data
%     d2: test data
%     m: loss function to use
%     difmin: minimum increase between rounds
%OUT: r: mSvr with optimal parameters
%     v: values of optimal parameters 
%     e: error with best parameters

	if ~exist('difmin', 'var')
		difmin = 0.001;
    end

	e_min= 1;   % - Working with error now

	% -- Assume that we have three parameters. Don't know what they mean, necessarily
	th = 0.0001;	% - Difference threshold
	l1 = 0.5;
	r1 = 5;
	n_steps = 4;
	l2 = 1; r2 = 100;
	done = 0;   % - We've just started!

	e = []; V = []; E_scale = [];
	
	CV = cvNfolds(d,n);  % 1 tells cvNfolds that the div. is for a regr.
	l3 = 0.001; r3 = 0.2;
	scaleNr = 0;
	while ~done
			% -- Get parameter search space (exponential search space, btw)
			[S1 S2 S3] = searchSpace(l1, r1, l2, r2, l3, r3, n_steps);
			e_scale = [];
			scaleNr = scaleNr+1;
			P1 = S1(1);
			P2 = S2(1);
			P3 = r3;
			for p1 = S1
				for p2 = S2
					e_eps = [0.999 0.998];
					p3 = 0.2;	% -- Start with zero-insensitive loss set to 0.4, then halve it in each step
					while  (e_eps(end-1) - e_eps(end)) > th && p3 >= 0.01
						e_folds = zeros(n, 1);
						for fold = 1:n
							I1 = CV(fold).train; I2 = CV(fold).test;
							if isa(d, 'seqdata') 
								d1 = data(sequenceSubset(d, I1));
								d2 = data(sequenceSubset(d, I1));
							else
								d1 = subset(d, I1);
								d2 = subset(d, I2);
							end
							e_folds(fold) = evaluation(r, p1, p2, p3, d1, d2, m);
						end
						e(end+1) = mean(e_folds);
						e_eps(end+1) = e(end);
						e_scale(end+1) = e(end);
						fprintf(1,'Scale %u: k = %3.4f\tC = %3.4f\tEpsilon=%3.4f\tAverage %s loss on test data: %f\n', scaleNr, p1, p2, p3, m, e(end));
						
						% -- Keep a 4D matrix for visualisation
						V(end+1,:) = [p1 p2 p3 e(end)];
						if (e_min - e(end)) > th
								fprintf(1, '\tNew best performance. Was %1.4f, now is %1.4f (%1.4f difference)\n', e_min, e(end), e(end)-e_min);
								e_min = e(end);
								v  = [p1 p2 p3];
						end
						% -- See if last prediction of current scale
						% improves over the best of this scale
						if  (min(e_scale(1:end-1)) - e_scale(end)) > th
								P1 = p1;
								P2 = p2;
								P3 = p3;
						end
						p3 = p3/2;
					end
				end
			end
			% -- Reduce scale, get new searchSpace parameters
			[l1 r1 l2 r2 l3 r3] = newSearchParameters(S1, S2, S3, v, n_steps);
			E_scale(end+1) = min(e_scale);
			if length(E_scale) > 1
					fprintf(1, '\n\nFinished for scale %u. Best value was %1.4f (impr. of %1.4f over previous scale) for parameters %f, %f, %f\n\n', scaleNr, E_scale(end), E_scale(end-1) - E_scale(end), P1, P2, P3);
			else
					fprintf(1, '\n\nFinished for first scale. Best value was %1.4f for parameters %f, %f, %f\n\n', E_scale(end), P1, P2, P3);
			end
			done = stopCondition(E_scale, difmin, scaleNr);
			if done
					fprintf(1, 'Stop condition met.\n');
			end
	end

	% -- The first column of optp contains the unsigned prediction
	r = setParameter(r, 1:3, v);
	e = e_min;
end

function f = evaluation(s, k, C, eps, d1, d2, m)
	s = setParameter(s, 1:3, [k C eps]);
    rr = [min(d1.Y), max(d1.Y)];
	s = train(s, d1);
	
% -- So, how good is this prediction?
	R2 = test(s,d2); 
    R2 = clipLabels(R2, rr);
    d2 = clipLabels(d2, rr);
    ev = cEval(d2, R2); % - Continuous eval. object
% -- Optimum is highest possible F-measure on test data
    switch m    % - Which loss function to use
        case 'corr'
            f = corrcoefLoss(ev);
        case 'linear'
            f = linearLoss(ev, rr);
        case 'square'
            f = squareLoss(ev, rr);
        case 'exp'
            f = expLoss(ev, rr);
        case 'radial'
            f = radialLoss(ev);
        case 'radialSquare'
            f = radialSquareLoss(ev);
        otherwise
            error('Loss function not implemented');
    end

end

function [S1 S2 S3] = searchSpace(l1, r1, l2, r2, l3, r3, n)
	% l = left boundary, r is right boundary, n is nr of steps in search space, 1 denotes par. 1

	% -- Convert to exp. scale
	l1_e = log(l1);
	r1_e = log(r1);
	l2_e = log(l2);
	r2_e = log(r2);
	l3_e = log(l3);
	r3_e = log(r3);

	S1 = exp(l1_e:(r1_e-l1_e)/(n-1):r1_e);
	S2 = exp(l2_e:(r2_e-l2_e)/(n-1):r2_e);
	S3 = exp(l3_e:(r3_e-l3_e)/(n-1):r3_e);
end

function I = findClosest(S, p)
% - Finds the nearest hit
d = inf;
for i = 1:length(S)
        d_i = abs(S(i)-p);
        if d_i < d
                d = d_i;
                I = i;
        end
end
end

function [L1 R1 L2 R2 L3 R3] = newSearchParameters(S1, S2, S3, v, n)

I1 = findClosest(S1, v(1));
I2 = findClosest(S2, v(2));
I3 = findClosest(S3, v(3));

if I1 == 1
        % -- Extend edge to the left
        L1 = exp(log(S1(1)) - (log(S1(2))-log(S1(1))));
        R1 = S1(2);
else
        if I1 == length(S1)
                L1 = S1(end-1);
                R1 = exp(log(S1(end)) + (log(S1(end))-log(S1(end-1))));
        else
                L1 = S1(I1-1);
                R1 = S1(I1+1);
        end
end

if I2 == 1
	% -- Extend edge to the left
	L2 = exp(log(S2(1)) - (log(S2(2))-log(S2(1))));
	R2 = S2(2);
else
	if I2 == length(S2)
			L2 = S2(end-1);
			R2 = exp(log(S2(end)) + (log(S2(end))-log(S2(end-1))));
	else
			L2 = S2(I2-1);
			R2 = S2(I2+1);
	end
end

if I3 == 1
	% -- Extend edge to the left
	L3 = exp(log(S3(1)) - (log(S3(2))-log(S3(1))));
	R3 = S3(2);
else
	if I3 == length(S3)
			L3 = S3(end-1);
			R3 = exp(log(S3(end)) + (log(S3(end))-log(S3(end-1))));
	else
			L3 = S3(I3-1);
			R3 = S3(I3+1);
	end

end
% -- Now we included the old left/right points. We don't want to compute
%    values for the same parameters twice
l1_e = log(L1);
l2_e = log(L2);
r1_e = log(R1);
r2_e = log(R2);
l3_e = log(L3);
r3_e = log(R3);

s1 = exp(l1_e:(r1_e-l1_e)/(n+1):r1_e);
s2 = exp(l2_e:(r2_e-l2_e)/(n+1):r2_e);
s3 = exp(l3_e:(r3_e-l3_e)/(n+1):r3_e);
L1 = s1(2); R1 = s1(end-1);
L2 = s2(2); R2 = s2(end-1);
L3 = s3(2); R3 = s3(end-1);
end

function B = stopCondition(e, m, s)
%B = done or not (1 means we're done)
% -- Stop condition: e hasn't improved for this new scale
% m: mindif between rounds, s is the current scale number
    if length(e) == 1
            B = 0;
    else
            if  (e(end) - e(end-1)) < -m
                    B = 0;
            else
                    B = 1;
            end
            if s >= 3	% -- stop after three scales tried
                B = 1;
            end
    end
end