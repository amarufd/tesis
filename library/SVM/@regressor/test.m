function y = test(r, d, id)

%TEST - tests a regressor
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2104.2008  
%  Mod. by B. Martinez to include preloading (2012)
%
%IN:  r: trained regressor object
%     d: data/seqdata object. Can have multiple (continuous) labels
%     id: indentifier when regressor model is preloaded
%OUT: y: regressor prediction 

if isa(d, 'seqdata')
    if exist('id','var')
        y  = test(r.regr, d, id);
    else
        y  = test(r.regr, d);
    end
else
    if exist('id','var')
        y  = test(r.regr, d, id);
    else
        y = test(r.regr, d);
    end
end

