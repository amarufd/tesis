function [e, T, Y, cvdef] = crossvalidate(a, d, n, m, start, cvtype, cvset, nopt)

%CROSSVALIDATE - cross validation scheme for regressor
%
% AUTHOR:	M.F. Valstar
% CREATED:	22042008
%
%IN:  a: classifier object [Svm]
%     d: data object
%     n: nr of cross validation rounds
%     m: loss function
%     start: fold to start with (for condor-applications)
%     cvtype: cross validation type. Either 'loo' for leave-one-out, 'lgo' leave-group-out or 'nfold' for nfold
%     cvset: cross validation definition to use. If you don't want to specify this, pass an empty array
%     nopt: nr of cross validation rounds in parameter optimisation to use
%     striped: optional: set striped cross validation (also for parameter opt) defautl is no
%OUT: e: evaluation of the cross validation [N]
%     T: true labels [data object]
%     Y: predictions [data object]
%     cvdef: the used cross validation 

if nargin < 5
	start = 1;
end
if nargin < 6
	cvtype = 'nfold';
end
if nargin < 7
	cvset = [];
end
if nargin < 8
	nopt = 3;
end

if isempty(cvset)
	switch lower(cvtype)
		case 'nfold'
			cvdef = cvNfolds(d, n, 1); % - Make sure to set data type to regressor
		case 'loo'
			cvdef = cvleaveoneout(d);
		case 'lgo'
			error('CV definition MUST be specified for cv type Leave Group Out!');
		otherwise
			error('Unknown cross validation type');
	end
else
	cvdef = cvset;
end

fprintf(1, 'Normalising data...');
d = normal(d);

for i = start:length(cvdef)
	% Copy the input classifier
	A = regressor(a);
	if isa(d, 'data')
		D = subset(d, cvdef(i).train);
		T{i} = subset(d, cvdef(i).test);
	else
		D = data(sequenceSubset(d, cvdef(i).train));
		T{i} = data(sequenceSubset(d, cvdef(i).test));
	end
	A = parameteroptimisation(A, D, nopt, m);
	A = train(A, D);
	Y{i} = test(A, T{i});
	if isa(d, 'data')
		savefold(i, cvdef(i).test', get_y(T{i}), get_y(Y{i}));
	else
		savefold(i, zeros(nrSamples(T{i}),1), get_y(T{i}), get_y(Y{i}));
	end
	e(i) = cEval(T{i}, Y{i});
end

function savefold(i, nrs, T, Y);
savedat = horzcat(nrs, T, Y);
fn = sprintf('cv_fold%.3u.mat', i);
save(fn, 'savedat', '-ASCII');
