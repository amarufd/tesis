function r = setParameter(r, n, v)

%SETPARAMETER - sets parameters of a regressor object
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 2204.2008
%
%IN:  r: regressor object
%     n: parameter indices
%     v: parameter values
%OUT: r: regressor with updated parameter values

r.regr = setParameter(r.regr, n, v);

% -- Needs to check for regressor type, unfortunately
% if isa(r.regr, 'svr')
%         for i = 1:length(n)
%                 p_n = n(i); % - parameter number
%                 p_v = p(i); % - parameter value
%                 switch p_n
%                 case 1
%                         r.regr.rbf = p_v;
%                 case 2
%                         r.regr.C = p_v;
% 				case 3
% 						r.regr.epsilon = p_v; 
%                 end
%                 r.p(p_n) = p_v; % - The class implementation of parameter keeping
%         end
% end
% if isa(r.regr, 'mSvr')
%     for i = 1:length(n)
%         p_n = n(i); % - parameter number
%         p_v = p(i); % - parameter value
%         switch p_n
%             case 1
%                 r.regr = setSigma(r.regr, p_v);
%             case 2
%                 r.regr = setC(r.regr, p_v);
% 			case 3
% 				r.regr = setEpsilon(r.regr, p_v);
%         end
%         r.p(p_n) = p_v; % - The class implementation of parameter keeping
%     end
% end
