function d = display(r)

%DISPLAY - displays a regressor object
%
% AUTHOR:       M.F. Valstar
% CREATED:      1301.2010
%
%IN:  r: adaBoostReg object
%OUT: d: string

	reg_type = class(r.regr);

	d = sprintf('\nRegressor of type %s', reg_type); 
	%for i = 1:length(r.p)
	%	d = horzcat(d, sprintf('Variable %s: %f\n', char(r.pNames{i}), r.p(i)));
	%end
	disp(d);
	display(r.regr);
	
end