function [r v e V] = optimise2dep(r, d, n, m, difmin)

%OPTIMISE2DEP - parameter optimisation for 2 mutually dependent variables
%Optimises the C and the karg parameter. Can be modified for other params
%
% Based on optimise for mSvm
%
%IN:  r: mSvr object
%     d: data
%     n: nr folds
%     m: loss function to use
%     difmin: minimum increase between rounds
%OUT: r: mSvr with optimal parameters
%     v: values of optimal parameters 
%     e: error with best parameters

	if nargin < 5
		difmin = 0.001;
	end


	plotornot = 0;
	if plotornot
		fig = figure;
	end

	e_min= 1;   % - Working with error now

	% -- Assume that we have two parameters. Don't know what they mean, necessarily
	l1 = 1;
	r1 = 100;
	n_steps = 4;
	l2 = 0.01; r2 = 0.2;   % - For epsilon
	done = 0;   % - We've just started!

	e = []; V = []; E_scale = [];
	
	CV = cvNfolds(d,n);  % - Make sure to set that we use regression data (the 1)

	while ~done
			% -- Get parameter search space (exponential search space, btw)
			[S1 S2] = searchSpace(l1, r1, l2, r2, n_steps);
			e_scale = [];
			P1 = S1(1,1);
			P2 = S2(1,1);
			for i = 1:size(S1,1)
					for j = 1:size(S1,2)
							p1 = S1(i,j);
							p2 = S2(i,j);
							e_folds = zeros(n, 1);
							for fold = 1:n
								I1 = CV(fold).train; I2 = CV(fold).test;
								if isa(d, 'seqdata') 
									d1 = data(sequenceSubset(d, I1));
									d2 = data(sequenceSubset(d, I2));
								else
									d1 = subset(d, I1);
									d2 = subset(d, I2);
								end
								e_folds(fold) = evaluation(r, p1, p2, d1, d2, m);
							end
							e(end+1) = mean(e_folds);
							fprintf(1,'C = %3.2f\teps = %u\tAverage %s loss on test data: %f\n', p1, p2, m, e(end));
							e_scale(end+1) = e(end);
							% -- Keep a 3D matrix for visualisation
							V(end+1,:) = [p1 p2 e(end)];
							if e(end) < e_min
									fprintf(1, '\tNew best performance. Was %1.4f, now is %1.4f (%1.4f difference)\n', e_min, e(end), e(end)-e_min);
									e_min = e(end);
									v  = [p1 p2];
							end
							if e_scale(end) < min(e_scale)
									P1 = p1;
									P2 = p2;
							end
					end
			end
			% -- Reduce scale, get new searchSpace parameters
			[l1 r1 l2 r2] = newSearchParameters(S1(1,:), S2(:,1)', v(1), v(2), n_steps);
			E_scale(end+1) = min(e_scale);
			if length(E_scale) > 1
					fprintf(1, '\n\nFinished for this scale. Best value was %1.4f (impr. of %1.4f over previous scale) for parameters %f,%f\n\n', E_scale(end), E_scale(end-1) - E_scale(end), P1, P2);
			else
					fprintf(1, '\n\nFinished for first scale. Best value was %1.4f for parameters %f,%f\n\n', E_scale(end), P1, P2);
			end
			done = stopCondition(E_scale, difmin);
			if done
					fprintf(1, 'Stop condition met.\n');
			end
	end

	% -- The first column of optp contains the unsigned prediction
	r = setParameter(r, 2:3, v);
	e = e_min;
end

function f = evaluation(s, C, eps, d1, d2, m)
	s = setParameter(s, 2:3, [C, eps]);
    rr = [min(d1.Y), max(d1.Y)];
	s = train(s, d1);
	
% -- So, how good is this prediction?
	R2 = test(s,d2); 
    R2 = clipLabels(R2, rr);
    d2 = clipLabels(d2, rr);
    ev = cEval(d2, R2); % - Continuous eval. object
% -- Optimum depends on loss function
    switch m    % - Which loss function to use
            case 'corr'
                f = corrcoefLoss(ev);
            case 'linear'
                f = linearLoss(ev, rr);
            case 'square'
                f = squareLoss(ev, rr);
            case 'exp'
                f = expLoss(ev, rr);
            case 'radial'
                f = radialLoss(ev);
        case 'radialSquare'
            f = radialSquareLoss(ev);
            otherwise
                error('Loss function not implemented');
    end

end

function [S1 S2] = searchSpace(l1, r1, l2, r2, n)
% l = left boundary, r is right boundary, n is nr of steps in search space, 1 denotes par. 1

% -- Convert to exp. scale
l1_e = log(l1);
r1_e = log(r1);
l2_e = log(l2);
r2_e = log(r2);

s1 = exp(l1_e:(r1_e-l1_e)/(n-1):r1_e);
s2 = exp(l2_e:(r2_e-l2_e)/(n-1):r2_e);

[S1 S2] = meshgrid(s1, s2);
end

function I = findClosest(S, p)
% - Finds the nearest hit
d = inf;
for i = 1:length(S)
        d_i = abs(S(i)-p);
        if d_i < d
                d = d_i;
                I = i;
        end
end
end

function [L1 R1 L2 R2] = newSearchParameters(S1, S2, p1, p2, n)

I1 = findClosest(S1, p1);
I2 = findClosest(S2, p2);

if I1 == 1
        % -- Extend edge to the left
        L1 = exp(log(S1(1)) - (log(S1(2))-log(S1(1))));
        R1 = S1(2);
else
        if I1 == length(S1)
                L1 = S1(end-1);
                R1 = exp(log(S1(end)) + (log(S1(end))-log(S1(end-1))));
        else
                L1 = S1(I1-1);
                R1 = S1(I1+1);
        end
end

if I2 == 1
        % -- Extend edge to the left
        L2 = exp(log(S2(1)) - (log(S2(2))-log(S2(1))));
        R2 = S2(2);
        else
                if I2 == length(S2)
                        L2 = S2(end-1);
                        R2 = exp(log(S2(end)) + (log(S2(end))-log(S2(end-1))));
                else
                        L2 = S2(I2-1);
                        R2 = S2(I2+1);
        end
end
% -- Now we included the old left/right points. We don't want to compute
%    values for the same parameters twice
l1_e = log(L1);
l2_e = log(L2);
r1_e = log(R1);
r2_e = log(R2);

s1 = exp(l1_e:(r1_e-l1_e)/(n+1):r1_e);
s2 = exp(l2_e:(r2_e-l2_e)/(n+1):r2_e);
L1 = s1(2); R1 = s1(end-1);
L2 = s2(2); R2 = s2(end-1);
end

function B = stopCondition(e, m)
%B = done of not (1 means we're done)
% -- Stop condition: e hasn't improved for this new scale
% m: mindif between rounds
if length(e) == 1
        B = 0;
else
        if  (e(end) - e(end-1)) < -m
                B = 0;
        else
                B = 1;
        end
end
end