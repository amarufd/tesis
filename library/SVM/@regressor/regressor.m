function r = regressor(varargin)

%REGRESSOR - constructor of regressor object 
%
% AUTHOR:	M.F. Valstar
% CREATED:	0811.2007
%
%Ways to call constructor and order of input variables:
% A: to create an empty new object: []
% B: to copy a regressor object: [regressor]
% C: new object with 
%     1: regression object (i.e. svr, or relvm_r, see spider toolbox)
%     2: p: parameter values
%     3: pNames: parameter names (cell string)
%
%OUT: r: regressor object
%
%Typical use: featureselect, parameteroptimisation, train, test. Or use crossvalidate.


if nargin == 0 % - Default constructor
	r.regr = mSvr;   % -- Default is Support Vector Regressor
	r = class(r, 'regressor');
else 
	if isa(varargin{1}, 'regressor')
		r = varargin{1};
	else
		switch length(varargin)
		case 1
			r.regr = mSvr( varargin{1} );
		otherwise
				error('Too many input arguments');
		end
		r = class(r, 'regressor');
	end
end
