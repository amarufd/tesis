function r = train(r, d)

%TRAIN - trains a regressor
%
%  AUTHOR:  M.F. Valstar
%  CREATED: 0204.2008
% 
%IN:  r: regressor object
%     d: data object. Can have multiple (continuous) labels
%OUT: r: trained regressor object

if isa(d, 'seqdata')
        [r.regr] = train(r.regr, data(d));
else
        [r.regr] = train(r.regr, d);
end
