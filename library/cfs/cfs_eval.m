function f = cfs_eval(x, y)
%CFS_EVAL - evaluates feature subset value in cfs terms (see Mark Hall's
%thesis)
%
%IN: x: feature set (n examples x k dimensions)
%    y: label set

    [~, k] = size(x);
    
    rho = corr([y x]);    % - Compute correlation
    
    cy = rho(1, 2:end); % - Correlations between features and label
    rho_x = rho(2:end, 2:end);  % - Correlations between pairs of features
    
    r_cf = mean(abs(cy));
    if k == 1
        r_ff = 1;
    else
        sm = sum(sum(abs(triu(rho_x))))-k;
        r_ff = sm/(k*(k-1)/2);    % - Average of pair-wise feature correlation. Subtract diagonal entries (all 1)
    end
    f = (k*r_cf)/(sqrt(k + k*(k-1)*r_ff));

end