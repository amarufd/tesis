function [x_fs, x_nfs, c_best, I, I_nfs] = bestAddition(x_fs, x_nfs, y, I, I_nfs)
%BESTADDITION - finds and returns best feature to add to selected subset
%
%IN:  x_fs: selected feature subset
%     x_nfs: feature subset to consider
%     y: labels
%     I: indices of selected labels, relative to original
%     I_nfs: indices of not-selected features
%OUT: x_fs: updated selected feature subset
%     x_nfs: not selected feature subset, to be reconsidered in next round
%     i: index of selected feature relative to original (full) set
%     I: updated indices of selected labels, relative to original
%     I_nfs: updated index of not-selected features relative to original
%     set

    [~, k] = size(x_nfs);
    c = zeros(1, k);
    for i = 1:k
        x = [x_fs x_nfs(:,i)];
        c(i) = cfs_eval(x, y) ;
        %fprintf(1, '\t\tCFS value with addition of feature %u/%u:\t%f\n', i, k, c(i));
    end
    [c_best, i_best ] = max(c);
    
    % -- Update variables and return
    x_fs = [x_fs x_nfs(:,i_best)];
    x_nfs = [x_nfs(:, 1:i_best-1) x_nfs(:, i_best+1:end)];
    I = [I I_nfs(i_best)];
    I_nfs = [I_nfs(1:i_best-1) I_nfs(i_best+1:end)];
end