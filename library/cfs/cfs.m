function [x_fs I] = cfs(x, y)
%CFS - Finds the optimal feature subset according to cfs (see Mark Hall's
%thesis)
%
%IN:  x: feature set (n examples x k dimensions)
%     y: label set
%OUT: x_fs: feature subset
%     I: index of selected features relative to original feature set

    [~, k] = size(x);
    n = 3;
    I = [];
    c_best = 0;
    c = 0;
    m = 0;  % - Nr of rounds without improvement
    rnd = 0;
    
    x_fs = [];  % - Selected features
    x_nfs = x;  % - Not yet selected features
    I_nfs = 1:k;
    
    while (length(I) < k && m < n)
        rnd = rnd+1;
        [x_fs, x_nfs, c, I, I_nfs] = bestAddition(x_fs, x_nfs, y, I, I_nfs); 
        fprintf(1, 'Round %u of max %u\n\tBest additional feature is %u, new cfs value is %f\n', rnd, k, I(end), c);
        
        if c > c_best + 0.001
            d = c - c_best;
            fprintf(1, '\tImprovement of %f \n', d);
            c_best = c;
            m = 0;
        else
            m = m + 1;
            if m == 1
                fprintf(1, '\tNo improvement for 1 round\n');
            else
                fprintf(1, '\tNo improvement for %u rounds\n', m);
            end
        end
    end
    
    % -- Get rid of possible extraneous features
    x_fs = x_fs(:,1:end-m);
    I = I(1:end-m);
end