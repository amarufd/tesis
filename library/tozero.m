function [matriz] = tozero( matriz )

a=size(matriz);
for i=1:a(1,1)
    for j=1:a(1,2)
        if cellfun('isempty',matriz(i,j))
            matriz(i,j)={0};
        end
    end
end
matriz=cell2mat(matriz);

end