function [WandF] = doc_ter_frec_conmat(mat)
    dic={};
    dic=mat';
    dic=sort(dic);

    %%%%%%%%%%%part two

    WandF={};
    flag=1;
    t_length=length(dic);
    ind=1;
    j=1;

    if ~isempty(dic)
        dic(ind+1);
        while ind<=t_length
            if ind+1<=t_length
                f=1;
                while f && ind<t_length
                    if strcmp(dic(ind),dic(ind+1))
                        flag=0;
                        j=j+1;
                        ind=ind+1;
                    else
                        f=0;
                    end
                end
            end
            if flag==1
                WandF=[WandF;{dic(ind),1}];
                ind=ind+1;
            else
                WandF=[WandF;{dic(ind),j}];
                flag=1;
                j=1;
                ind=ind+1;
            end
        end

        WandF=WandF';
    else
        WandF=dic;
    end
end