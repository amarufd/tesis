function [dic_FT] = matriz_inc_ter_doc(dic_FT,doc)
'Matriz de incidencia termino documento'
dic_FT=dic_FT';
doc=doc';
s=size(dic_FT);
l=length(doc);

for i=1:l
    j=1;
    while j<s(1) && ~(strcmp(char(doc{i}),char(dic_FT{j})))
        j=j+1;
    end
    if strcmp(char(doc{i}),char(dic_FT{j}))
        dic_FT{j,s(2)+1}=1;
    else
        'agregar al diccionario'
    end
end
dic_FT=dic_FT';
end