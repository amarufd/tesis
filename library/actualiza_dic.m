%este programa recibe un archivo iniciado como "ejemplo={};" el cual el 
%ejemplo es el diccionario a crear y ademas debe recibir el
%texto, con estos dos valores se va creando el diccionario, lo que el
%usuario debe hacer es que el debe agregar el texto tantas veces como
%documentos tenga, y una ves terminada esta operacion debe almacenar el
%diccionario en una variable ".mat"
%
function [dic] = actualiza_dic(diccionario,text)
texto=text;
i = 1;
txt='';
dic={};
tem={};
flag=1;
temp=0;
eval(['save temp.mat ' 'tem'] );
for ind=1:length(texto)
    if isalpha_num(texto(ind))
        if flag==1
            'aqui 1'
            dic=[dic; txt];
            txt='';
            i=i+1;
            flag=0;
        end
        txt=[txt texto(ind)];
    elseif texto(ind)==''''
        texto(ind);
    else
        flag=1;
    end
    if (length(dic)/10000-floor(length(dic)/10000))==0 && ~(length(dic)==0)
        'crea temporal'
        eval(['load temp.mat ' 'tem'] );
        tem=[tem;dic];
        eval(['save temp.mat ' 'tem'] );
        clear tem;
        dic={};
        temp=1;
    end
end
if flag==1 && length(texto)>1
    'aqui 2'
	dic=[dic; txt];
end

if temp==1
    eval(['load temp.mat ' 'tem'] );
    dic=[tem;dic];
end
diccionario=diccionario';
dic=[dic;diccionario];
if length(texto)>1
    dic=sort(dic);
end
dic=unique(dic);
dic=dic';
eval(['delete temp.mat ' ''] );



end