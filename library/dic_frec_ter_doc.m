%

function [idf] = dic_frec_ter_doc(dic_FT)
'diccionario frecuencia por documento'
dic_FT=dic_FT';
idf={};
s=size(dic_FT);

for i=1:s(1)
    iidf=0;
    for j=2:s(2)
        if dic_FT{i,j}>=1
           iidf=iidf+1;
        end
    end
    if iidf>=1
        idf{i,1}=dic_FT{i,1};
        idf{i,2}=iidf;
    else
        idf{i,1}=dic_FT{i,1};
    end
end

idf=idf';

end