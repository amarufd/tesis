function [WandF] = doc_ter_frec(text)

texto=text;
i = 1;
txt='';
tic
dic={};
flag=1;
for ind=1:length(texto)
    if isalpha_num(texto(ind)),
        if flag==1, 
            dic=[dic; txt];
            txt='';
            i=i+1;
            flag=0;
        end;
        txt=[txt texto(ind)];
    elseif texto(ind)==''''
        texto(ind);
    else
        flag=1;
    end
    if (ind/10000-floor(ind/10000))==0
        ind
    end
end
toc
dic=sort(dic);

%%%%%%%%%%%part two

WandF={};
flag=1;
t_length=length(dic);
ind=1;
j=1;
dic(ind+1)
while ind<=t_length
    if ind+1<=t_length
        f=1;
        while f && ind<t_length
            if strcmp(dic(ind),dic(ind+1))
                flag=0;
                j=j+1;
                ind=ind+1;
            else
                f=0;
            end
        end
    end
    if flag==1
        WandF=[WandF;{dic(ind),1}];
        ind=ind+1;
    else
        WandF=[WandF;{dic(ind),j}];
        flag=1;
        j=1;
        ind=ind+1;
    end
end

WandF=WandF';

end