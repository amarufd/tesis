ESTAMBUL (AFP)
                    El gobierno de Turquía amenazó el lunes con recurrir a las Fuerzas Armadas para poner punto final a las protestas contra el primer ministro, Recep Tayyip Erdogan, en una jornada en la que dos poderosos sindicatos salieron a la calle en defensa de una huelga general que tuvo un apoyo apenas tímido.
Un día después de la demostración de fuerza del gobierno, que reunió a unas 100.000 personas en un acto de apoyo, el viceprimer ministro, Bulent Arinç, endureció todavía más el tono contra los manifestantes y agitó por primera vez el espectro de un llamado a las Fuerzas Armadas.
La policía "utilizará todos los medios que le son conferidos por ley", dijo Arinç, quien añadió: "Si eso no es suficiente, incluso las Fuerzas Armadas podrían ser utilizadas en las ciudades bajo la autoridad de los gobernadores".
Guardianas autoproclamadas de la Turquía laica, las Fuerzas Armadas han intervenido seguidamente en la política mediante golpes de Estado, pero Erdogan llevó adelante diversas purgas internas que dejaron diezmada a su jerarquía.
Dos poderoso sindicatos, la Confederación Nacional de los Obreros Revolucionarios (DISK) y la Confederación Sindical de los Empleados del Sector Público (KEK) decretaron para este lunes una huelga en apoyo a las protestas, que parecen enfrentar problemas desde que los manifestantes fueron desalojados de la plaza Taksim y del parque Gezi el sábado.
El Gobierno turco amenazó este lunes con recurrir al ejército para acallar las protestas contra el Gobierno que agitan al país desde hace más de dos semanas, y cuando dos importantes sindicatos convocaron una huelga general para apoyar a los manifestantes.Sin embargo, lejos de las marchas multitudinarias del 5 de junio, las dos organizaciones gremiales lograron movilizar apenas unos pocos miles de personas en Estambul y Ankara.
"Salir a la calle, ya es un avance", dijo a la AFP un manifestante que se identificó como Kerem. "Es la primera vez en Turquía que las personas salen a la calle sin organización política", añadió.
En Estambul, los manifestantes evitaron claramente cualquier conflicto con la policía que bloquea los accesos a la plaza Taksim, y desplegaron sus banderas a una cierta distancia.
Sin embargo, cuando los sindicatos ya empezaban a dispersar sus columnas de manifestantes, la policía atacó con bombas de gases lacrimógenos y balas de goma, a lo que los sindicalistas respondieron arrojando palos y piedras.
-OLA DE DETENCIONES-
"Hay tentativas de sacar a la gente a la calle para acciones ilegales como una huelga", pero las fuerzas de seguridad "no lo permitirán", alertó el ministro del Interior, Muamer Guler.
Después de desalojar brutalmente el sábado a los manifestantes de la plaza Taksim y del parque Gezi, Erdogan no dio señales de flexibilizar el tono en un acto gigantesco de apoyo al gobierno realizado el domingo en la noche en Estambul.
Durante casi dos horas, Erdogan arremetió contra los manifestantes y prometió perseguir a los "responsables" por las protestas, incluyendo a los médicos que atendieron a manifestantes heridos o a los hoteles de lujo que les dieron refugio.
"Conocemos muy bien a los que han protegido y a los que han cooperado con los terroristas", alertó el primer Ministro.
En una prueba de la seriedad de sus palabras, el domingo en la noche más de 600 personas fueron arrestadas en Estambul y Ankara. Decenas de miles de agentes de policía se desplegaron en esas dos ciudades y en Esmirna, en el oeste del país.
Según el último balance del sindicato de médicos turcos, publicado a principios de la semana, cuatro personas murieron y cerca de 7.500 fueron heridas.
En este contexto, la presión sobre Erdogan proviene también de países europeos.
La represión es "demasiado dura", declaró la jefa del gobierno alemán, Angela Merkel, en una entrevista que el canal de televisión RTL difundirá en la noche del lunes.
Ya el domingo el Consejo de Europa había pedido a Turquía que evitase "toda escalada suplementaria de la violencia".
El secretario general del Consejo de Europa, Thorbjorn Jagland, formuló un "llamado a todas las partes a continuar el diálogo", y agregó que "toda escalada suplementaria de la violencia debe ser evitada".
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  