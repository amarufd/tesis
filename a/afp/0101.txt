ATENAS (AFP)
                    El pequeño partido de izquierda griego Dimar "abandonará" la coalición gubernamental dirigida por el conservador primer ministro Antonis Samaras, informó este viernes el ministro de Reforma de la Administración, Antonis Manitakis.
Tras una reunión del grupo parlamentario Dimar con el jefe del gobierno sobre la cuestión del cierre de la radiotelevisión pública, Manitakis indicó que presentará "su dimisión" tras "la decisión del partido de retirarse del gobierno".
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  