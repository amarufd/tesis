PEKÍN (AFP)
                    La extradición de la fuente Richard Snowden a Estados Unidos sería una "traición" de la confianza depositada por el extrabajador de la NSA en la democracia en Hong Kong y una "pérdida de prestigio" para Pekín, dijo este lunes el diario oficial chino Global Times, en el posicionamiento del gigante asiático más directo.
"Extraditar a Snowden a Estados Unidos no sólo sería una traición de la confianza de Snowden, sino también una decepción de las expectativas del mundo entero", escribió el periódico en inglés del grupo del Diario del Pueblo, órgano oficial del Partido Comunista Chino (PCC).
"Snowden cree en la democracia y la libertad de Hong Kong", donde se encuentra escondido, asegura el diario para quien el exagente de la CIA, que reveló programas estadounidenses de vigilancia masiva en internet y de las comunicaciones telefónicas, "no hizo daño a nadie" y se limitó a "alertar sobre la violación de los derechos civiles por el Gobierno" estadounidense. "En consecuencia, sería una pérdida de prestigio tanto para el gobierno de (...) Hong Kong como para el gobierno central chino si Snowden fuera extraditado", advirtió el Global Times.
"La imagen de Hong Kong quedaría empañada para siempre", añadió y aseguró que "la reacción local" a una posible extradición "provocaría más problemas" en la excolonia británica y en China. "El creciente poder de China atrae a la gente para pedir asilo", continúa el diario para quien "es inevitable y debe servir para acumular prestigio moral".
El Gobierno chino prácticamente no se pronunció desde que estallara este caso y únicamente una portavoz de las Asuntos Exteriores chinas declaró la semana pasada que no tenía "ninguna información" que proporcionar.
Según un sondeo publicado el domingo, la mitad de los hongkoneses están en contra de una extradición. Varios cientos de personas se manifestaron el sábado contra esta posibilidad.
Hong Kong, donde la justicia es independiente, se beneficia de una cierta autonomía bajo la tutela de China y firmó hace mucho tiempo un acuerdo de extradición con Estados Unidos. Sin embargo, Pekín tiene el derecho a vetar las decisiones de su territorio.
Edward Snowden, refugiado desde el 20 de mayo en Hong Kong, está siendo investigado por el FBI pero todavía no se presentó ninguna petición oficial de extradición.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  