RIGA (AFP)
                    Los bomberos fueron llamados la noche del jueves para combatir un incendio en el techo del Castillo de Riga, que tiene varios siglos de antigüedad y es la sede de la presidencia de Letonia y que actualmente está en proceso de renovación.
Al parecer no hay muertos o heridos, según la portavoz del servicio de bomberos, Viktorija Sembele. Algunos testigos dijeron a AFP que gran parte del techo era presa del fuego y que bomberos intentaban controlarlo bajo la mirada de cientos de espectadores. "Unos 100 metros cuadrados se han incendiado y más de 10 equipos de bomberos están trabajando duro", dijo Sembele. "Esto es grave, ya que es un edificio histórico del casco antiguo de Riga", dijo a AFP, añadiendo que la causa del siniestro era aún desconocida.
El ejército está está ayudando a los bomberos con un helicóptero capaz de verter sobre el fuego el agua cargada del cercano río Daugava. Un barco de crucero atracado cerca del castillo debió ser desplazado para permitir a los barcos que luchan contra los incendios acercar agua desde el río, dijo la portavoz. El castillo de Riga, que parcialmente data de principios del siglo XIII, alberga un Museo de Historia y, normalmente, también las oficinas del presidente letón, Andris Berzins. Debido a los trabajos de renovación, las oficinas presidenciales han sido trasladados temporalmente a otro establecimiento histórico de la capital letona.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  