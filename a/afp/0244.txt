MOSCÜ (AFP)
                    Los rebeldes del Cáucaso del Norte negaron este domingo cualquier implicación en el atentado de Boston del pasado lunes, del que se acusa a dos hermanos de origen checheno residentes en EEUU.
"El comando de Vilayat Dagestan Muyahidín (...) declara que los luchadores del Cáucaso no llevan a cabo ninguna acción militar contra Estados Unidos", dice un comunicado colgado en la página web Kavkacenter.com. "Sólo combatimos a Rusia, que no sólo es responsable de la ocupación del Cáucaso, sino de crímenes monstruosos contra los musulmanes", añade el grupo rebelde.
La prensa estadounidense asegura que el FBI está estudiando los posibles vínculos entre los dos sospechosos -los hermanos Dzhokhar y Tamerlan Tsarnaev- y el movimiento del Emirato del Cáucaso liderado por el temido 'señor de la guerra' Doku Umarov. Dicha información asegura que las autoridades estadounidenses están interesadas en particular en el Vilayat Dagestan, una rama del grupo de Umarov.
Daguestán es una república vecina de Chechenia con una gran comunidad de la minoría chechena. También ha sido una de las regiones más violentas de Rusia desde el final, hace cerca de una década, de la última de las dos guerras que vivió Chechenia tras el desmoronamiento de la Unión Soviética.
Las autoridades rusas también aseguraron que están confirmando eventuales vínculos de los dos hermanos con los rebeldes, pero que hasta ahora no ha encontrado ninguna prueba. "Por ahora, no tenemos ninguna información creíble sobre la implicación de los hermanos Tsarnaev con el movimiento del Emirato del Cáucaso", dijo una fuente de seguridad anónima a la agencia de noticias Interfax.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  