LONDRES (AFP)
                    photo0
El ministro británico de Exteriores, William Hague, y su homólogo ecuatoriano, Ricardo Patiño, no lograron ningún avance este lunes en el caso del fundador de WikiLeaks, el australiano Julian Assange, refugiado en la embajada ecuatoriana en Londres desde hace un año, indicó el ministerio británico.
Hague y Patiño "tuvieron una entrevista bilateral de 45 minutos" y "se pusieron de acuerdo en formar un grupo de trabajo para encontrar una solución diplomática al caso de Julian Assange pero no se logró ningún avance substancial", indicó un comunicado del ministerio de Relaciones Exteriores de Gran Bretaña.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  