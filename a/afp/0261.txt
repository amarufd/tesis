MARSELLA (AFP)
                    El tribunal correccional de Marsella (sur de Francia), que juzga desde el miércoles el caso de las prótesis mamarias adulteradas de la firma francesa PIP rechazó este jueves dos recursos de constitucionalidad presentados por la defensa, por lo que el proceso continúa.
Al reanudarse la audiencia este jueves por la mañana, la jueza Claude Vieillard, que preside el tribunal, rechazó el pedido de los abogados de dos de los acusados, que deseaban que fueran transmitidas al tribunal de casación dos cuestionamientos de constitucionalidad.
Los abogados de Hannelore Font y Claude Couty, directivos de PIP, argumentaban que la manera como la fiscalía organizó el procedimiento afecta los derechos de la defensa.
Después de deliberar, el tribunal rechazó los dos recursos, considerándolos "carentes de carácter serio". "Por tanto la audiencia prosigue y el tribunal va a examinar ahora los incidentes relevados por los acusados", agregó la jueza.
Si el tribunal hubiera aceptado esos recursos, el proceso por este escándalo de repercusiones mundiales hubiera sido aplazado 'sine die'.
En este proceso, Jean-Claude Mas, fundador de la empresa PIP (Poly Implant Prothèse) y otros cuatro directivos de la firma comparecen por engaño agravado y estafa por haber utilizado para los implantes mamarios un gel de silicona impropio para uso médico. Esas prótesis fueron implantadas a decenas de miles de mujeres en Francia y en el extranjero, en particular en América Latina.
Los acusados son pasibles de cinco años de cárcel.
Más de 5.200 mujeres presentaron demanda en este proceso, que durará en principio hasta el 17 de mayo.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  