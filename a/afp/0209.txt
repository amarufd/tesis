RÏO DE JANEIRO (AFP)
                    Más de 200.000 personas protestaron el lunes en las principales ciudades de Brasil contra los multimillonarios gastos del Mundial-2014, incluido en Rio de Janeiro, una de las sedes de la Copa Confederaciones, donde hubo escenas de caos con saqueos, vandalismo y enfrentamientos con la policía.
Un pequeño grupo de manifestantes intentó invadir la Asamblea Legislativa de Rio, en el centro de la ciudad. Tiraron fuegos artificiales, cócteles molotov y piedras contra policías militares apostados en el edificio, constató la AFP.
También prendieron fuego a un coche y a basura en las inmediaciones y quebraron vidrios de bancos y tiendas cercanas, saqueando negocios mientras el resto de los manifestantes les gritaba "¡Ladrones!" y "¡Sin vandalismo!".
La policía intentó dispersarlos con gases lacrimógenos y balas de goma, así como con balas de plomo al cielo. Unos 80 policías aún están refugiados dentro de la Asamblea, y cinco resultaron heridos, según la policía militar.
La Asamblea sigue rodeada por manifestantes. Una hoguera arde frente al edificio.
Unas 100.000 personas protestan en Rio, según la policía militar.
En todo el país, los manifestantes convocados a través de las redes sociales y sin liderazgo político o social definido, denuncian el alza del precio del transporte y piden más inversiones en este sector, así como en salud y en educación.
-EN LA CASA DEL PUEBLO-
Más de 200.000 personas protestaron el lunes en las principales ciudades de Brasil contra los multimillonarios gastos del Mundial-2014, incluido en Rio de Janeiro, una de las sedes de la Copa Confederaciones, donde hubo escenas de caos con saqueos, vandalismo y enfrentamientos con la policía.En Brasilia, más de 200 manifestantes eufóricos y portando banderas lograron subir al techo del Congreso nacional, aunque luego de un par de horas descendieron, según una periodista de la AFP.
"Llegamos a la Casa del Pueblo. Es el primer paso para demostrar que no somos un pueblo muerto, pensaban que pararíamos para ver el fútbol pero Brasil no es solo eso", dijo Bruno Pastrana, un estudiante de 24 años, sentado en el techo del Congreso junto a sus amigos.
Unas 5.000 personas rodean el entorno del Congreso, según la policía.
"Las manifestaciones pacíficas son legítimas y propias de la democracia", dijo la presidenta brasileña, Dilma Rousseff. "Es propio de los jóvenes manifestarse", aseguró en el blog de la Presidencia.
La seguridad del Palacio Presidencial, que se encuentra cerca, fue reforzada.
En Belo Horizonte (este), donde la protesta reunió a unas 30.000 personas según los organizadores, la policía disparó gases lacrimógenos y balas de goma contra los manifestantes para impedirles que se acercaran al estadio Mineirao durante el partido Nigeria y Tahití por la Confederaciones (6-1).
En Porto Alegre (sur), la policía también dispersó a un grupo de manifestantes con gases lacrimógenos luego de que éstos destruyeron un autobús e incendiaron basura en la principal avenida de la ciudad.
Un pequeño grupo de manifestantes intentó invadir la Asamblea Legislativa de Rio, en el centro de la ciudad. Tiraron fuegos artificiales, cócteles molotov y piedras contra policías militares apostados en el edificio, constató la AFP.Estas son las mayores protestas callejeras en 21 años en Brasil -donde la población no acostumbra salir a la calle a expresar su descontento-, desde las manifestaciones de 1992 contra la corrupción del gobierno del expresidente Fernando Collor de Melo, que renunció durante su juicio político ante el Senado.
-QUIERO QUE BRASIL DESPIERTE-
Unas 65.000 personas manifiestan pacíficamente en el centro de Sao Paulo, estimó la encuestadora Datafolha.
"Quiero que Brasil despierte. No es solo por los pasajes, sino porque la educación y la salud son malas", dijo a la AFP Diyo Coelho, de 20 años, que marchaba en Sao Paulo junto a un grupo de amigos y llevaba flores en las manos.
En Rio, decenas de miles se manifestaban pacíficamente frente al Teatro Municipal.
"Estoy aquí para mostrar que Brasil no es sólo fútbol. Aquí no hay sólo fiesta. Hay otras preocupaciones, como la falta de inversiones en cosas realmente importantes, la salud y la educación", dijo a la AFP la abogada Daiana Venancio, de 24 años, que protestaba en Rio con una nariz de payaso.
"¿Qué sentido tiene hacer una fiesta para los gringos cuando Brasil está mal?", se preguntó Priscila Parra, una estudiante de física de 20 años.
Las manifestaciones comenzaron hace unos 10 días en Sao Paulo a raíz del alza del boleto de bus, tren y metro de 1,5 a 1,6 dólares, días antes del inicio de la Copa Confederaciones, un ensayo general del Mundial-2014 entre los campeones de cada continente, lo cual les ha dado una fuerte visibilidad dentro y fuera de fronteras.
Cronología de las protestas en Brasil que se iniciaron hace 10 días por el alza del precio del transporteRápidamente, se expandieron a otras ciudades y la causa se amplió a denuncias contra los 15.000 millones de dólares destinados por el gobierno para el Mundial de fútbol del año próximo.
Los manifestantes piden ese dinero para vivienda digna, salud y educación públicas de calidad, en este país donde existe aún una gran brecha entre pobres y ricos.
En su mayoría jóvenes de clase media, los manifestantes han denunciado la represión policial, especialmente el jueves pasado en Sao Paulo, donde hubo más de 230 detenidos y un centenar de heridos.
Las protestas ocurren en un momento de magro crecimiento económico en Brasil y una inflación en alza. Recientes encuestas señalaron por primera vez una caída en la aprobación del gobierno de Rousseff, sobre todo entre los más jóvenes y más ricos.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  