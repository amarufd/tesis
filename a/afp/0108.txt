WASHINGTON (AFP)
                    Estados Unidos despertó el martes con imágenes de carnicería y la inquietante realidad de que el terrorismo puede tocar en casa, un día después de un atentado que dejó tres muertos y más de 180 heridos en el maratón de Boston, poniendo fin a una racha de once años de suerte y tranquilidad, tras los ataques del 11 de setiembre.
Las imágenes que muestran dos explosiones que provocan llamas y esparcen una nube en el aire cerca a la meta de la mítica maratón de Boston el lunes, fueron difundidas continuamente por los canales de noticias.
Las fotos sangrientas ocuparon portadas de periódicos y las horribles imágenes de los heridos en el primer atentado en más de 11 años en suelo estadounidense, se pueden ver fácilmente en internet.
Al mismo tiempo, la lluvia de preguntas, comentarios, conferencias de prensa de líderes políticos y funcionarios, acompañados de historias desgarradoras acerca de las víctimas hicieron revivir días de septiembre de 2001.
Estados Unidos despertó el martes con imágenes de carnicería y la inquietante realidad de que el terrorismo puede tocar en casa, un día después de un atentado que dejó tres muertos y más de 180 heridos en el maratón de Boston, poniendo fin a una racha de once años de suerte y tranquilidad, tras los ataques del 11 de setiembre.Si bien la escala no es comparable a la de los ataques aéreos perpetrados por Al Qaida en Nueva York y Washington -en que murieron casi 3.000 personas-, el atentado en Boston desmiente la idea de que el suelo estadounidense es inmune al terrorismo.
"Con el 11 de setiembre, perdimos para siempre la ilusión de que los ataques como el que sacudió Boston ayer (lunes) solo ocurren en campos de batalla o países lejanos", dijo el jefe de la minoría republicana en el Senado, Mitch McConnell.
Pero en los últimos años una cierta ilusión de inmunidad se había restablecido gracias a "los esfuerzos vigilantes de nuestros militares, de los profesionales de inteligencia y las fuerzas del orden", admitió McConnell.
"Es la primera vez que hay muertos en territorio estadounidense a causa del terrorismo desde el 11 de setiembre", subrayó por su parte el senador demócrata Chuck Schumer. Lo que recuerda que "no debemos bajar nunca la guardia", añadió.
Mapa con los diez mayores atentados mortales en los EEUU desde 1970 (130 x 114 mm)La ausencia de novedades en las investigaciones, no impidió al presidente Barack Obama aparecer frente a cámaras por segunda vez en menos de 24 horas.
"Este fue un acto atroz y cobarde", proclamó. "Cada vez que se usan bombas contra civiles inocentes se trata de un acto de terrorismo", enfatizó Obama.
Al atentado en Boston se sumó la revelación de que un sobre con veneno de ricino enviado a un senador fue interceptado en la oficina de correos del Congreso.
Es previsible que el tema de la lucha contra el terrorismo se vuelva a agitar.
Mientras en la última década cientos de estadounidenses han muerto en campos de batalla en el extranjero, Estados Unidos se ha librado de muertes masivas por terrorismo en su territorio.
En privado, líderes políticos han admitido que se ha tenido suerte desde el 2001, aunque hubo incidentes como aquel en el que se acusó a un psiquiatra del ejército de Estados Unidos de asesinar a 13 personas en Fort Hood en 2009.
Estados Unidos despertó el martes con imágenes de carnicería y la inquietante realidad de que el terrorismo puede tocar en casa, un día después de un atentado que dejó tres muertos y más de 180 heridos en el maratón de Boston, poniendo fin a una racha de once años de suerte y tranquilidad, tras los ataques del 11 de setiembre.Es imposible detener totalmente los ataques y hacerlo requiere de un poco de fortuna como en 2010 cuando en Nueva York se frustró la explosión de un coche bomba.
Obama ha reivindicado sus logros contra el terrorismo para ganar las elecciones en 2012: la muerte de Osama bin Laden y la terrible ofensiva contra Al Qaida, por lo que si éste ataque en Boston provino del extranjero, el mandatario podría enfrentar algún tipo de responsabilidad política.
El presidente, que irá a Boston el jueves para hablar en una ceremonia en memoria de las víctimas, ha aprendido en estos años en la Casa Blanca sobre la necesidad de aparecer y hacerse cargo durante las crisis.
El mandatario recibió una andanada de criticas en la navidad de 2009 por no haber aparecido durante tres días, cuando se frustró un intento de atentado en un avión de línea estadounidense.
Al principio, la sensación de incertidumbre se vio agravada por un vacío de información, llenado luego por la especulación.
"Evidentemente tenemos que reflexionar sobre la idea de que esto puede haber sido hecho por yihadistas", afirmó el legislador de Nueva York, Peter King, especialista de asuntos de seguridad interior.
"Pero también podría haber sido hecho por la supremacía blanca. Pueden haber sido opositores al gobierno", añadió a la cadena MSNBC.
"Cuando se produce un atentado como éste, es difícil no pensar que pueden estar implicados extremistas islamistas, pero no tengo pruebas", destacó por su parte la senadora republicana del Maine (noreste) Susan Collins.
Mientras aumentaban los policías desplegados en los metros de Nueva York y Washington, el debate político sobre cómo responder al terrorismo reaparecía.
"Es muy, muy difícil, detener algo como esto en una sociedad libre y abierta", concluyó el republicano Michael McCaul, jefe de la comisión de Seguridad Interna.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  