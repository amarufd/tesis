BELFAST, Reino Unido (AFP)
                    photo0
El presidente estadounidense, Barack Obama, llegó este lunes a Belfast, en Irlanda del Norte, donde participará en la cumbre del G8, constató un periodista de AFP.
El Air Force One aterrizó poco después de las 8H35 local (07H35 GMT) en Belfast, donde el presidente de Estados Unidos dará un discurso, antes de reunirse con su homólogo ruso, Vladimir Putin, para hablar sobre Siria.
Obama desembarcó junto a su esposa, Michelle, y sus dos hijas antes de tomar un helicóptero que los llevará al aeropuerto Belfast City, cerca del centro de la ciudad.
El mandatario estadounidense dará luego un discurso ante unas 2.000 personas sobre el proceso de paz en esta provincia británica. Será recibido por el primer ministro de Irlanda del Norte, Peter Robinson, y el viceprimer ministro Martin McGuinness.
Posteriormente, el presidente estadounidense irá a Lough Erne, un hotel de lujo donde tendrá lugar la cumbre del G8.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  