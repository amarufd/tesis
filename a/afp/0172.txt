BOSTON, Estados Unidos (AFP)
                    Las dos explosiones que dejaron al menos tres muertos y más de un centenar de heridos en la maratón de Boston eran tratadas este martes como un "potencial atentado terrorista".
Con un lapso de 13 segundos entre una y otra, las explosiones hicieron volar a personas por los aires en la línea de llegada de la maratón.
La seguridad fue elevada en las principales ciudades de Estados Unidos, en medio de temores de una repetición de los atentados del 11 de septiembre de 2001.
El presidente estadounidense, Barack Obama, dijo que "encontraremos a quien hizo esto" y que "cualquier responsable individual, cualquier grupo responsable, sentirá como cae el peso de la justicia".
Obama advirtió en un breve discurso televisado contra "conclusiones apresuradas", pero un alto funcionario de la Casa Blanca dijo, bajo condición de anonimato, que "cualquier suceso con varios explosivos -como parece ser el caso- es claramente un acto terrorista".
El agente especial Rick DesLauriers, que encabeza la oficina de Boston del Buró Federal de Investigaciones (FBI), dijo a los periodistas: "Es una investigación criminal, lo que es una potencial investigación de terrorismo".
Las explosiones dejaron más de 100 heridos, dijo el gobernador de Boston, Deval Patrick, sin dar una cifra exacta. El Boston Globe indicó que hubo más de 140 heridos y que uno de los fallecidos era un niño de 8 años. Los médicos trabajaban durante la noche para atender a los heridos en las explosiones, que tuvieron lugar a unos 100 metros de distancia una de otra.
Varias víctimas sufrieron "amputaciones traumáticas" en la carpa del maratón transformada en improvisada emergencia y en los hospitales, dijo Alasdair Conn, jefe de emergencia en el Massachusetts General Hospital, que atendió heridos junto a otros cinco hospitales.
Tres personas han muerto y más de 100 han resultado heridas, varias de ellas en estado crítico, tras la explosión de dos bombas en la meta de la maratón de Boston, donde había más de 27.000 participantes.Más de 27.000 corredores participaban en la carrera de 42 kilómetros, uno de los maratones más prestigiosos del mundo. Decenas de miles de personas estaban agolpadas en la línea de llegada. Muchos de los corredores ya habían llegado a la meta cuando tuvieron lugar las explosiones.
Las deflagraciones provocaron la rotura de cristales de tiendas y afectó los indicadores bursátiles en el cierre de la jornada del lunes.
El fondista peruano Oliver Landeo, que participaba en la carrera junto con su compañera Jessica Márquez, quienes resultaron ilesos, relató a la limeña radio RPP que "ha sido muy terrible, porque había gente literalmente volando por los aires, hemos estado muy cerca al lugar de la explosión, aproximadamente a unos seis metros".
Otros testigos relataron como los cuerpos se habían apilado unos sobre otros por la onda expansiva. "Hemos visto a gente a la que les volaron las piernas", relató a AFP Mark Hagopian, propietario del Hotel Mark, situado cerca de la línea de meta del maratón. "Una de ellas no tenía más piernas a la altura de la rodilla, pero vivía", añadió.
NBC News, citando funcionarios, informó que la policía había hallado "múltiples dispositivos explosivos" en Boston, abriendo la posibilidad de un ataque coordinado.
Las autoridades de la ciudad urgieron a la población a no congregarse en multitudes y vallaron la zona de las explosiones.
La policía advirtió que habría un aumento de la seguridad en la ciudad este martes con chequeos aleatorios de mochilas y bolsos en autobuses así como en trenes. Muchas calles también permanecerán cerradas.
El gobernador Patrick dijo a última hora del lunes que "la ciudad de Boston estará abierta (el martes), pero no será como todos los días".
Estas explosiones tienen lugar más de una década después de los atentados contra las torres gemelas de Nueva Yoirk, Washington y Pensilvania, que dejaron casi 3.000 el 11 de septiembre de 2001.
Los niveles de seguridad fueron elevados en varias ciudades de Estados Unidos poco después de las explosiones. Además de Boston, Nueva York y Washington, blanco de los ataques del 11-S, así como Los Ángeles incrementaron su presencia policial.
La bandera nacional ondeaba a media asta sobre el domo del Capitolio en Washington, en honor a las víctimas de las explosiones de Boston.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  