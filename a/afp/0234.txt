WATERTOWN, EEUU (AFP)
                    Las autoridades estadounidenses detuvieron el viernes por la noche al joven de 19 años de origen checheno sospechoso del atentado en el maratón de Boston junto con su hermano, abatido en la madrugada, indicó la policía de esa ciudad de Massachusetts.
El sospechoso identificado como Dzhojar Tsarnaev, de 19 años, había sido rodeado en una casa en el suburbio de Watertown, en el oeste de Boston, escenario durante toda la jornada de un rastrillaje casa por casa de parte de más de 9.000 policías.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  