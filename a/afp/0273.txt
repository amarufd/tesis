PARÍS (AFP)
                    Tres constructores aeronáuticos, Dassault Aviation, EADS y Finmeccanica, pidieron este domingo a los Gobiernos europeos que pongan en marcha un programa de 'drones' (aviones no tripulados) de vigilancia para competir con los fabricantes israelíes y estadounidenses.
El francés Dassault, el grupo europeo EADS y el italiano Finmeccanica indicaron en un comunicado común estar dispuestos a trabajar en la creación un 'dron' de tipo MALE ('Media altitud, larga resistencia') que permite vigilar durante 24 horas amplias zonas de terreno.
Los aviones no tripulados, que hasta ahora fabrican Israel y Estados Unidos, se han convertido en un elemento indispensable de la guerra moderna en países como Afganistán o Malí.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  