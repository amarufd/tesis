DUBÁI (AFP)
                    Los rebeldes sirios advirtieron este jueves a los países "amigos de Siria" que si no les entregan misiles antiaéreos y antitanques para proteger a las zonas civiles tendrá lugar una "catástrofe humanitaria".
En una declaración a AFP, el portavoz del Ejército Sirio Libre, Luay Mokdad, instó a los países del grupo de 'amigos de Siria', que se reunirán el sábado en Doha, a imponer una zona de exclusión aérea sobre las zonas que controlan los rebeldes.
"Necesitamos misiles tierra-aire de corto alcance MANPAD, misiles antitanques, morteros, municiones", entre otras cosas, dijo Mokdad. "También necesitamos material de comunicación, chalecos antibalas, máscaras de gas", agregó.
Se corre el riesgo de una "verdadera catástrofe humanitaria (...) si no nos dan esas armas para proteger a las zonas civiles", puntualizó, acusando a las fuerzas leales al presidente sirio, Bashar al Asad, de llevar a cabo masacres en las regiones que han reconquistado.
Los ministros de Exteriores de once países del grupo de 'amigos de Siria' se reunirán el sábado en Doha para discutir sobre la ayuda militar que debe aportarse a la rebelión, informó el miércoles una fuente diplomática francesa. También se discutirá sobre la posibilidad de organizar la conferencia de paz llamada 'Ginebra 2'.
Los once países que participarán en la reunión de Doha son Francia, Reino Unido, Estados Unidos, Alemania, Italia, Jordania, Arabia Saudita, Qatar, Emiratos Árabes Unidos, Turquía y Egipto.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  