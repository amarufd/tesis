PEKÏN (AFP)
                    El terremoto de magnitud 6,6 que sacudió este sábado el suroeste de China dejó centenares de muertos o heridos, anunciaron las autoridades locales.
"El sismo en Ya'an, distrito de Lushan, causó cientos de muertos o heridos", indicó la agencia sismológica de la provincia de Sichuan, en una página gubernamental de internet.
El temblor se produjo a 122,3 km de profundidad, precisó el Instituto Geofísico de Estados Unidos (USGS).
Nueva China anunció poco antes que el USGS un sismo de magnitud 7 que se sintió a las 08H00 locales (00H00 GMT).
Los terremotos son relativamente frecuentes en el suroeste de China.
En 2008, un sismo en Sichuan dejó 87.000 muertos o desaparecidos.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  