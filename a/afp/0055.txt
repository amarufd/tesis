WASHINGTON (AFP)
                    Autoridades federales de Estados Unidos detectaron veneno de ricino en una carta enviada al senador republicano Roger Wicker, indicaron fuentes oficiales.
"La instalación que maneja el correo del Senado (...) recibió correo que dio positivo para ricino", dijo en un comunicado el sargento en armas del Senado, Terrance Gainer.
El veneno fue detectado durante una inspección de rutina del correo en una dependencia exterior al edificio del Congreso, por lo que la misiva nunca llegó a oficina de Wicker en el Capitolio, señaló un colaborador del líder de la mayoría demócrata, Harry Reid, quien informó sobre la brecha de seguridad.
Roger Wicker es un republicano electo por el estado de Mississipi, considerado un político de bajo perfil que no está especialmente implicado en los debates sobre inmigración y porte de armas.
Gainer señaló en el comunicado que "la policía del Capitolio, el FBI y otras agencias están involucradas en la investigación de este correo".
El director del FBI, Robert Mueller y la secretaria de Seguridad Interior, Janet Reno, informaron a los senadores sobre el incidente durante una reunión a puertas cerradas en la noche del martes que tenía el objeto de rendir un informe sobre los atentados de Boston, indicó el colaborador de Reid.
Autoridades federales de Estados Unidos detectaron veneno de ricino en una carta enviada al senador republicano Roger Wicker, indicaron fuentes oficiales.No está claro si hay conexión entre las explosiones de Boston y el correo con ricino.
Gainer dijo que "mientras no tenemos indicios de la existencia de más correo sospechoso, es imperativo aplicar todos los protocolos para el manejo de la correspondencia".
Según la senadora Claire McCaskill, citada por el sitio internet "Politico", las autoridades tienen identificado a un sospechoso.
En febrero del 2004 el Senado y la Casa Blanca habían sido objeto de un ataque con ricino, agente biológico enviado en forma de polvo y que en aquellas ocasiones no causó víctimas.
Pero en otoño de 2001 otros ataques con antrax causaron cinco muertos. Su autoría nunca fue dilucidada.
Desde entonces todo el correo enviado a los cargos electos de la nación es examinado fuera del Capitolio antes de ser expedido.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  