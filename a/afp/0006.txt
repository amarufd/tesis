DEHRADUN, India (AFP)
                    Al menos 120 personas murieron y otras miles siguen aisladas en el norte de India desde finales de la semana pasada por las precoces lluvias del monzón, informaron las autoridades, que enviaron helicópteros militares para distribuir comida.
Desde la llegada de las lluvias del monzón en el norte del país, dos semanas antes de la fecha prevista, los Estados de Uttarakhand y Himachal Pradesh han recibido un volumen de precipitaciones tres veces superior a la media en esta época del año. "Al menos 110 personas murieron. El Gobierno del Estado y el ejército intentan auxiliar a miles de turistas que están aislados cerca de las zonas inundadas y de los lugares santos hindúes", dijo Jaspal Arya, ministro de Salud del Estado de Uttarakhand.
Las autoridades anularon las peregrinaciones en este Estado, donde hay numerosos templos y lugares de culto que atraen a miles de turistas. El martes, el templo de Kedarnath quedó cubierto de fango y cerca de 10.000 peregrinos están aislados cerca del lugar, dijo el ministro a AFP. Las lluvias del monzón empezaron el sábado y desde entonces las autoridades han instalado 40 campos para acoger a los afectados. Al menos 21 puentes se han hundido en Uttarakhand, según las autoridades.
En el estado vecino de Himachal Pradesh, las inundaciones destruyeron más de 500 casas y al menos 10 personas murieron en desprendimientos de terreno. La llegada del monzón con dos semanas de antelación sorprendió a las autoridades y puso de nuevo de manifiesto los problemas de los programas de prevención y socorro.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  