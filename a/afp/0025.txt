MOSUL, Irak (AFP)
                    El jefe de un partido político aliado del primer ministro iraquí y cuatro miembros de su familia murieron este miércoles en un atentado suicida al norte de Irak, afirmaron responsables de los servicios de seguridad y de salud, en la víspera de elecciones provinciales.
Ataques con bombas mataron además a ocho jóvenes cerca de una cancha de fútbol en un pueblo al noreste de Bagdad. Yunus al Ramah, jefe del Partido de Irak Unido, acababa de tener una reunión en su domicilio de la ciudad de Al Hadhr, en la provincia de Nínive, este miércoles por la mañana cuando se produjo el atentado, informaron las fuentes.
"El kamikaze apuntó a las personas que (todavía) estaban reunidas en el jardín de Ramah", afirmó el teniente de policía Islam al Juburi. Cinco personas murieron -el líder político y cuatro familiares- y otras seis resultaron heridas, afirmaron Juburi y un médico.
El Partido de Irak Unido es considerado un aliado del primer ministro, el chiita Nuri al Maliki. Ramah no se presentaba a los comicios pero algunos miembros de su partido son candidatos a las elecciones provinciales del jueves en Al Anbar y Nínive, dos provincias de mayoría sunita. Ningún grupo ha reivindicado este ataque, pero activistas sunitas vinculados a Al Qaida habían intentado intimidar a los candidatos a los comicios.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  