BAGDAD (AFP)
                    Al menos 20 personas murieron este domingo en varias localidades al sur de Bagdad en atentados con coche bomba, informaron fuentes médicas y de seguridad.
Las explosiones tuvieron lugar en Kut y Aziziyah, en la provincia de Wasit, así como en Mahmudiy, Nasiriya y Basora, en el sur del país. Al menos 56 personas resultaron heridas.
En Kut, capital de la provincia de Wasit, 160 kilómetros al sur de Bagdad, un coche bomba mató a siete personas e hirió a 15 al estallar frente a un restaurante en una zona de talleres de reparación de vehículos. Otro coche bomba mató a cinco personas e hirió a 10 en el mercado central de Aziziyah y cerca de una mezquita chiita. En Basora, sur de Irak, murieron cinco personas en dos explosiones. Entre las víctimas figura un experto en explosivos que trataba de desactivar una bomba en un vehículo. Otras tres personas murieron en atentados con bomba en Nasiriya y Mahmudiya.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  