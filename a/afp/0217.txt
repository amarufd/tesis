WELLINGTON (AFP)
                    Nueva Zelanda se convirtió este miércoles en el decimotercer país del mundo y el primero de la región de Asia Pacífico en legalizar el matrimonio homosexual.
La nueva ley que modifica la legislación que regía el matrimonio en este país desde 1955 fue aprobada por la Cámara de Diputados este miércoles, un poco más de un cuarto de siglo después de la despenalización de la homosexualidad en 1986.
Nueva Zelanda autorizaba ya las uniones civiles desde 2005.
Esta reforma de ley defendida por el primer ministro neocelandés, el centro-derechista John Key, fue presentada por Louisa Wall, diputada homosexual del Partido Laborista, principal partido de oposición.
"La ley (precedente) consideraba a los neozelandeses homosexuales como seres inferiores a los seres humanos, y a los demás ciudadanos. Este texto permite asegurarse que el Estado no discrimine a ningún categoría de la población", en función de su orientación sexual, dijo la diputada a AFP.
El texto tuvo que hacer frente a una fuerte oposición, principalmente del grupo Family First, que acusa a los dirigentes políticos de debilitar la institución tradicional del matrimonio bajo la presión de los activistas a favor del matrimonio entre homosexuales.
En el mundo, trece países autorizan el matrimonio entre personas del mismo sexo, según la ONG Human Right Watch (HRW).
Dinamarca fue el primer país del mundo que autorizó las uniones civiles entre personas del mismo sexo en 1989.
Australia, país vecino de Nueva Zelanda, rechazó el matrimonio entre homosexuales el pasado septiembre.
Uruguay se convirtió a principios de mes en el segundo país latinoamericano, después de Argentina, en legalizar el matrimonio homosexual, después de que la Cámara de Diputados diera sanción definitiva a un proyecto de ley de "matrimonio igualitario".
Actualmente, los diputados franceses debaten un proyecto de ley que legalizará el matrimonio entre personas del mismo sexo, así como la adopción, en un contexto de fuerte oposición por una parte de la población.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  