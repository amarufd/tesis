ENNISKILLEN, Reino Unido (AFP)
                    Los dirigentes de las ocho mayores potencias industrializadas (G8) comenzaron oficialmente este lunes por la tarde su gran cumbre anual en Lough Erne, un lujoso complejo hotelero de Irlanda del Norte.
Esta conferencia, que reúne a EEUU, Alemania, Canadá, Francia, Italia, Japón, Reino Unido y Rusia, estará mayoritariamente dedicada al tema de Siria, sobre el cual los ocho países han manifestado ya sus divergencias.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  