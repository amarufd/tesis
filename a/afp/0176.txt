YAUNDE, Camerún (AFP)
                    El presidente camerunés, Paul Biya, anunció este viernes la liberación de los siete rehenes franceses de una misma familia, entre ellos cuatro niños, que fueron secuestrados hace dos meses en el norte de Camerún por el grupo yihadista nigeriano Boko Haram y retenidos en Nigeria.
"El presidente de la República de Camerún, Paul Biya, anuncia a la opinión pública nacional e internacional que los siete rehenes franceses secuestrados el 19 de febrero de 2013 en Dabanga (norte) fueron entregados esta noche a las autoridades camerunesas", según un comunicado leído en la radio nacional.
                200 oficinas en 150 países    Ampliar

¿Desea compartir una información, un comentario?  Escríbanos a…      
   
  