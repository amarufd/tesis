
Argentina, Salta: "Tranquerazo" contra la fábrica de explosivos



Alrededor de 700 vecinos de El Galpón, El Tunal, J. V. González, Metán, El Quebrachal (localidades de los departamentos de Anta y Metán en la provincia de Salta) acompañados por representantes de organizaciones de ocho provincias participaron el sábado 15 de junio del denominado “Tranquerazo” para oponerse al proyecto de instalación de una planta de nitrato de amonio.

A un año del inicio de esta lucha, los Vecinos Autoconvocados de la Cuenca del Río Juramento programaron esta actividad que estuvo fuertemente apoyada por la CTA y ATE, movimientos sociales, organizaciones ambientalistas y algunos partidos politicos.

Los manifestantes locales y de las provincias de Catamarca, La Rioja, Jujuy, Córdoba, Buenos Aires, Tucumán, Entre Ríos y de distintas localidades de Salta se concentraron en la plaza principal de El Galpón donde en horas de la mañana se desarrolló un breve acto.

Luego una caravana de vehículos con carteles y pancartas y una ruidosa batucada recorrió las calles de la ciudad; a su paso los vecinos saludaban y aplaudían con entusiasmo.

Posteriormente la caravana se dirigió hacia el kilómetro 653 de la ruta nacional 16, cerca del pueblo de El Tunal. Allí en el paraje El Lapacho la empresa Austin está realizando los trabajos previos para la instalación de la planta.

En ese lugar, desde hace 32 días, vecinos acampan y bloquean el acceso a donde se pretende construir el emprendimiento. Allí se compartió un locro comunitario y posteriormente se desarrolló una asamblea que contó con numerosos oradores, quienes resaltaron el efecto altamente contaminante y las consecuencias devastadoras para la salud de las poblaciones cercanas pero también las más alejadas que forman parte de la cuenca del Juramento.

Normando "Piojo" Ocampo, de la CTA y ATE La Rioja, relató la experiencia en Famatina que ya lleva 8 años de resistencia a la explotación megaminera de ese cerro.

Fernando "Nando" Acosta, Secretario del Interior de CTA nacional, comprometió su apoyo para que el conflicto se nacionalice.

Vuenaventura David, Secretario General de la CTA Salta propuso profundizar la resistencia y programar un acto en Salta Capital y un escrache en la casa de Salta en Buenos Aires.

Los antecedentes

La empresa de capitales mayoritarios yanquis Austin Powder SA, que lidera el mercado argentino de explosivos, esta por instalar una planta productora de Nitrato de amonio, elemento utilizado en la fabricación de explosivos. Explosivos que, a su vez, serán utilizados por las empresas megamineras que están intentando destruir nuestra cordillera.

Según los profesionales que han analizado el caso la empresa cuenta con la autorización ministerial tras un informe de impacto ambiental muy cuestionable. Se estima que esta fábrica va a contaminar la cuenca del río Juramento, va a sacar una cantidad de desechos tóxicos a un piletón de agua de 4 hectáreas que contaminara las napas y el aire.

Otro riesgo que implica la instalación de esta fábrica está vinculado a los accidentes explosivos que se pueden producir, no sólo en el propio lugar de fabricación, sino a lo largo de las rutas por donde van a circular se estima que 12 o 13 camiones diariamente cargados de explosivos. Recientemente en Texas una explosión en una fábrica de “fertilizantes” a base de nitrato de amonio mato a 30 personas dejando 200 heridos.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

