
Contrato de lectura



A largo plazo siempre ganará la verdad. Tal vez el periodista pueda pensar en que su trabajo ha fracasado al suministrar material para la historia. Pero la historia no fracasara mientras él esté con la verdad. La Ética es la moral de la conciencia y lo hará intentar entender las motivaciones de todos los implicados en una situación.

La idea del “periodismo independiente” parecería ser una bandera de libertad en el oficio de escribir o presentar los hechos en sociedad y se entroniza la figura del periodista como la del defensor de un noble interés ciudadano por sobre otras determinaciones.

En general, las fuentes presionan la tarea del periodista en un juego perverso. Quienes suministran información suelen ser fuentes interesadas de las que los periodistas dependemos para obtener datos y a su vez corroborarlos, en esa rutina diaria que en el circuito que puede finalizar con una primicia.

Además de las fuentes existen otros factores que determinan la independencia periodística: la empresa, y los anunciantes. La empresa pone en la calle o en el aire un estilo de producto periodístico que sale a competir con otros proyectos. Frente a esta realidad, el periodista produce su material, que también debe complementarse con la planificación que realiza la “empresa periodística”. La tendencia se disputa, muchas veces, en la mesa de redacción, sobre el alcance político, económico (los anunciantes) y social.

Cada empresa –gráfica, tv o digital- crea un modo de acercamiento hacia su público que reúne una visión del mundo que comparte con sus lectores. Es allí donde el periodista debe ensamblar su trabajo con el “contrato de lectura” establecido entre el medio y su público.

La sociedad se desdibuja en el recuerdo y el mañana se vislumbra desde lo noticioso. El presente se relata como una manera de afianzar lo que se es y la historia se vuelve una puesta en escena ante la lente de una cámara, unas líneas bien redactadas frente a un micrófono, o la nota complaciente.

Existen “periodistas” sin sentido del contenido social del oficio, y del quehacer profesional. El buen periodismo es aquel que es útil a los intereses de quien juzga o califica. Hoy los grandes medios son cada vez más funcionales a las corporaciones que a defender los intereses de la comunidad.

La verdad ha dejado de tener algo que ver con la honestidad y se acomoda a la necesidad para aparentar lo que no se piensa y decir lo que el poder quiere oír, para contribuir al engaño masivo. Entonces el periodista se convierte en corporativo o subversivo, aliado o adversario, tiñendo con aspectos ideológicos la información y callando las injusticias en beneficio de su bolsillo.

Las “empresas periodísticas” están matando el capital social del periodismo como institución. Es función del periodista replantearse una noción de que es la verdad, la honestidad intelectual y reconociendo los intereses reales dentro de los cuales la práctica periodística debe ser ejercida.

Ética es la moral de la conciencia. La conciencia es la verdad. Sabe que está bien y que está mal. Hoy el lenguaje es panfletario. Creíble para los adeptos y odioso para los antónimos. La demonización del otro cierra filas en torno a las ideas, dejando de lado la reflexión. La verdad para algunos, es subversiva, mientras que la mentira, para otros actúa como tranquilizante.

El periodismo es la primera versión de la historia y los vencedores son quienes cuentan la historia y si la historia no existe, hay que inventarla o moldearla según los intereses de quien gana. Hoy buena parte de contar la historia la asumen los medios a través de sus periodistas.

Los intereses de las corporaciones, los condicionamientos laborales y en muchos casos la falta de oficio, están ahogando el pensamiento de la práctica periodística, que está perdiendo su esencia y su credibilidad.-

Ernesto Martinchuk es periodista.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

