
Más evidencias sobre soberanía insular de Vietnam



Mapas, documentos, objetos y publicaciones, que evidencian la soberanía de Vietnam sobre los archipiélagos Hoang Sa (Paracels) y Truong Sa (Spratlys), en el Mar Oriental, se exhiben en la provincia central de Ha Tinh.

Entre las 200 evidencias presentadas, se destacan documentos publicados en chino, vietnamita y francés del siglo XVII a principios del XX, mapas antiguas del siglo XVI y tres Atlas de Reino Unido, Alemania y Estados Unidos elaborados de 1626 a 1980.

Todos estos documentos muestran que Hoang Sa y Truong Sa pertenecen a Vietnam.
 
La exposición permanecerá abierta hasta el próximo 8 de junio.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

