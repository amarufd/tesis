
Menos consumo, italianos pierden peso



A raíz de la profunda y prolongada crisis económica del país, los italianos están consumiendo menos alimentos y en consecuencia también están bajando de peso.

Este es uno de los resultados de un estudio llevado a cabo en la Península sobre el peso promedio de hombres y mujeres en relación al mismo dato de 1993.

Hace veinte años el peso promedio de un hombre era de 76,9 kilogramos, mientras que el de una mujer era de 60,3 kilogramos, según destacó el estudio difundido por la asociación de consumidores Codacons. El reporte precisó que hoy en día en el caso del hombre el peso es de 74,1 kg. y el de la mujer de 59,1 kg.

En otras palabras, el peso promedio de un hombre es hoy 2,8 kg inferior al del 1993 y lo mismo ocurre con el caso de las mujeres (-1,2 kg.). "No hay ninguna duda que los problemas económicos de estos tiempos están golpeando a las familias italianas, a raíz de los cambios en las costumbres alimentarias", destacó el presidente de Codacons, Carlo Rienzi.

"Entre el 2007 y el 2012 -precisó- los consumos alimentarios en nuestro país han bajado de casi el 10, con una contracción en el gasto destinado a este rubro de unos 12.000 millones de euros".

"Los italianos -concluyó el experto- están comiendo menos no porque quieren mantener su forma física sino simplemente porque tienen menos dinero, hecho que está a su vez afectando a su peso".
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

