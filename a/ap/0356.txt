
Caso Mariano Ferreyra: difundieron los fundamentos de la sentencia que condenó a José Pedraza a 15 años de prisión



El Tribunal Oral en lo Criminal N° 21 de la Capital dio a conocer este lunes los fundamentos de la sentencia que, el 19 de abril pasado, condenó a 15 años de prisión a José Pedraza, en el marco del juicio oral por el homicidio del joven militante del Partido Obrero Mariano Ferreyra, ocurrido el 20 de octubre de 2010.

El tribunal, integrado por los jueces Horacio Días, Diego Barroetaveña y Carlos Bossi, impuso la misma pena para Juan Carlos Fernández, por ser, junto a Pedraza, penalmente responsables del delito de homicidio en concurso ideal con homicidio en grado de tentativa en calidad de partícipes necesarios.

También fueron condenados Pablo Marcelo Díaz, Cristian Daniel Favale y Gabriel Fernando Sánchez a 18 años de prisión; Jorge Daniel González y Salvador Pipitó a 11 años de prisión, y Claudio Alcorcel a 8 años de prisión.

Asimismo, fueron condenados a 10 años de prisión Luis Mansilla; a 9 años de prisión a Jorge Ferreyra; a 2 años de prisión de ejecución condicional a Hugo Lompizano, Luis Echavarría y Gastón Conti, y al pago de una multa a David Villalba.

En tanto, fueron absueltos Guillermo Uño, Juan Carlos Pérez y Rolando Garay.

Acceder desde aquí a Fallo completo I (52278.93 Kb., formato pdf)

Acceder desde aquí a Fallo completo II (156.32 Kb., formato pdf)
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

