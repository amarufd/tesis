
Corea: Metas inviables



Desde el borde de la guerra y más allá de la sensatez, Corea del Norte parece regresar a las posiciones iniciales y propone retomar el diálogo, condicionándolo a: (1) Levantamiento de las sanciones del Consejo de Seguridad (2) Retirada de las armas nucleares de la región (3) Fin de los ejercicios militares de Estados Unidos y Corea del Sur. La propuesta se realiza en el tono recién estrenado: "Si Estados Unidos y los títeres de Corea del Sur desean evitar el golpe de mazo de nuestro Ejército y pueblo deben decidir…”

Aunque alguna de las demandas pueden resultar inviables, el hecho de que los mismos líderes que llegaron a declarar un virtual “estado de guerra” y solicitar a los extranjeros evacuar Pyongyang, acepten la posibilidad de dialogar es una señal en la dirección de la distensión.

Es evidente que Estados Unidos no retirará ahora ni en plazos predecibles sus armas nucleares desplegadas en la región ni en ninguna otra parte porque otro país se lo demande. Entre otras cosas tales armas son la base de su doctrina de militar global. La exigencia es especialmente contraria a la estrategia recién anunciada basada en el incremento de la presencia y del potencial militar en la región Asia-Pacifico.

La única hipótesis viable de reducción de las armas atómicas es la adopción de acuerdos bilaterales entre Estados Unidos y Rusia, proceso a los que en algún momento, pudieran sumarse China y otros países nucleares. Las negociaciones sobre limitación de armas estratégicas (SALT I y II), iniciadas en Helsinki en 1969, en 44 años han producido como únicos y magros resultados los tratados START y ABM. Ni siquiera la desaparición de la Unión Soviética cambió esos hechos.

En toda la era nuclear la única exigencia de obligar a retirar armas nucleares bajo amenaza fue intentada por la administración Kennedy durante la Crisis de los Misiles de 1962, que finalmente se cubrió con una negociación de intercambio de los cohetes instalados en Cuba por la Unión Soviética por los Júpiter emplazados por Estados Unidos en Turquía. Una exigencia como la de Corea del Norte jamás ha funcionado con ningún país; incluso no ha surtido efecto ni en ella misma.

Lo novedoso en el nuevo enfoque norcoreano es que aparentemente se retira de la intención de negociar su programa nuclear, en primer lugar el de naturaleza militar, intercambiándolo por compromisos políticos asociados a la reunificación y por algunas formas de asistencia económica que hasta ahora había sostenido y que fue la base de los acuerdos durante la administración del presidente Clinton y son el núcleo de las negociaciones a Seis Bandas cuya esencia es la desnuclearización de Corea del Norte, no la de Estados Unidos.

Seguramente en los próximos días habrá nuevos esclarecimiento pero por ahora los dedos parecen alejarse de los gatillos atómicos. Ojalá la tendencia se consolide. Allá nos vemos.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

