
Antiveneno argentino contra la araña de los bananeros



El antídoto se produce en Brasil, pero un avance de investigadores de la UBA y el Malbrán sienta las bases para su desarrollo local.

En la película “Mercado de Abasto”, de 1955, una “araña de los bananeros” pica al hijo del personaje representado por Tita Merello. Esta ficción ocurre ocasionalmente en la realidad: la picadura produce un intenso dolor y puede provocar la muerte en dos a doce horas, especialmente en niños. Ahora, un equipo de científicos argentinos desarrolló un antiveneno que, en estudios de laboratorio con ratones y cobayos, logró neutralizar en un cien por ciento dosis de venenos provenientes de arañas de ese género (Phoneutria).

Si bien ya existe un producto antiveneno específico, fabricado en Brasil, la producción local permitiría “no depender de la adquisición de este tipo de productos del extranjero, lo que en ocasiones es imposible pues está supeditada al excedente de stock del productor”, indicó a la Agencia CyTA el doctor Adolfo Rafael de Roodt, del Laboratorio de Toxinopatología del Centro de Patología Experimental y Aplicada de la Facultad de Medicina de la UBA.

Según el especialista, cuyo equipo publicó el trabajo en la revista “Archivos Argentinos de Pediatría”, escalar la producción del antídoto no debería llevar más de un año hasta la obtención del “frasquito” disponible en la heladera de un hospital.

Las arañas del género Phoneutria incluyen diferentes especies. “Phoneutria nigriventer se encuentra en Misiones, Chaco, Formosa, Salta y Jujuy; mientras que Phoneutria fera, es una especie de Brasil que llega a encontrarse en la Argentina, principalmente en puertos y mercados de frutas relacionada a cargamentos”, indicó De Roodt, quien también trabaja en el área Investigación y Desarrollo INPB-ANLIS “Dr. Carlos G. Malbrán” del Ministerio de Salud de la Nación.

De día, estos arácnidos permanecen ocultos en cortezas de árboles, bajo troncos, en bananeros, palmeras o bromelias. “Pueden estar en el interior de viviendas, en lugares oscuros y húmedos, dentro de roperos o calzado, atrás de muebles y de cortinas, entre otros sitios. Al sentirse amenazadas son capaces de atacar saltando hacia adelante más de 20 centímetros”, puntualizó el investigador.

La inoculación de veneno produce dolor inmediato y muy intenso que se irradia a partir del sitio de la picadura. Puede haber edema, calambres dolorosos, temblores, convulsiones tónicas, sudoración, taquicardia, arritmias, disnea y disturbios visuales.

Aunque no hay registros precisos en Argentina, se calcula que, en Misiones, apenas el diez por ciento de los casos requieren de antiveneno.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

