
Cluster Shale, la frontera extractivista



El empresariado extractivo junto a funcionarios de la Provincia del Neuquén, con apoyo del gobierno nacional, llevaron adelante el encuentro "Cluster Shale" para fomentar la actividad del fracking en el país, mientras muchos representantes de organizaciones quedaron afuera por "derecho de admisión".

Se realizó en la capital neuquina la segunda edición del encuentro "Cluster Shale", el cual reúne a todos los eslabones empresariales y políticos de la industria del petróleo y del gas.

Cartago TV estuvo cubriendo el evento desde sus puertas, junto a distintas organizaciones que representan a un amplio sector social afectado por el neoextractivismo. Quienes cuestionan el avance de esta nueva forma de explotación energética, por sus consecuencias climáticas y ambientales, así como económicas y sociales, fueron impedidos de participar aun pagando su entrada (la cual costaba $1200) por "derecho de admisión".

Parte I

En declaraciones para Cartago, Jorge Nahuel, Lonko del Lof Newen Mapu, también miembro de la zonal Xawvnko de la Confederación Mapuche de Neuquén, expresó: "Todo este tipo de propaganda oficial que están organizando ahí adentro, apunta a eso a aislar a los que estamos luchando en este tema, apunta a crear toda una imaginación de riqueza que se va a derramar cuando sabemos que en estos 50 años de gobierno provincial los únicos que se han enriquecido son los sectores ligados al poder. Para el pueblo se ha repartido la miseria, la exclusión".



Parte II

Por su parte, la trabjadora de Áreas Provinciales Protegida y mienbro de ATE, Andrea Mazieres, amplió sobre la aprobación de la explotación no convencional en la zona intangible de Auca Mahuida.

"En Auca Mahuida, un área provincial protegida, se está llevando adelante unos de los primeros pozos exploratorios de gas no convencional, en la cual, por decretos y legislaciones, no se podrían realizar"

"La empresa Total es francesa, en su país está prohibida la explotación con francking y viene a Neuquén a explotar en un área protegida"

"Somos los primeros del mundo en donde se explota en un área natural protegida"



Parte III

El Lonko del Lof Kaxipayiñ, Gabriel Cherqui, y Peti Pichiñan, werken del Lof Puel Pvjv, declaran sobre la dura realidad de Loma de la Lata, que a escasos años del comienzo de la mega explotación ya deja sentir sus consecuencias.

Peti Pichiñan werken del Lof Puel Pvjv: "Ya con los convencionales han generado un montón de contaminación y de muerte, con los no convencionales esto se profundiza. (...) El primer pozo no convencional fue en una comunidad mapuche y ya tenemos la muerte de la lamien Cristina Lincopan".

Gabriel Cherqui, Lonko del Lof Kaxipayiñ: "El yacimiento está dentro de nuestro territorio en Loma de la Lata, llevamos años de muerte".



Cartago TV se emite por Somos el Valle (Canal 8 de Cablevisión del Alto Valle), todos los sábados a partir de las 22, con repeticiones los días:

Domingo 0.30 y 6 hs.

Lunes 0.30 hs.

Martes 12 hs.

Jueves 18 hs.

En YouTube: http://www.youtube.com/user/2009Cartago

En Vimeo: http://vimeo.com/cartagotv

En Twitter: @cartagotv

Sitio oficial: www.principioesperanza.com.arHaga click aquí para recibir gratis Argenpress en su correo electrónico.

