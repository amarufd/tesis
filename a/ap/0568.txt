
Paraguay: Elecciones antidemocraticas



De inmediato al cierre de las urnas, serán legalizadas por el Tribunal Superior de Justicia Electoral, y legitimadas por la Organización de Estados Americanos (OEA), así como por algunos otros organismos y ciertos gobiernos extranjeros, pero nadie, honestamente, podrá negar la ausencia de democracia que es la característica más sobresaliente de las elecciones generales, convocadas para este domingo 21 en Paraguay.

Su realización, si bien puede ser interpretada como una oportunidad para reordenar la vida institucional del país, fracturada hace 10 meses con el Golpe de Estado, llamado falsamente parlamentario, el resultado electoral constituirá un desafío insoslayable para la democracia en todo el mundo, porque su reconocimiento significará legalizar lo ilegítimo. ¿Qué harán, entonces, el MERCOSUR, UNASUR Y LA CELAC?.

El primer elemento que permite considerar antidemocráticas estas elecciones es el carácter espurio y faccioso del gobierno convocante, instalado hace 10 meses tras un Golpe de Estado, llamado parlamentario, que terminó con la administración de Fernando Lugo, elegido en las elecciones más libres en 200 años de historia del país, a cuatro años de su ejercicio y a sólo un año de finalizar su mandato.

La corrupción imperante en toda la campaña electoral a nivel de los dos principales partidos, el Colorado y el Liberal, cuyos dirigentes confiesan que han invertido muchos millones de dólares, es el segundo elemento antidemocrático en juego, al que se debe sumar un tercero: la censura aplicada por algunos órganos de prensa a las fuerzas progresistas o de oposición a ese bicéfalo y viejo engendro cupular colorado-liberal.

En concreto, Aníbal Carrillo Iramain, el presidenciable del Frente Guasu, encabezado por Lugo, e integrado por una docena de partidos y organizaciones, acusó al Canal 9 de la Televisión privada y a la propia Televisión Pública, de censurar a esa fuerza política que, según algunos sondeos, se situaría en tercera posición en las preferencias del electorado, con similitudes en las intenciones y parejas posibilidades con el recién formado Movimiento Avanza País, conducido por el comunicador Mario Ferreiro.

La discriminación de fuerzas opositoras, en particular las progresistas o con algún tinte de izquierda, también se ha expresado en la invitación a los postulantes en los programas de la pantalla chica y en dos actos públicos realizados en el Banco Central.

Un cuarto elemento probatorio del carácter antidemocrático de las elecciones de este domingo en Paraguay, es que se llegará a las urnas con una fuerte presión sobre el pueblo, buscando difundir miedo, particularmente en las poblaciones rurales, de lo que es una muestra grosera la docena de presos políticos que, desde hace varios meses, están repartidos en diferentes cárceles del país, sin condena por falta de pruebas.

Todos son activistas campesinos, cuyo “delito” es reclamar una reforma agraria que redistribuya la tierra, y termine con el abandono y expulsión de sus chacras de unas 300 mil familias, convertidas en parias, sin que el Ministerio Público haya podido, hasta ahora, presentar cargos que justifiquen tal encierro, ni tampoco la carátula de “prófugos” que la Fiscalía ha puesto contra unos 50 humildes labriegos, obligados a vivir escondidos como delincuentes.

Su calvario, uno más en la larga historia de sufrimiento y humillación del campesinado paraguayo, comenzó el 15 de junio del año pasado, cuando se produjo una masacre de campesinos y policías, que diversos elementos y fuentes consideran que fue parte del plan golpista que, una semana después, terminó con el Gobierno de Lugo.

Siete policías y unos 20 campesinos murieron, algunos rematados en el suelo y otros porque las fuerzas del orden los dejaron desangrarse sin prestarles atención médica.

Ese hecho fue utilizado por la dirigencia derechista, enquistada en los partidos y gremios empresariales, aplicando un diseño pergeñado por ciertos diplomáticos extranjeros y elementos de las corporaciones sojeras y de la megaminería, para acusar a Lugo de mal desempeño de sus funciones.

En una parodia de juicio político, que duró 17 horas, el parlamento destituyó al mandatario que había sido elegido democráticamente el 20 de abril del 2008, en presencia de miles de extranjeros que habían llegado al país con la esperanza de ver resurgir la democracia en este país miembro del MERCOSUR y la UNSAUR, de donde quedó suspendido a partir de cortando el proceso de cambios.

Insultos y acusaciones de corrupción es lo que más sobresale entre los candidatos con más posibilidades de imponerse. Propuestas de programa de gobierno no existen. Cartes es presentado por los liberales, y no sólo por ellos, como narcotraficante y contrabandista y éste dice que obligará a Alegre a devolver al Estado 25 millones de dólares que malversó cuando fue Ministro de Obras Públicas en el Gobierno de Lugo, quien lo destituyó a los dos años.

En política internacional, aunque es un tema casi ignorado por ambos candidatos, algo de sus intenciones se filtra a través de algún postulante a gobernador o parlamentario y, en general, poca diferencia habría con el comportamiento del actual gobierno del golpe, es decir, continuar la subordinación a Estados Unidos en todos los planos.

En la afanada competencia por quien es más entreguista de los intereses y la soberanía nacional, Cartes aparenta ser aún más preocupante que Alegre, pues se declara partidario de confiar al Ejército la misión de control de la ciudadanía, desplazando a la policía, proyecto avanzado por el Pentágono para toda Latinoamérica.

Además, en el entorno del candidato colorado, hay varios egresados de cursos militares y de espionaje organizados por Estados Unidos, y todos apoyan la instalación de una base bélica en el Chaco, en la frontera con Bolivia, con los radares puestos en dirección de Argentina, Brasil, Venezuela y la rica Amazonía.

Por la vereda de enfrente, camina una parte del pueblo que piensa diferente, pero que aún no cuenta con una representación política que vaya a la par, convencida de que el país necesita cambios políticos, económicos y culturales, tomando cuerpo el reclamo de un cambio estructural del Estado, cuya función sirva para consolidar independencia y bienestar social, goces que disfrutó la población desde el nacimiento de Nación Independiente en 1813 hasta 1864, cuando el imperio inglés lanzó la Guerra de la Triple (Infamia) Alianza, con los ejércitos de Argentina, Brasil y Uruguay.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

