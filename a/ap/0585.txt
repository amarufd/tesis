
“Territorio robado, será recuperado”



Tres días de debate, intercambio y articulación de luchas. Dos documentos de posicionamiento político (para el Gobernador de Formosa y para la Presidenta de Argentina). Lo principal: el territorio. La denuncia a las industrias extractivas, las respuestas a las organizaciones que cuestionan la lucha, y una Cumbre Indígena que dejó huellas. La marcha por las calles de Formosa y un grito con destinatario claro: “Asesino”.

“Territorio robado, será recuperado”, fue el grito principal de la marcha de ayer, por las calles de Formosa, en el marco de la Cumbre Nacional de Pueblos y Organizaciones Indígenas. Luego de dos días de trabajo, 250 dirigentes de 15 pueblos indígenas elaboraron dos documentos (uno para el gobierno nacional y otro para el provincial) con demandas concretas y coordinaron acciones de lucha para el corto y mediano plazo. Cuando la marcha llegó hasta la puerta de la Casa de Gobierno provincial, un solo grito (destinado al Gobernador) resonó en la capital formoseña: “Asesino. Asesino. Asesino”.

En las afueras de Formosa capital se desarrolló desde el lunes y hasta ayer miércoles una histórica cumbre indígena, convocada por el Consejo Plurinacional Indígena (espacio de articulación de dirigentes indígenas de todo el país). El lunes se hizo hincapié en la realidad de Formosa, donde los cuatro pueblos (Qom, Pilagá, Wichi y Nivaklé) elaboraron un documento sobre la realidad provincial.

El martes llegaron los dirigentes de quince pueblos indígenas de 16 provincias. Desde la mañana y hasta la medianoche, con breves descansos, escribieron un documento para ser presentado (en Casa Rosada) a la presidenta Cristina Fernández de Kirchner.

A las 9.45 de ayer partió desde la Casa Juan Pablo II –lugar del encuentro— la columna de dirigentes indígenas. La bandera del encuentro, con el lema “memoria, verdad y justicia para los pueblos indígenas”, encabezó la marcha. Banderas del Pueblo Mapuche y la wiphala multicolor de las naciones originarias se dejaban ver desde lejos.

Sobre la ruta 11, camino a la capital de Formosa, se iban sumando organizaciones sociales, algunos partidos políticos y gente a pie. Los autos y (muchos) ciclomotores tocaban sus bocinas en señal de aprobación.

“Esto es histórico. Hermanos de todo el país vinieron a abrazarnos y juntos denunciamos la violación de derechos del gobernador Gildo Insfrán y del gobierno nacional. Juntos gritamos basta de atropellos. Juntos luchamos, juntos estamos de pie”, arengó Israel Alegre, histórico dirigente del barrio Nam Qom.

Roxana Vilches, del Pueblo Ranqulche de La Pampa, explicó que la cumbre sirvió para evaluar la situación a nivel nacional “donde en todos los territorios es clara la violación de los derechos de los pueblos indígenas. El mismo Estado que aprueba las leyes, luego las viola y avanza sobre nuestros territorios y nuestras vidas”.

La marcha recorrió tres kilómetros, el sol fuerte se hizo sentir, pero se mantuvieron los cánticos por territorio y los gritos de lucha de los distintos pueblos, en idioma ancestral.

A las 12 se llegó hasta la Casa de Gobierno de Formosa, un edificio de diez pisos, vidriado, en una amplia esquina, y vallado con numerosa custodia policial. Cuando los marchantes llegaron hasta el lugar, se hizo un silencio extraño, sabían que estaban frente a la sede del gobernador (desde hace casi 20 años) Gildo Insfrán, y el primer grito provino de una mujer: “¡Asesino!”. De inmediato, todos los marchantes, gritaron con fuerza: “¡Asesino. Asesino. Asesino!”. El grito se mantuvo durante dos minutos, pero pareció eterno. Se respiraba emoción.

Sobrevinieron aplausos. Y otro grito que unificó a todos: “Territorio robado, será recuperado”.

Documentos

El documento escrito por los cuatro pueblos indígenas de Formosa reclama que se cumpla el relevamiento territorial que estipula la Ley Nacional 26.160, exige que haya un diálogo sincero del Gobierno para resolver los urgentes problemas de salud, educación y agua, insta a que se cumplan los derechos indígenas y exige el fin de la violencia contra las comunidades que luchan.

El escrito para la Presidenta aporta pruebas concretas (en base a información oficial del Instituto Nacional de Asuntos Indígenas y la Auditoría General de la Nación) sobre el incumplimiento de la Ley 26160. Da cuenta de que se gastó el 76 por ciento del presupuesto y sólo se relevaron el 24 por ciento de las comunidades).

Cuestiona la reforma del Código Civil, donde remarcan que no hubo participación indígena según establece la ley y exigen una ley especial para tratar el articulado referido a pueblos indígenas.

Por sobre todo, el documento cuestiona al modelo extractivo de agronegocios, petróleo y megaminería. Deja claro que ese modelo “atenta contra la vida indígena” y cuestiona al Gobierno Nacional por impulsarlo en alianza con las corporaciones.

El martes a la noche, a último momento, se agregó un párrafo donde cuestiona que la jefa de fiscales, Alejandra Gils Carbó, haya dictaminado a favor de la petrolera estadounidense Chevron para levantar un embargo de 19.000 millones de dólares que pesa sobre la multinacional por contaminación en Ecuador. También cuestiona a la Corte Suprema de Justicia de la Nación, que el martes falló a favor de la petrolera Chevron.

En el debate de plenario se hizo hincapié en la alianza del Gobierno Nacional (mediante YPF) con Chevron. “La multinacional petrolera contaminó y provocó muertes en hermanos indígenas de Ecuador, está comprobado por la Justicia de ese país, y acá se la recibe con brazos abiertos. Es la alianza del Gobierno con las corporaciones”, afirmó Jorge Nahuel, de la Confederación Mapuche de Neuquén.

El documento final será leído hoy, en conferencia de presa a las 17, frente a Casa Rosada. Participarán delegados indígenas de la Cumbre organismos de derechos humanos y se solicitó tener una entrevista con la Presidenta.

Discursos

Arriba de una camioneta, abrió el discurso Argentina Paz Quiroga, guía espiritual del Pueblo Warpe de San Juan. Primero hablo en idioma ancestral, luego tradujo: “Muchos creen que no existimos, hacen como que no estamos, pero somos, acá estamos y estamos de pie”. Sobrevinieron aplausos.

Recordó que el Consejo Plurinacional Indígena nació en vísperas del Bicentenario de 2010. Y marcharon miles a Plaza de Mayo. “Somos el Consejo Plurinacional, somos los que estamos de pie, no somos los que bajaron, nosotros no vendemos nuestra dignidad de pueblos indígenas”, gritó la dirigente Warpe y contestó en forma explicita la denuncia de los últimos días de supuestas organizaciones indígenas que cuestionan la Cumbre de Formosa.

A los largo de los dos días, todos los dirigentes originarios desmintieron que se trate de “300 comunidades” y afirmaron que el descontento proviene de la Tupac Amaru (de Milagro Sala), que apoyó la marcha del Bicentenario pero luego (cuando las respuestas de la Presidenta no llegaron y las críticas indígenas se hicieron más duras) dejó el Consejo Plurinacional.

“Nos quieren desterrar de nuestros territorios. Las multinacionales tienen el apoyo de los gobernadores que se dicen parte de un gobierno nacional y popular, pero son gobernadores de las multinacionales”, afirmó Argentina y cerró su discurso: “Cinco siglos resistiendo, estamos de pie, seguiremos luchando, sembrando vida”.

Félix Díaz, autoridad de la comunidad qom Potae Napocna Navogoh (La Primavera) era una de las voces más esperadas. Comenzó hablando en idioma qom. Y luego explicó: “Es un día histórico, esta Cumbre y esta marcha es histórica. Nosotros queremos dialogar, pero miren lo que hace el gobernador Insfrán, nos pone vallas y nos pone a la policía, como el 23 de noviembre de 2010, cuando nos reprimió, mató al hermano Roberto López, nos quemó casas y DNI, nos humilló”.

Y resumió lo que sobrevoló durante tres días en la Cumbre: “No somos opositores, no somos oficialistas, queremos manejar nuestros territorios. No somos dirigentes con sueldos, no somos políticos, queremos trabajar nuestros territorios para la vida”. Y remató: “Qué triste ser títere de un poder económico. Que triste no tener la libertad de los indígenas”.

Félix cuestionó a los medios de comunicación que tergiversan la lucha indígena, apuntó a los “políticos que prometen pero siempre engañan” y se preguntó por qué los gobernantes no quieren dialogar con los pueblos indígenas: “No somos usurpadores, no somos corruptos, por qué no nos reciben”.

“El Estado no valora nuestra vida, no tenemos valor para ellos, sólo quieren el dinero”, acusó Díaz, recordó el incumplimiento de la Ley 26160 y lamentó que ya no esté presente la organización Tupac Amaru “que tanto apoyó con dinero para la marcha de 2010”.

Reconoció que es una “lucha desigual” y contó que “muchos hermanos no pudieron venir por falta de dinero para pasajes”. Remarcó la “capacidad de organización” del Consejo Plurinacional, advirtió que seguirán luchando hasta que “se cumplan los derechos de todos los pueblos indígenas” e informó que el Consejo Plurinacional lo había designado como representante de los pueblos indígenas del país: “Agradezco esa confianza. La confianza no se compra ni se vende, se gana con la lucha. La militancia no se paga, se vive, se comparte. Esta Cumbre y esta marcha es un inicio, un puntapié de la Argentina plurinacional que queremos”.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

