
La paja en el ojo ajeno



El mundo está convulsionado por los tres muertos de la maratón de Boston que ha arrancado la aseveración más fuerte del presidente Obama en sus cinco años de gobierno: “Iremos por los responsables hasta los confines del mundo”.

Todo el mundo cree que los confines del mundo quedan en cualquier parte, menos en Estados Unidos. Damos en pensar que los confines del mundo son muy lejos de donde uno está parado. Y resulta que en este caso de la maratón de Boston, los confines del mundo de los que habla el presidente Obama pueden estar, precisamente, debajo de sus pies.

Las características del atentado llevan todas a creer, sin necesidad de ser un sabueso inglés al estilo Sherlock Holmes o un intuitivo agente de la CIA, que puede tratarse de “un hombre perturbado que actuaba solo al servicio de nadie”, que ha sido la más maravillosa conclusión de todos los grandes acontecimientos criminales en Estados Unidos, como el asesinato del presidente Kennedy a manos de Lee Harvey Oswald, por ejemplo.

Que el presidente Obama haya insinuado ir hasta los confines del mundo en busca del asesino, es lo que preocupa porque, sabemos que él, como todo el mundo pro occidental, tienen los confines del mundo más allá de su mundo: en Irak, Afganistán, Arabia o Corea; o inclusive en algún país de su patio trasero que esté haciendo goteras y que, como excusa del horrendo atentado en Boston, pueda entrar en él en busca del asesino.

No es malo pensar como el presidente Obama. Si los colombianos pensáramos así, ya habríamos dado con el confín del mundo donde permanecen ocultos los asesinos de Gaitán, Galán, Bernardo Jaramillo, Álvaro Gómez, Pizarro León Gómez; todos los magistrados del Palacio de Justicia y tantas otras víctimas del conflicto interno que alguna mano venida del confín del mundo prendió hace años en nuestro medio, y aviva constantemente en diversas formas y maneras, todas llenas de sangre.

-

Fin de folio: Solo ahora, gracias a Obama, podemos comprender la inmensa dimensión de ese viejo dicho que llama a escudriñar por entre la viga que nubla nuestra mirada, buscando la paja en el ojo ajeno.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

