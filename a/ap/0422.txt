
El Secretario de Defensa de Estados Unidos carece de evidencia sobre el uso de armas químicas en Siria



El secretario de Defensa de Estados Unidos, Chuck Hagel, declaró que él no ha visto la evidencia del uso de armas químicas en Siria, anunciado por el gobierno francés la víspera.

En una conferencia de prensa después de la reunión de dos días de ministros de Defensa de la Organización del Tratado del Atlántico Norte (OTAN), Hagel señaló que se reunió con su homólogo francés, pero que Francia no solicitó ninguna acción específica de Estados Unidos respecto a Siria.

El ministro de Relaciones Exteriores de Francia, Laurent Fabius, anunció el martes que su país "no tiene duda" de que el régimen sirio utilizó gas nervioso sarín en su conflicto con los rebeldes y "todas las opciones están en la mesa" para poner fin al conflicto.

Sin embargo, el jefe del Pentágono dijo que no ha visto evidencia de que se hayan empleado esas armas químicas en Siria.

Sobre el papel de la OTAN en la crisis de Siria, Hagel dijo que la alianza debe continuar ayudando a proteger a su país miembro Turquía, "pero más allá de eso no tenemos planes bélicos adicionales respecto a Siria".
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

