
Estudian demanda contra Estado salvadoreño por caso Beatriz



Los abogados de la Agrupación Ciudadana por la Despenalización del Aborto estudian interponer una demanda en contra del Estado salvadoreño, ante la Corte Interamericana de Derechos Humanos (CIDH), en el caso de Beatriz.

De acuerdo a la información de la agrupación, analizarán todo el caso de la joven para determinar si existió violación a los derechos humanos, por el retraso en la atención.

Beatriz es una joven de 22 años que salió a la palestra pública luego que los médicos del hospital de Maternidad recomendaran practicarle un aborto terapéutico cuando tenía 19 semanas de gestión, ya que el feto carecía de corteza cerebral.

En El Salvador, el aborto es penalizado, por lo que se buscó un amparo ante la Sala de lo Constitucional de la Corte Suprema de Justicia (CSJ) para que se avalara dicho procedimiento y no se procesará judicialmente a los médicos y la joven.

Sin embargo, el caso se resolvió en contra de su petición cuando habían pasado más de 50 días, sin importar la urgencia por la gravedad de su estado de salud. Luego de negarle el amparo, la Corte IDH de carácter urgente emitió una sentencia en la que ordenó al Estado salvadoreño atender a la joven para salvar su vida.

Sin embargo Beatriz fue intervenida la tarde del lunes, luego de presentar síntomas de parto, sin necesidad de ser inducida.

La joven se recupera a dos días de su intervención, en la Unidad de Cuidados Intensivos (UCI) del hospital de Maternidad.

Morena Herrera, de la Agrupación Ciudadana por la Despenalización del Aborto, argumentó que tienen seis meses para presentar la demanda, así que se tomarán el tiempo para analizar.

Herrera criticó el “dolor” y “tortura” a que se sometió a Beatriz, a pesar del trabajo hecho por los médicos.

“Es lamentable lo que ha pasado, la actual legislación y la acción de grupos antiderechos han prolongado una situación tan dolorosa, que debió haberse resuelto hacía 14 semanas” , explicó la representante de la Agrupación.

De acuerdo al expediente médico la indicación de la interrupción del embarazo se dio catorce semanas antes, de la cesárea practicada. “Responsabilizamos a esos grupos del sufrimiento, de la tortura y del riesgo innecesario y no sabemos si irreversible en la salud de Beatriz” explicó.

Tanto la Agrupación como el Colectivo Feminista y el Foro Nacional para la Salud se mantendrán vigilantes de la evolución que tendrá la joven y cómo el Estado le responde.

Asimismo, informaron que desde mañana iniciarán un acercamiento con los sectores políticos del país, para poner en debate el tema de la despenalización del aborto terapéutico.

Las organizaciones tienen una propuesta que será presentada a todos los sectores del país. “La actual legislación es injusta y tenemos una propuesta para este debate”, advirtió Herrera.

Mientras que Margarita Posada del Foro Nacional para la Salud, dijo que el tema de la despenalización hay que abordarlo y que no es exclusivo de la iglesia, con activistas de pasarela.

“Vamos a acompañar todo este proceso de revisión del marco legal, qué urge. No puede ser que los feminicidas anden sueltos y que mujeres por abortos naturales por problemas de salud estén hasta esposadas recibiendo atención de salud”, criticó.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

