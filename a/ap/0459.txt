
Eurodiputado español critica política antisocial de la Unión Europea



El eurodiputado español Juan Fernando López Aguilar criticó hoy las políticas antisociales impuestas por la Unión Europea (UE) y aplicadas por los gobiernos conservadores capitaneados por la derecha alemana.

Entrevistado por Televisión Española, el diputado del Partido Socialista Obrero Español (PSOE) en el Parlamento Europeo denunció que la estrategia de austeridad regresiva y destructiva ha llevado a la UE al borde del suicidio y no servirá para salir de la crisis económica.

Es una evidencia empírica, Europa es la única región globalmente relevante que está en recesión, advirtió el presidente de la delegación del PSOE en la Eurocámara, quien demandó un cambio de rumbo urgente.

López Aguilar precisó que esa transformación pasa por una modificación del papel del Banco Central Europeo (BCE) y por una inyección de potencia de fuego del Banco Europeo de Inversiones, con estímulos al crecimiento, a la inversión y a la generación de empleo.

Para eso, subrayó, es necesaria una reforma fiscal europea que no consista tan solo en apretarle el cinturón a los presupuestos de los estados miembros del bloque comunitario.

Calificó de intolerable que la UE se haya gastado medio billón de euros en salvar a entidades financieras y dedique la raquítica cantidad seis mil millones de euros para combatir el alto paro juvenil que sufre el llamado Viejo Continente.

Me sumo a los millones de europeos enfadados con una unión que derramó 500 mil millones de euros en socorrer a los bancos, que luego se enriquecieron comprando la deuda de los países que se endeudaron rescatándolos, remarcó.

A su juicio, se trata de una situación insostenible, porque la gente joven está desenchufando como nunca del proyecto europeo.

El eurodiputado socialdemócrata reclamó 100 mil millones de euros para un plan de empleo juvenil, porque, insistió, seis mil millones es una cantidad raquítica, casi ofensiva.

Dijo comprender a la gente joven cuando se pregunta cómo es posible que se destine todo ese dinero a rescatar bancos con problemas en los cuales ellos mismos se metieron y no hay recursos para salvarlos a ellos.

López Aguilar también lanzó sus dardos contra la troika -formada por la Comisión Europea, el Fondo Monetario Internacional y el BCE-, a la cual tachó de institución sin ninguna legitimación democrática.

Opinó que los errores de la troika han sido fruto de una política deliberadamente diseñada por una hegemonía conservadora al frente de la cual, señaló, se encuentra la canciller alemana, Angela Merkel.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

