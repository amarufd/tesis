
Informan que expertos israelíes en seguridad recorrieron Paraguay



Expertos israelíes en seguridad que trabajan para el presidente electo, Horacio Cartes, recorrieron departamentos del norte del país y zonas fronterizas, divulgó hoy el diario Ultima Hora.

Apoyado en fuentes cercanas a Cartes, el rotativo explicó que se trata de agentes del Instituto de Inteligencia y Operaciones Especiales de Israel (conocido por sus siglas Mossad) y estuvieron en las convulsionadas áreas de los departamentos de San Pedro, Concepción y Amambay.

El objetivo de los especialistas fue "reconocer y analizar" las zonas consideradas como las más conflictivas del país en cuestión de seguridad y en donde opera con total libertad el denominado Ejército del Pueblo Paraguayo (EPP) , planteó el medio de difusión.

Aseguró que Cartes cuenta con cinco guardaespaldas israelíes, sin mencionar a los que tienen a su cargo la seguridad de sus dos hijas, su esposa, su madre y el resto de sus familiares más cercanos.

Estos guardias fueron puestos a disposición del mandatario electo por los expertos del Mossad como una "regalía" por la confianza de Cartes en el trabajo de asesoría, según reveló uno de los hombres más cercanos al próximo gobernante, explicó Ultima Hora.

Añadió que, en todos los actos que el futuro jefe de Estado participa, los israelíes están presentes, aunque oficialmente los encargados de la seguridad presidencial son los miembros del Regimiento de Escolta Presidencial.

En este caso, el jefe de seguridad de ese regimiento en la residencia de Cartes es el mayor Daniel Falcón que, en su momento, resguardó la seguridad del expresidente Nicanor Duarte Frutos, en el periodo 2003/2008.

Los israelíes se encargaron de hacer para Cartes un mapeo de la situación en el norte del país, recorrieron las fronteras y realizaron un informe técnico confidencial para entregar luego al presidente electo, puntualizó Ultima Hora.

Ellos ya recorrieron casi todas las zonas, estudiaron casi toda la región del norte y, sobre todo, a cada uno de los supuestos miembros del EPP, a los más buscados, reveló la fuente.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

