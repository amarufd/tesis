
Argentina, Jujuy. “El escenario del menemismo continúa”: La pobreza llega al 56 de la población. Un 15 es indigente



El reconocido economista jujeño, Carlos Aramayo, informó sobre los resultados de una investigación desarrollada por la Cátedra de Economía que encabeza en relación a la realidad socioeconómica de la provincia y a los índices de indigencia y pobreza de Jujuy. Según se desprende del estudio la pobreza llega al 56 por ciento de la población. Un 15 por ciento es indigente.

"El escenario que inauguró el menemismo continúa", señalo el catedrático. En tanto, ATE y el Frente de Gremios Estatales presentó una propuesta para mejorar el salario familiar.

En relación a las bases utilizadas para el desarrollo de la investigación, Carlos Aramayo explicó que “desde la Cátedra de Economía nos encargamos de estudiar sobre la base de estadísticas serias que no sólo produce el INDEC o la DIPEC en Jujuy, sino también distintas Cámaras Empresariales. Con ellos analizamos la realidad económica y social de Jujuy en los últimos 30 años y sobre esa base vemos la actualidad de la producción y actividad social para opinar sobre cómo se tiene que medir técnica y científicamente la pobreza e indigencia de la provincia de Jujuy”.

Señaló que “la pobreza e indigencia se miden según criterios que tiene que ver con la canasta básica alimentaria, la que se publicaba en la provincia, hasta el año pasado, por la DIPEC, la cual daba una canasta básica de aproximadamente 540 pesos para que se alimente un adulto en edad activa en materia laboral y que le permite reponer diariamente 2.700 calorías para poder volver a su trabajo al otro día”.

Continuó explicando que “este criterio se aplica luego al grupo familiar sobre cuánto es el consumo de calorías de cada miembro ya sea que se trate de 4, 6 u 8. Nosotros tomamos un promedio de 6 miembros, madre, padre, 4 hijos desde los 5 hasta a los 19 años y sobre esa base medimos las unidades calóricas que necesita la familia”.

Al referirse a los resultados de la investigación, Carlos Aramayo afirmó que “luego de estos cálculos se puede medir la indigencia. Lo vital que se debe hacer es alimentarse. Si uno no cubre la canasta básica alimentaria, es indigente. En Jujuy, hechas las mediciones, se necesita 3 mil pesos para que un grupo familiar de 6 miembros supere la indigencia”. Explicó que después se aplica un criterio técnico conocido universalmente como de la inversa del coeficiente de Engel, que es 2,3 y se multiplica con lo que es la canasta de indigencia para llegar a la de la pobreza, en Jujuy, para no ser pobres, un grupo familiar de seis personas debe tener 7.070 pesos mensuales.

“Sobre esa base y conociendo el ingreso promedio de las familias en Jujuy, medidos en este caso por la comisión interna del INDEC que responde a ATE como gremio, en Jujuy andamos en promedio de ingresos familiares de 5.200 pesos. Si uno aplica este nivel de ingreso al conjunto de la población, el 56 de los jujeños es pobres, es decir no llega a los 7.070 pesos por mes en su grupo familiar y un 15 son indigentes, que representa aproximadamente entre 120 a 130 mil personas”, aseguró el economista.

Aramayo señaló que “este escenario en la provincia se inauguró con el menemismo y las privatizaciones en los ‘90 y continúa. Ya llevamos 20 años y las cosas no se han modificado, y si no se producen cambios estructurales en la economía de Jujuy, lo que significa un uso pleno de las tierras que están ociosas, de todos los bosques que se pueden implantar con especies forestales, si no hay políticas de promoción de 2 mil o 3 mil PYMES que modifiquen la estructura del empleo privado, va a ser muy difícil cambiar esta estructura y esta realidad social en la provincia”.

Propuesta de ATE y el Frente de Gremios Estatales

Se reunieron representantes de la Asociación Trabajadores del Estado (ATE-CTA) junto a gremios de la Multisectorioal con representantes del gobierno provincial para tratar puntualmente el tema de las asignaciones familiares.

La reunión contó con la presencia de varios gremios, entre ellos SITRAVIP (Vialidad), Judiciales, APUAP (Profesionales de la Administración Pública) el Frente de Gremios Estatales, ATE Jujuy y SEOM (Municipales). Los sindicalistas hicieron entrega de un estudio referido a la aplicación puntual del salario familiar.

La propuesta plantea una diferenciación con los tramos y modo de aplicación de las asignaciones a nivel nacional, haciendo hincapié en la situación específica de los trabajadores de la provincia y conlleva una mejoría de la situación salarial para los estatales. Los dirigentes aclararon que no quieren dilatar más las definiciones sobre el tema ya que quedan otros puntos para tratar, entre ellos el blanqueo de las sumas “en gris y negro” y una la recomposición salarial acorde con la inflación.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

