
Conmemoran en Polonia aniversario 70 del levantamiento del gueto judío de Varsovia



Con sirenas y repique de campanas Polonia recuerda hoy el aniversario 70 del levantamiento del gueto judío de Varsovia, cuando las tropas alemanas iniciaron una deportación masiva hacia los campos de concentración durante la Segunda Guerra Mundial.

La primera sublevación armada en una ciudad ocupada por los nazis constituyó un acto en defensa de la dignidad que tuvo un precio terrible, con 13 mil judíos muertos y miles de deportados, recordó este viernes el presidente polaco, Bronislav Komorowski.

A las 10:00, hora local, campanas y sirenas sonaron en toda la capital polaca en recuerdo de 750 judíos que en la mañana del 19 de abril de 1943 prefirieron morir con las armas en la mano y no en un campo de exterminio.

El levantamiento fue el último acto de resistencia de personas privadas de su dignidad y esperanza, expresó el jefe de Estado en el acto principal para rememorar la insurrección, realizado hoy ante el monumento a los héroes de la sublevación del gueto de Varsovia.

En la ceremonia, estuvo presente el presidente del Parlamento Europeo, Martin Schulz, y el ministro israelí de Educación, Shai Piron, así como sobrevivientes del Holocausto. El primer ministro de Israel, Simon Peres, quien había anunciado su participación, no hizo acto de presencia.

El presidente de la Eurocámara destacó la importancia de esta conmemoración no sólo para polacos y judíos, sino también para Alemania como muestra de su responsabilidad en lo sucedido hace siete décadas.

Tanto Komorowski como el resto de mandatarios y representantes de la comunidad judía mostraban en su solapa una estrella de David amarilla, similar a la identificación obligatoria que debía llevar la población judía durante el poder nazi.

Symcha Ratajzer, nacido en 1925 y uno de los últimos supervivientes de aquel levantamiento, fue condecorado por Komorowski con la Gran Cruz de la Orden del Renacimiento de Polonia.

Las actividades de los participantes incluyen una visita al monumento de Umschlagplatz, punto de salida de trenes a las cámaras de gas del campo de Treblinka, al que los alemanes mandaron a más de 300 mil judíos de Varsovia.

Este viernes la jornada de recordación continuará en el museo de Historia Judía de Polonia, donde el compositor polaco Krzysztof Penderecki y la Orquesta Sinfónica de Varsovia ofrecerán un concierto en honor a los insurgentes.

Unos 13 mil judíos murieron en la insurrección del gueto capitalino, el cual finalizó en mayo de 1943, mientras la mayoría de los supervivientes fueron enviados a campos de concentración.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

