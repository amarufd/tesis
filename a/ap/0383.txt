
Cuba busca dar mayor autonomía a empresas estatales



El gobierno cubano, que impulsa un profundo proceso de "actualización del modelo económico", otorgará de manera experimental mayor autonomía y capacidad financieras a un grupo de empresas estatales.

"Comienzan los experimentos en un grupo de organizaciones estatales seleccionadas, con el objetivo de ensayar modernas técnicas de gestión, a la par que se les otorga una mayor autonomía y facultades superiores en su actividad económica y financiera", informó el diario Juventud Rebelde, que no ofreció mayores detalles.

El rotativo dijo que la medida se aplicará "a pequeña escala" para probar su eficacia antes de aplicarla en toda la isla.

Unas 3.700 empresas estatales funcionan en la economía cubana, pero la eficiencia de esas entidades se ha vista mermada por la falta de rentabilidad, creatividad y paternalismo.

Como parte de la "actualización", iniciada hace dos años, se han deslindado las funciones estatales de las empresariales, se han fusionado varios ministerios y se han creado empresas independientes en sectores clave para la economía como la biotecnología o el azúcar.

En una reciente reunión del Consejo de Ministros, el vicepresidente, Marino Murillo, opinó que es imprescindible continuar impulsando las transformaciones del sistema empresarial para avanzar en un mejor desenvolvimiento económico.

"Solo transformando el sistema empresarial, que es donde se producen las riquezas, lograremos un desarrollo económico sostenible", dijo Murillo.

"Podemos darnos cuenta de que se avanza a buen ritmo, pues la magnitud y complejidad de los problemas no permiten que podamos resolverlos de un día para otro. Tenemos que resistir a las presiones de quienes insisten en que debemos ir más rápido", dijo el presidente Raúl Castro en esa reunión del gabinete.

"Ahora estamos en un mejor momento, el Programa del Partido nos ha ayudado a trabajar con más orden y disciplina", agregó Castro.

En abril de 2011, el VI Congreso del Partido Comunista de Cuba (PCC) aprobó una "hoja de ruta" con más de 300 medidas para hacer revivir la maltrecha economía cubana.

Por su parte, la ministra de Finanzas y Precios, Lina Pedraza, presentó las directivas que regirán la propuesta de Presupuesto del Estado en el 2014.

Pedraza anunció que tras cumplir los compromisos tributarios con el Estado, las empresas podrán retener hasta el 50 por ciento de las utilidades para crear fondos.

Estos fondos se destinarán "a incrementar el capital de trabajo, a las inversiones, al desarrollo, las investigaciones y la capacitación, así como, a pagar a los trabajadores por los resultados".

En cuanto a los ingresos tributarios, Pedraza señaló que se prevé un incremento a partir de un alza en la recaudación por impuestos sobre los servicios, considerando la ampliación del trabajo por cuenta propia y de otras formas de gestión.

El gobierno prioriza la producción de alimentos, para lo que desde finales de 2008 ha entregado en usufructo las tierras ociosas y otorgado créditos, además de vender útiles de labor y mejorar los precios de los productos agrícolas.

Sin embargo, la producción agrícola no ha alcanzado los niveles esperados y obliga a importar arroz, granos, soja, leche en polvo, carnes y otros.

El Estado controla el 60 por ciento de las tierras en Cuba, pero las granjas y cooperativas estatales producen apenas un 30 por ciento de los alimentos cosechados en la isla.

"La clave actual de la reforma económica cubana es la agricultura, como lo fue en Vietnam y en China. Si fracasa la reforma en la agricultura, fracasa toda la reforma", afirmó el investigador Pavel Vidal, del Centro de Estudios de la Economía Cubana, en una reciente entrevista con la revista Espacio Laical.

Vidal apuntó que si se resuelve el tema de la alimentación, que es una de las demandas sociales más importantes, donde la población quiere ver resultados a corto plazo, "la reforma tendría mucho más apoyo popular".

Economistas cubanos estiman que las familias gastan entre el 60 y el 80 por ciento de los ingresos en la compra de alimentos, pues la canasta básica subsidiada que recibe mes por mes cada cubano sólo alcanza para unos 15 días.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

