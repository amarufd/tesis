
México. Comentario a tiempo: Explotación infantil



Uno de los problemas sociales más graves, no sólo de México sino del mundo entero, incluyendo a los países más desarrollados, es el del trabajo infantil o la explotación de los niños.

Una iniciativa de ley que sube de 14 a 15 años la edad permitida para que un menor sea contratado para un trabajo, ha sido enviada por el presidente, Enrique Peña Nieto, al Congreso de la Unión, con el propósito de evitar esta vulnerabilidad en el desarrollo de los seres humanos en su pubertad.

En sus considerandos más sobresalientes explica: Que de conformidad con los artículos 4o. de la Constitución Política de los Estados Unidos Mexicanos, 1o y 7o de la Ley para la Protección de los Derechos de Niñas, Niños y Adolescentes, en las decisiones y actuaciones del Estado debe prevalecer el principio del interés superior de la niñez, para garantizar a niñas, niños y adolescentes la tutela y el respeto de sus derechos fundamentales;

Que el Plan Nacional de Desarrollo 2013-2018, conforme a la Meta Nacional IV "México Próspero", contempla en su objetivo 4.3 "Promover el empleo de calidad", y establece como estrategia 4.3.2 "El promover el trabajo digno o decente, a través de diversas líneas de acción, entre las que destaca la encaminada a contribuir a la erradicación del trabajo infantil".

Que el trabajo infantil constituye un fenómeno de alcance mundial al que ningún país ni región es inmune, por lo que es necesario fortalecer las instituciones para identificar y atender los factores sociales de riesgo que lo propician, entre los que destacan la falta de acceso a servicios básicos; la carencia de vivienda digna y situación de calle; la desintegración familiar; la educación nula o deficiente y la deserción escolar; las adicciones y la falta de espacios públicos seguros, así como para proteger y garantizar los derechos inherentes a los trabajadores adolescentes.

Desde luego, que es de aplaudirse la iniciativa, sin embargo el problema social toral persistirá a pesar de lo que digan las leyes, puesto que la situación de pobreza de las familias las obliga a iniciar en el trabajo a los niños apenas tengan la fuerza suficiente para desarrollar algún tipo de tarea.

Esto, muy aparte de los niños desheredados, en algunos casos llamados de la calle, que se ven obligados a buscar su propia subsistencia inclusive con la explotación inicua en su propio cuerpo.

En consecuencia, el problema no se reducirá a la aprobación de leyes sino a atender en toda su profundidad la pobreza que se extiende en más del 50 por ciento de la población. Mientras eso no suceda, la explotación infantil continuará, por desgracia, en nuestro país.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

