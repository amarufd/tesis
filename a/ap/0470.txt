
Habrá 100 millones de turistas chinos en 2015



El número de turistas chinos llegará en 2015 a los 100 millones, una cifra que se alcanzará cinco años antes de lo previsto por la Organización Mundial de Turismo (OMT), calculó el director del Centro de Promoción Turística en China de la Agencia Catalana de Turismo, Antonio Li.

Li hizo esta afirmación durante su ponencia en el III Congreso Internacional de Turismo Asiático, que fue clausurada hoy en Barcelona, capital de la región de Cataluña, España.

Li explicó que los datos actuales sobre el turismo chino permitían afirmar que la previsión de la OMT para el 2020 se adelantaría al 2015.

En 2012 viajaron "un poco por encima de 83 millones" y China experimentó "una tasa de crecimiento en los últimos cinco años por encima del 15 o 14 por ciento, por lo que esa cifra de 100 millones de viajes al exterior llegará en el 2015", concluyó.

El turismo chino se incrementó un 7 por ciento en el año 2012 y sus ciudadanos gastaron un total de 79.400 millones de euros (unos 103.825 millones de dólares) en sus viajes fuera de su país, lo que confirma su importancia para la economía española, actualmente en crisis.

Li dijo que los ciudadanos chinos viajaban a España por motivos culturales pero los famosos equipos del Futbol Club Barcelona y Real Madrid eran importantes factores también de motivación.

España recibió un total de 177.100 turistas chinos en 2012, es el segundo mercado emisor tanto para España como Cataluña que recibió más de 100.000 turistas chinos el año pasado sobre todo en su capital, Barcelona.

Durante el Congreso los ponentes también señalaron las limitaciones del sector turístico español a la hora de atraer al turista proveniente de Asia, entre las cuales están el escaso número de vuelos directos y las largas gestiones para obtener un visado, algo en lo que se ha mejorado según los expertos.

España, por ejemplo, es el único país europeo que no tiene conexión directa con Japón, dijo la directora de la Oficina Española de Turismo para Japón Beatriz Marco.

El Congreso Internacional contó con la presencia de importantes autoridades como el Secretario General de la Organización Mundial de Turismo (OMT), Taleb Rifai quien aprovechó la ocasión para anunciar que Barcelona acogería la conferencia más importante del mundo sobre turismo urbano en 2014.

Rifai aconsejó "estar preparados" para el aumento del turismo asiático previsto para los próximos años apostando por las nuevas tecnologías, adaptándose a la diversidad asiática, facilitando el viaje y fomentando la cooperación entre países europeos al respecto, porque los asiáticos "viajan a Europa".

El Congreso de Turismo Asiático, de dos días de duración, tuvo como título "Asia, el gran mercado emisor y las nuevas tecnologías como instrumentos de promoción y fidelización".

Su objetivo fue analizar el potencial de este turismo como un mercado emisor para España en general, y para Cataluña en particular y en el participaron alrededor de 30 expertos.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

