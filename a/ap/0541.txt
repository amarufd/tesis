
Medvédev alerta sobre la desaceleración de la economía rusa



El primer ministro de Rusia, Dmitri Medvédev, evaluó hoy en términos positivos los resultados macroeconómicos de 2012 pero alertó del riesgo de la desaceleración de la economía rusa.

“Si tomamos en consideración el estado de la economía global, los resultados macroeconómicos del año (pasado en Rusia) no están mal”, declaró Medvédv al rendir cuentas de su gestión ante la Duma de Estado, o Cámara Baja del Parlamento ruso,

Recordó en particular que la economía rusa creció un 3,4 en 2012, la tasa del paro es de apenas un 5,5, mientras que el déficit presupuestario y la deuda pública no superan, respectivamente, el 0,06 y el 10 del PIB.

Al mismo tiempo, constató que “subsiste la tendencia a la ralentización del crecimiento económico a juzgar por los primeros meses de este año”.

“Es un riesgo muy serio”, agregó.

El Gobierno, los legisladores y los expertos, a su juicio, deberían debatir entre todos los posibles estímulos al crecimiento de la economía y compartir la responsabilidad.

Al margen de los ritmos del crecimiento, importan la calidad y la estructura de la economía, subrayó el primer ministro. Dijo que es necesario mejorar el clima empresarial, incrementar la eficacia de la economía y del sector social, así como centrarse en el fomento de la competitividad y el potencial humano.

Las inversiones extranjeras directas en Rusia, según él, deben incrementar hacia 2018 de 50.000 a 70.000 millones de dólares anuales.

Medvédev recordó que la economía rusa captó en el último quinquenio 265.000 millones de dólares en inversiones foráneas directas, lo que la sitúa en el segundo lugar dentro del Grupo BRICS (Brasil, Rusia, India, China y Sudáfrica), y en el sexto a escala global.

La economía rusa creció un 1 en enero-marzo de 2013, frente al 4 del primer trimestre de 2012.

La semana pasada, el Ministerio de Desarrollo Económico revisó a la baja su previsión de varios indicadores macroeconómicos para 2013.

El producto interno bruto (PIB) de Rusia crecerá este año un 2,4, un punto y dos décimas por debajo del pronóstico anterior, dijo a la prensa el viceministro Andréi Klepach. La producción industrial, conforme al guion optimista, aumentará solo un 2.

Para 2014 y 2015, la revisión del crecimiento económico es del 4,3 al 3,7 y del 4,5 al 4,1, respectivamente. En 2016 se espera un crecimiento del 4,2.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

