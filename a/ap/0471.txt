
Haití: Un país maquila que no tiene qué comer 



Hace 30 años, los asesores haitianos y estadounidenses del dictador Jean-Claude Duvalier compartían la misma visión del futuro de Haití: el país debería ser el "Taiwán de El Caribe". Es decir: un gigantesco complejo de maquilas que garantizaría salarios de miseria en las industrias de textil, electrónica y de fabricación de implementos de béisbol de Estados Unidos. La 'reconstrucción' del país va por el mismo camino.

Tres décadas después de esta visión, el gobierno de Michel Martelly, la Comisión Interina para la Reconstrucción de Haití (CIRH), el Departamento de Estado de Estados Unidos, el Banco Mundial (BM), el Banco Interamericano de Desarrollo (BID), otras instituciones financieras internacionales, George Soros y otros actores implicados en la 'reconstrucción' de Haití planean lograr que 200 o quizás 500 mil obreras y obreros haitianos trabajen con salarios de hambre en las "zonas francas" y en los "parques industriales" (eufemismo para nombrar las maquilas donde se ensamblan piezas importadas libres de impuestos, que a su vez serán re-exportadas sin pagar impuestos).

Hay un problema

"Hay un problema en mi país: trabajo en una fábrica hace 25 años y todavía no tengo casa propia", le confió Evelyne Pierre-Paul a Ayiti Kale Je (AKJ). Evelyn tiene 50 años y tres hijos. Antes del terremoto de enero de 2010 vivía en un cuarto de alquiler. Veintidós meses después del seísmo, ella y su familia continúan hacinados en una tienda en uno de los sórdidos campos de refugiados de Puerto Príncipe. Su salario es de 225 gourdes (4,69 $US) por día. Esa suma no alcanza para cubrir ni siquiera la mitad de los gastos básicos de una familia. Por eso, no todos los hijos de Evelyne Pierre-Paul pueden ir a la escuela. "El día de pago, después de cubrir las deudas no me queda casi nada", explica Evelyn, que cose vestidos para One World Apparel, una fábrica gigantesca donde las obreras y obreros cortan y cosen vestidos para K-Mart, Wal-Mart y algunas compañías que venden uniformes.

Según Ayiti Kale Je (AKJ), en 2011, había en las maquilas haitianas cerca de 29 mil trabajadores, de los cuales el 65 son mujeres, que cortaban y cosían vestidos para Banana Republic, Gap, Gildan Activewear, Levis y otras marcas. La agencia haitiana señala que su salario es más bajo que en los tiempos de la dictadura de "Bebé Doc".

El estudio 'Time for a «High-Road» Approach to EPZ Development in Haiti', de Yasmine Shamsie, señala que el modelo de las zonas francas donde funcionan las maquilas ha incrementado la concentración de la riqueza y de las desigualdades entre las regiones en Haití, ha contribuido a aumentar el precio de la alimentación y la vivienda, y ha impulsado el crecimiento de cordones de miseria alrededor de las fábricas porque los salarios excesivamente bajos impiden que las obreras y obreros puedan pagar una vivienda digna y segura.

El Parque Industrial Caracol

En octubre de 2012 el gobierno haitiano y algunas autoridades de los "países amigos de Haití" vieron su sueño hacerse realidad durante la inauguración del Parque Industrial Caracol (PIC) que, según ellos, creará 20 mil -tal vez 65 mil- empleos. El PIC fue presentado como la "joya de la corona" de la 'reconstrucción' después de la catástrofe de enero del 2010.

El presidente Michel Martelly aprovechó la ocasión para repetir una vez más que "Haití es un país abierto a los negocios". Multimillonarios, actores y gobernantes extranjeros aplaudieron al presidente. Los folletos de promoción de Caracol prometían que el parque haría de Haití un país "competitivo a nivel mundial, sin comprometer los estándares laborales y ambientales".

El PIC es un proyecto de los gobiernos de Haití, Estados Unidos y el Banco Interamericano de Desarrollo (BID). La agencia haitiana de información Ayiti Kale Je (AKJ) revela que, un año después de iniciar operaciones, 1.388 personas trabajan en el Parque Industrial Caracol, entre estos 26 personas de otros países y 24 agentes de seguridad. La maquila coreana que emplea el mayor número de personas es S&H Global, una filial de SAE-A Trading. Esta maquila ensambla ropa para marcas estadounidenses como JC Penny, WalMart y otras.

Según el New York Times, antes de que se firmara el acuerdo para que la fábrica coreana se estableciera en Caracol, la AFL-CIO, la mayor federación de sindicatos de los Estados Unidos, instó a los funcionarios estadounidenses e internacionales a reconsiderar abrir las puertas de Caracol a SAE-A Trading y, les envió información detallada sobre la "represión antisindical atroz" que Sae-A llevó a cabo en Guatemala, que incluyó "actos de violencia e intimidación". En la información, Homero Fuentes, quien supervisa las fábricas para los minoristas estadounidenses, clasifica a Sae-A como "uno de los violadores más importantes de las normas laborales."

La advertencia de AFL-CIO fue desoída. Como en la mayoría de maquilas, en Caracol, S&H Global ha contratado sobre todo a mujeres jóvenes. A pesar de que el salario mínimo en Haití es de 300 gourdes por día, la maquila coreana paga 200 gourdes (4,75 $ US). AKJ pudo establecer que una obrera puede gastar 61 gourdes en transporte y 82 en alimentación. Sólo le restarían 57 gourdes o 1,36 $ US para los gastos de su familia.

Una de las obreras entrevistadas por AKJ declaro: "Nos tratan como a bestias. Nos gritan. Los alimentos que nos venden están mal preparados, solo nos dan agua caliente, trabajamos sin cubreboca, el polvo nos entra en la nariz. Los supervisores no nos respetan, no nos consideran como a seres humanos, nos golpean con los vestidos que estamos cosiendo". A pesar de que SAE-A asegura respetar el código de trabajo de Haití, se negó a recibir a un grupo de reporteros de AKJ en su fábrica en Caracol.

Compitiendo con China

El economista haitiano y profesor de l'Université d'État de Haïti Frédérick Gérald Chéry explica que el salario pagado en las fábricas de ensamblaje, con la complicidad del gobierno haitiano, no contribuye al crecimiento de la economía. "El salario mínimo se debe fijar en términos de la canasta básica y de los precios de los productos locales. No podemos empujar a un obrero a comprar maíz importado de Estados Unidos", advierte Chéry.

La exministra de Asuntos Sociales, Josépha Raymond Gauthier, reconoció en una entrevista que los salarios son bajos, pero se apresuró a repetir la misma justificación que enarbolan los dueños de las ensambladoras. "Alguien que trabaja (en una maquila) no va a volverse rico de la noche a la mañana. Pero el que no trabaja, no tiene ninguna esperanza".

Los que trabajan en las maquilas haitianas tampoco pueden hacerse muchas esperanzas. Todas las compañías que se instalarán en el PIC se beneficiarán de incentivos fiscales, y las maquilas de ropa tienen privilegios suplementarios en virtud de la ley HELP (Haiti Economic Lift Program, por sus siglas en inglés), aprobada después del terremoto por el Congreso de Estados Unidos. La ley triplica las cuotas de exoneración de aduana para las exportaciones de ropa fabricada en Haití a los Estados Unidos hasta el año 2020. Como contrapartida, Haití se comprometió a garantizar que los salarios permanecerán a un nivel "suficientemente" bajo. De hecho, el informe "Private Sector Development in Haiti: Opportunities for Investment, Job Creation and Growth The World", preparado por el Banco Mundial y el BID para el Forum Económico Mundial de Davos en 2011, señala que para ese año el costo de la mano de obra en Haití era "perfectamente competitivo con el de China".

Desplazamiento a cambio de salarios de hambre

Para construir el Parque Industrial Caracol, el gobierno haitiano y sus socios (BID y el Departamento de Estado de Estados Unidos) desplazaron a 366 familias que explotaban 250 hectáreas de tierras fértiles. La producción de estas parcelas aseguraba la sobrevivencia de cerca de 2.500 personas y el trabajo de 750 agricultores. Desde noviembre de 2011, el espacio que ocupaban los agricultores fue asfaltado con el fin de construir hangares en donde supuestamente funcionarán las maquilas.

Las autoridades haitianas han declarado que han indemnizado a los agricultores y que han encontrado un espacio para reubicarlos cerca de Glaudine. Pero, después de dos años, los agricultores se muestran escépticos. Recuerdan que en la zona de Ouanaminthe, donde se construyó el Parque Industrial CODEVI, en 2003, todavía hay agricultores expulsados que no han recibido la tierra que les prometieron.

Antes Caracol era el granero del departamento del Noreste. Pero "en este momento escasean los productos agrícolas, vivimos en la miseria", señaló a AKJ el agricultor expulsado Breus Wilcien. Vilsaint Joseph, una de las autoridades de la comunidad, recuerda: "Antes, cuando se recogía la cosecha, los camiones salían cargados de maíz y frijoles hacia Puerto Príncipe".

Aunque cualquier persona con tres dedos de frente, el gobierno de Martelly y los onerosos estudios de las agencias de cooperación y las Naciones Unidas identifican la producción de alimentos y la seguridad alimentaria como una de las prioridades del país, la "reconstrucción" financiada y apoyada por actores extranjeros continúa dando prioridad a la visión de Haití como "país-maquila" en detrimento del apoyo a los pequeños agricultores.

En una entrevista realizada por el New York Times en 2012, José Agustín Aguerre, director del BID en Haití (entidad que financia el parque industrial Caracol), reconoció que "crear una industria de maquila de ropa es una opción que todo el mundo intenta evitar". El muy bien pagado funcionario Aguerre consideró esta opción "como un último recurso". Pero aseguró que la maquila es "una buena oportunidad" para Haití, "aunque los salarios son bajos". Y para finalizar declaró: "Sí, mañana las compañías podrían irse porque encuentran mejores lugares. Pero todo el mundo piensa que esta apuesta valía la pena (para Haití)". Aguerre se refería a "todo el mundo" en el BID.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

