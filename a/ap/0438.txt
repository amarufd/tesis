
Está previsto que este viernes se conozca la sentencia en el juicio oral por el homicidio de Mariano Ferreyra



Previo a ello, el Tribunal Oral en lo Criminal N° 21 de la Capital brindará a los imputados la posibilidad de decir sus últimas palabras antes de la lectura del veredicto. Entre los acusados está José Ángel Pedraza. CIJ TV transmitirá en vivo.

Está previsto que este viernes, el Tribunal Oral en lo Criminal N° 21 de la Capital dé a conocer la sentencia en el juicio oral por el homicidio del joven militante del Partido Obrero Mariano Ferreyra, ocurrido el 20 de octubre de 2010.

Previo a ello, los jueces Horacio Días, Diego Barroetaveña y Carlos Bossi, que integran el tribunal, ofrecerán a los imputados la posibilidad de expresar sus últimas palabras antes de la sentencia. CIJ TV transmitirá en vivo.

Cabe recordar que la Fiscalía solicitó la pena de prisión perpetua para José Pedraza, titular de la Unión Ferroviaria, y Juán Carlos Fernández, acusándolos de ser instigadores del homicidio del joven militante del Partido Obrero y de la tentativa de homicidio de Elsa Rodríguez, Nelson Aguirre y Ariel Pintos.

En tanto que requirió la misma pena para Cristian Daniel Favale, Gabriel Fernando Sánchez, Pablo Marcelo Díaz, como coautores de esos mismo delitos.

Además, pidió 10 años de prisión para Gustavo Alcorcel y 9 años para Daniel González y Salvador Pipitó.

Con respecto al personal policial, solicitó las siguientes penas: 10 años para Hugo Lompizano y Luis Mansilla, 9 años para Jorge Ferreyra, 8 años para Luis Echavarría, 7 años para Rolando Garay y Gastón Conti. En todos los casos se les imputó el abandono de persona. En tanto que pidió la absolución de David Villalaba.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

