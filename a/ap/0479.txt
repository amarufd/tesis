
Inundaciones: el endeudamiento es más miseria y ninguna solución



Informe de lo discutido en Asamblea de Asambleas (11/06/13).

Con la participación de 7 Asambleas barriales este martes 11 de junio se llevó a cabo la reunión del organismo que reúne a las asambleas barriales. En primer lugar se informó sobre una reunión mantenida entre 4 asambleas vecinales con el intendente Bruera y su hermano, el diputado provincial, G Bruera, el día lunes 10 en el edificio de la municipalidad.

Si bien la mayoría de los presentes se encargó de cuestionar que esa reunión haya sido acotada a un puñado de asambleas -cuando se encuentra pendiente el pedido de audiencia con todas las asambleas al titular del Ejecutivo municipal, desde el mes de abril- no deja de tener importancia el informe brindado por uno de los compañeros presentes ( Pque. Castelli) en dicha reunión, porque nos permite conocer lo argumentos con los que los Bruera, intentan abordar la cuestión hídrica en nuestra ciudad y sus barrios periféricos.

Debe quedar claro, desde un primer momento, que ninguno de los presentes planteó el ultimátum para que los funcionarios o nos reciben a “todos o ninguno”, dado que cada asamblea tiene una cierta autonomía para mantener reuniones con los funcionarios que quieran, por los problemas puntuales de cada barrio. Lo que si se discutió en forma de comunicado, es el afán por aclarar que esta reunión mantenida por el municipio con algunas asambleas, no se corresponde con los pedidos vecinales durante todo el mes de abril y mayo ni con el espíritu de las grandes movilizaciones que recorrieron la ciudad de La Plata tanto en el primer aniversario de la catástrofe como en el segundo. En este sentido, el intendente Bruera sigue en deuda con la sociedad platense y debe recibir a los vecinos afectados por la trágica inundación, aun cuando lo entiendan como corresponsable de la desgracia vivida ese dos de abril.

De todas formas, aunque no representativa, la reunión realizada entre el intendente y este puñado de asambleas tiene un interés por lo menos curioso, por los argumentos que esgrimieron los funcionarios municipales frente a los asambleístas, los que nos da una idea de sus planes o por lo menos como intentan seguir con el tema de las obras hídricas en nuestra ciudad, esto es: con una serie de promesas parciales para algunos barrios, de justificación de sus políticas, para la mayoría; la desacreditación de los reclamos y de generar división, en la medida de lo posibles y de que se lo permitamos, al organismo que nucléa a las Asambleas.

Primer argumento: “el COU ha pasado a mejor vida”

Según el intendente Pablo Bruera el tema del “código de planeamiento urbano” no debe seguir siendo motivo de preocupación vecinal dado que la crisis económica y su secuela de parates en la comercialización inmobiliaria ha hecho que se paralizaran las obras. De esta manera el funcionario desacredita el reclamo vecinal por la derogación del COU.

La realidad es, que el problema con este código no sólo está en lo que produciría hacia adelante sino en lo que ya produjo el 2 de abril. Es decir, con declarar que el COU “ha muerto” no se hace más que repetir “viva el COU” el cual, mientras siga vigente, establece las pautas de construcción arbitrarias de torres y mega torres en toda la ciudad; la perdida de espacios verdes que absorban el agua de lluvia; profundiza la falta de infraestructura pluvial y sanitaria, etc.

Además, todo el plan nacional de puesto en práctica por el gobierno nacional (de los CEDIN) tiene como su costado más fuerte que el lavado de dinero se haga mediante emprendimientos inmobiliarios. Los nuevos “inversores” que traigan su capital negro, no sólo se opondrán a la derogación del COU sino que precisarán de su profundización, en perjuicio de los vecinos platenses, para que resulte un negocio más rentable.

Segundo argumento: “existe un plan”

Los vecinos presentes en esta reunión reservada se desayunaron con que el municipio ya tiene un plan de obra pública encargado a una consultora privada para resolver el tema hídrico. Según se informó este plan será puesto prontamente a disposición de los vecinos para efectúen un control del mismo y su discusión. No se explicó si la consultora privada que realizó el plan cobró por el mismo y cuanto; si tiene en cuenta las recomendaciones realizadas por los técnicos de la UNLP, que venían pronosticado la emergencia en que se encuentra nuestra ciudad frente a lluvias fuertes, etc. Tampoco como intervienen los trabajadores del Ministerio de hidráulica en la confección del plan, ya que ellos son los mas idóneos para la elaboración del mismo. En definitiva, los funcionarios municipales plantean ejecutar un plan que hasta ahora tiene un carácter secreto y el cual incluye el endeudamiento en 2 mil millones de pesos.

La existencia de este plan hídrico va de la mano con el tercer argumento: la falta de fondos.

Tercer argumento: “endeudamiento”

Los Bruera informaron que los recursos para la realización de las obras son escasos y que por esta razón el Estado provincial deberá contratar deuda externa con organismos internacionales por 2 mil millones de pesos.

La Asamblea de Asamblea discutió este posible endeudamiento provincial destacando que:

1- el endeudamiento en obra pública ha sido históricamente el pilar de la corrupción entre funcionarios, testaferros y empresas privadas, que finalmente desvían los fondos destinados a obras
para su enriquecimiento particular sin que las obras se realicen, o que s realicen a medias.

2- el endeudamiento externo es finalmente pagado por todos los vecinos cuando la responsabilidad de la falta de obras se da por la inescrupulosidad del negocio privado que levantó grandes torres sin tener en cuanta un mejoramiento en pluviales y servicios. El pueblo platense pagará de esta manera tres veces las obras necesarias. La primera, con los impuestos ya pagados con la esperanza de que las obras se hagan; la segunda, con la perdida de sus bienes y vidas humanas, con la reconstrucción de sus propiedades rotas por el temporal, etc. La tercera, cuando los efectos del endeudamiento publico vuelva como deuda a todos nosotros en ajustes y recortes de salud, educación y gastos sociales.

Las tareas de reconstrucción serán encargadas a las mismas empresas constructoras que se vieron beneficiadas con la sanción del COU, con lo cual, cuando nosotros perdemos 3 veces ellos ganan por duplicado.

Presentar el endeudamiento externo, como el único recurso para que se realicen las obras es una falacia y una renuncia. Falacia, porque el gobierno nacional y sus correveidiles provincial y municipal saben que existen fondos legítimos para la realización de esta obra si recurrir a generar nueva deuda externa. Y una renuncia, a cobrarles deudas e impuestos a lo grandes evasores de fortunas y a las constructoras corresponsables del desastre, a los amigos que viven de los subsidios estatales (los trenes argentinos y las líneas de micros son mas que una muestra de esto), a los funcionarios de las altas jerarquías del Estado, a los sueldos siderales de diputados y senadores, etc.

La provincia se dispone a tomar una deuda bajo la excusa de realizar obras para que La Plata no vuelva a inundarse; es una posición que terminará de coronar los efectos de la inundación hasta sacarles, esta vez no por intermedio del agua, hasta el último recurso al pueblo. Como se dice normalmente el negocio de la guerra no está sólo en las ventas de armas sino y fundamentalmente en las tareas de reconstrucción de lo que la guerra destruyó.

La Asamblea de Asambleas deberá discutir con mayor profundidad este tema porque la desesperación y angustia vecinal puede actuar en contra de nosotros mismos en este y en otros temas colaterales.

Defender los 6 puntos del petitorio, votado y firmado por más de 10 mil vecinos es el camino frente a las promesas de siempre.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

