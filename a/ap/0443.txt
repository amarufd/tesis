
Estados Unidos duplica la asistencia para la oposición siria hasta 250 millones de dólares



El secretario estadounidense de Estado, John Kerry, anunció el sábado en Estambul que su país proveerá otros 123 millones de dólares estadounidenses en asistencia no letal a la oposición siria.

Kerry anunció el compromiso, que aumentaría la suma de Washington a 250 millones de dólares, en una conferencia de prensa poco después de la reunión en Estambul de 11 ministros de Exteriores de países occidentales y árabes.

La asistencia de Estados Unidos se concentrará en el suministro de alimentos y servicios médicos, y el apoyo a los líderes sirios locales para realizar trabajos básicos por un futuro democrático de Siria, dijo el máximo diplomático estadounidense.

Se espera que el paquete también incluirá por primera vez vehículos blindados, chalecos antibalas, gafas de visión nocturna y otras provisiones militares defensivas.

Todos los partidarios extranjeros, añadió, han acordado dejar el insurgente Mando Militar Supremo (CMS) manejar su ayuda y asistencia.

El CMS es una fuerza militar de la oposición que opera bajo la protección del grupo de la Coalición Nacional Siria. Con una estructura establecida de control de comando, realiza operaciones militares organizadas contra las fuerzas del gobierno.

Kerry destacó que los "Amigos de Siria" y la Coalición Nacional Siria comparten la misma opinión de que hay que buscar una transición pacífica.

"Estamos de acuerdo por un gobierno transicional para el nuevo liderazgo de Siria. Eso es el camino a la paz. Esperamos que podamos de algún modo alcanzar negociaciones pacíficas bajo el marco del Comunicado de Ginebra", añadió.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

