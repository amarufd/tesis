
El turismo cubano por una mejor y variada oferta



El ministerio cubano del Turismo (Mintur) trabaja de manera intensa en elevar la capacidad y calidad de los servicios, diversificar los mercados y las ofertas, y perfeccionar la comunicación promocional dirigida especialmente al mercado interno.

La viceministra del sector, Xiomara Martínez, afirmó en un programa de televisión que se busca incrementar la competitividad de la isla en los mercados, a partir de la elevación de la calidad de los servicios y la diversificación de los mercados emisores.

En el habitual programa "Mesa Redonda", dedicado esta vez al desarrollo del turismo en la mayor de las Antillas, Martínez apuntó la noche del jueves que en 2012 la nación caribeña inauguró ocho nuevos hoteles y aumentó a 60.552 la capacidad habitacional de su infraestructura turística.

Durante el pasado año, 2 millones 838.468 visitantes extranjeros llegaron a la ínsula, cifra récord para la industria turística cubana, así como de crecimiento en esta esfera de un 4,5 por ciento, comportamiento superior a la media mundial y regional, según expertos locales.

Martínez destacó que también se avanza en la actividad privada de alojamiento (más de 6.000 habitaciones) y paladares o restaurantes (más de 2.200), como oferta turística complementaria a la estatal.

Admitió que en el ámbito interno persisten problemas asociados a la calidad y diversidad del servicio, que limitan alcanzar los resultados esperados.

A pesar de esas dificultades, más de un millón de cubanos disfrutaron de las ofertas turísticas disponibles el pasado año, promocionadas por las agencias que operan en pesos convertibles o divisas (CUC) y Campismo Popular (actividad de acampada que se realiza en contacto directo con la naturaleza).

José Manuel Bisbé, director comercial del Mintur, alertó que en lo que va de año se registra una disminución del 1,8 por ciento en la llegada de turistas, debido a ligeros decrecimientos de flujos de algunos mercados.

Entre las causas externas mencionó los efectos de la crisis económica en Europa, que afecta a importantes mercados emisores como España, Francia y Reino Unido, y la necesidad de actualizar la campaña promocional en el exterior.

Cuba es más que sol y playa, también cuenta con un rico patrimonio, cultura, naturaleza, eventos y náutica; en ese sentido, hay que ser más agresivo en las estrategias de comunicación hacia el mundo, subrayó Bisbé.

Al respecto, Dalila González, directora de Comunicación del propio ministerio, acotó que este se encuentra en un proceso de renovación, con una estrategia dirigida a afianzar espacios y ganar otros.

Explicó que Cuba pierde casi la mitad de los turistas que pasan por el Caribe, debido a que son estadounidenses y la política de bloqueo impuesta por Washington a La Habana desde hace más de medio siglo les impide viajar a la Isla.

Para revertir la tendencia negativa en el arribo de turistas y poder llegar este año a la ansiada meta de tres millones de visitantes, los directivos del ramo se han trazado una estrategia sobre la base de desarrollar un "turismo de paz, bienestar y salud".

"Cuba es uno de los países más seguros, sin drogas ni crímenes, con una población muy solidaria y la más alta profesionalidad del personal del sector", aseveró la viceministra.

Las previsiones de crecimiento de la planta hotelera local están cifradas para el 2020 en 85.000 habitaciones, de acuerdo con las inversiones, los estimados de aumento en las emisiones de los mercados y las potencialidades de cada polo.

Un sector importante en la nueva estrategia es el mercado interno y existe un plan de ampliar el número de excursiones y reiniciar la oferta de la "Vuelta a Cuba" para los próximos meses de julio y agosto.

El turismo es el segundo motor generador de divisas para la economía cubana, detrás de los servicios técnicos y profesionales, y sus ingresos se calculan en 2.500 millones de dólares anuales.

Por tal motivo, las autoridades del sector tienen el compromiso de mejorar a Cuba como producto turístico, en materia de calidad y diversidad, con vistas a responder y satisfacer los gustos más exigentes de los viajeros.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

