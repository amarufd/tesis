
Diario demanda a Estados Unidos por negar datos sobre antiterroristas cubanos



El periódico estadounidense Liberation demandó al Departamento de Estado por prohibir el acceso a información necesaria para abordar el caso de antiterroristas cubanos condenados y encarcelados en el país norteño, informó la abogada Mara Verheyden-Hilliard.

La jurista, directora ejecutiva del Fondo para la Asociación de la Justicia Civil, explicó en rueda de prensa que el recurso legal se interpuso al amparo del derecho a la información y busca la entrega de datos relevantes sobre cómo el Gobierno pagó a varios reporteros de Miami para crear un clima hostil contra esos hombres.

Entre 1998 y 2001, la población de esa ciudad del estado de Florida recibió a través de la prensa escrita, radial y televisiva un flujo de propaganda adversa para persuadir al jurado e interferir en el proceso legal de Gerardo Hernández, Ramón Labañino, Fernando González, Antonio Guerrero y René González.

Ellos son conocidos internacionalmente como Los Cinco y se les impuso severas sanciones en 2001 porque vigilaron grupos violentos radicados en Miami, desde donde organizan actos como los que en los últimos 53 años ocasionaron más de tres mil 400 víctimas en Cuba.

René González salió de prisión en octubre de 2011 tras cumplir su condena, pero luego fue sometido a tres años de libertad supervisada y solo regresó a la nación caribeña en abril último porque renunció a la ciudadanía estadounidense.

"Se han negado a entregar los documentos de antes de noviembre de 1999 con el argumento que hasta esa fecha Radio y Televisión Martí pertenencían a la Agencia de Información de Estados Unidos", dijo Verheyden-Hilliard, al precisar que tampoco autorizaron los registros de otras fechas.

La abogada ofreció la conferencia de prensa como parte de una jornada mundial de solidaridad con Los Cinco que sesionó en Washington del 30 de mayo al 5 de junio pasado.

Ese evento reunió en la capital norteamericana a parlamentarios, intelectuales y activistas de Argentina, Barbados, Bélgica, Brasil, Chile, Ecuador, El Salvador, Reino Unido, Francia, Alemania, Grecia, Haití, Italia, México, España, Suecia y Venezuela, entre otros países.

Su programa incluyó conferencias, una protesta pacífica frente a la Casa Blanca, un servicio ecuménico, presentaciones de libros y una muestra de 15 pinturas en acuarela de Antonio Guerrero.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

