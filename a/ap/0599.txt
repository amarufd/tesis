
Vietnam rechaza informe de Estados Unidos sobre derechos humanos



Vietnam rechazó el informe de derechos humanos 2012, presentado por el Departamento norteamericano de Estado, y en particular la parte referente a esta nación indochina, calificándolo de basado en informaciones erróneas y carentes de objetividad.

Al responder a la prensa el último domingo sobre la reacción oficial de Hanoi ante ese reporte, el vocero de la Cancillería, Luong Thanh Nghi, acotó que a pesar de reconocer logros vietnamitas en la garantía de los derechos humanos, el texto sigue pendiente de valoraciones unilaterales que no reflejan la situación real del país.

El vocero reiteró la política del Estado vietnamita de respeto y promoción de los derechos básicos de los ciudadanos y el respaldo del cumplimiento de los mismos en el marco de la ley.

Ese acto perjudica las relaciones entre ambos países, subrayó el diplomático tras precisar que Hanoi y Washington mantienen de forma regular el mecanismo de intercambio en el sector de derechos humanos.

Esperamos que mediante los diálogos francos y abiertos, incluido el reciente del pasado día 12, ambas partes tienen visiones objetivas y reales sobre el tema en cada país, en aras del fomento de la comprensión mutua y los nexos bilaterales.Haga click aquí para recibir gratis Argenpress en su correo electrónico.

