
Argentina, Tucumán: La Gaceta se niega a cerrar un acuerdo con sus trabajadores



Ante la negativa de la empresa de cerrar un incremento salarial del 28, los trabajadores de La Gaceta mudaron su malestar a la calle. Protestaron en la peatonal de calle Mendoza.

“La patronal ofrece un aumento del 25 al básico, y nosotros pedimos 35 entre el básico y sumas no remunerativas. Además, hay muchas situaciones irregulares en el diario que hemos revelado con la Secretaria de Trabajo, como la tercerización en la sección de página web y el incumplimiento del Convenio Colectivo de Trabajo, respecto al descanso de dos días por semana”, explicó Oscar Gijena, secretario General de la Asociación de Prensa de Tucumán (APT), en declaraciones a Radio Prensa.

"Entendemos que no es cuestión de dinero sino de una actitud de la empresa de amedrentar y someter a sus empleados", se quejó el dirigente sindical, a la vez que indicó que "desde hace tiempo que La Gaceta intenta dividir a sus trabajadores y realizar acuerdos por separado".

Ante esta situación, en asamblea, los trabajadores del diario junto a directivos de la Asociación de Prensa de Tucumán decidieron sacar el conflicto salarial a la calle y protestaron en las puertas del diario. La asamblea continuará en estado de alerta y movilización hasta que la patronal mejore los salarios. No se descartan nuevas medidas.
Haga click aquí para recibir gratis Argenpress en su correo electrónico.

