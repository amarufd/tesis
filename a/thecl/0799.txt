 Estás usando un Navegador desactualizado. Por favor, actualízalo a una  versión más moderna y podrás ver este y otros sitios como realmente fueron pensados. [X]
    1
                    Comentarios
                

El coordinador programático del comando de Michelle Bachelet, Álvaro Arenas, explicó este jueves que la propuesta que fue dada a conocer por la ex mandataria es una reforma “prudente, cautelosa y gradual”.
La propuesta que fue presentada por la ahora precandidata presidencial, cuya pretensión es recaudar en cuatro años un 3 del PIB, se basa en cuatro objetivos prioritarios que son aumentar la carga tributaria; mejorar la distribución del ingreso; introducir mecanismo de incentivos al ahorro e inversión y tener un mecanismo para disminuir la evasión y la elusión.
La propuesta que considera en su análisis, posibles factores que afectan la inversión en Chile, puso de relieve la falta de cohesión social, el problema de credibilidad en las instituciones públicas, y también el acceso y competitividad de los mercados. Al respecto el vocero del área programática, destacó que las condiciones macroeconómicas del país, no justifican la mantención de una política de déficit estructural por tanto tiempo.
“Cuando uno piensa en la economía sustentable, en la economía de mediano plazo, piensa que está ocurriendo en los factores de largo plazo, y básicamente uno puede pensar en función de las estimaciones, es que efectivamente se ven buenas luces en la economía internacional 2014 mediante, y en ese sentido es que entendemos que aquí hay un espacio muy concreto para avanzar en una reforma tributaria de verdad, en forma prudente, cautelosa, gradual, pero que Chile necesita”, afirmó el ex director de presupuesto durante el gobierno de Bachelet.
Arenas agregó que “lo que es importante hoy día son los ingresos permanentes, nuestra economía y la política fiscal en Chile se basa en el marco del balance estructural, y en este marco lo que importa son los ingresos permanentes con que cuenta Chile que son los que definen la capacidad de gasto que tiene el Estado y por eso hemos sido muy precisos en decir, en régimen esta Reforma tributaria va a generar un estimado de 3 del PIB”.
Respecto de la eliminación del FUT, el coordinador programático aclaró que “en relación al FUT histórico, no modifica las condiciones por las cuales hoy día opera, sino que efectivamente realizándose los retiros efectivos de las empresas, se generaran los pagos respectivos tal como funciona hoy día. En ese sentido el tratamiento histórico del FUT no tiene variaciones, la innovación está en cerrar la puerta al cuarto año a un flujo”.
Arenas enfatizó que “no hay a mi juicio, condiciones macroeconómicas, en términos de lo que ha sido el crecimiento de la economía, en términos del precio a largo plazo del cobre, en términos del precio efectivo del cobre, para haber mantenido una política de déficit estructural por tan largo plazo, no es sustentable en las cuentas fiscales seguir manteniendo esto”.
Finalmente Álvaro Arenas informó que la propuesta no consideró el impuesto específico a los combustibles, ni tampoco igualar el diesel con las bencinas, aclarando que “es una materia que no está considerada en la propuesta, nuestra propuesta considera dos áreas de intervención respecto de quienes contaminan más, paguen más”.
          Peos y peos... este ladrón (avalado por la ladrona mayor) acaba de decir que el impuesto también va para las pymes... En fin, cuando un ladrón se viene, la desaceleración tb.
                     

		


               
                    Para comentar necesitas estar logueado. Ingresa con:
                    
                    	
				
							
										
														Enter your WordPress.com blog URL
		
			http://.wordpress.com 
			Proceed
		
	“Le digo al rector que no confunda las cosas; una es autonomía universitaria y otra es pretender que la universidad sea santuario de delincuentes”
