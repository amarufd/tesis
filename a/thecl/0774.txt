 Estás usando un Navegador desactualizado. Por favor, actualízalo a una  versión más moderna y podrás ver este y otros sitios como realmente fueron pensados. [X]
    9
                    Comentarios
                
El precandidato presidencial del PRO, Marco Enríquez-Ominami, se refirió este martes a la polémica por las tomas de establecimientos educacionales que son sedes de votación para las primarias de este 30 de junio, resaltando que “escuchar las demandas estudiantiles es también parte esencial de una democracia”.
“Defiendo las demandas estudiantiles, de hecho muchas de ellas son parte importante de nuestro programa de gobierno, y defiendo también la democracia y la realización de las primarias”, argumentó el abanderado del Partido Progresista.
Enríquez-Ominami, sostuvo que “estas tomas son el síntoma de la enfermedad, tal como las marchas por la Alameda y en las principales ciudades del país. Difícilmente la derecha o la Concertación pueden curar la enfermedad, ya que no creen en la educación pública, gratuita y de calidad y porque participan del negocio de la educación y lucran con ella”.
“Ayer fueron las marchas, hoy son las tomas de los colegios que son recintos de votación. Es por esto que debemos ir al fondo, que es reconocer la educación como un derecho y no como un producto. Escuchar las demandas estudiantiles es también parte esencial de una democracia”, enfatizó el aspirante a La Moneda que es apoyado también por el Partido Liberal de Chile y el Movimiento del Socialismo Allendista.
          Hay un cuento muy conocido en el que un rey es convencido por sus criados de cámara que iba vestido con los trajes más elegantes, cuando en realidad salió en pelotas sobre su caballo. Lo mismo te pasa a ti Marco, andai puro escapando, tai guatón ya no tenís brillo, tai en pelotas. Todo lo que dices suena hueco y ridículo. Mejor dedícate a hacer cine, porque con un par de pelis más como "Mansacue" quedarías al nivel del mejor perpetrador de bodrios del cine nacional en toda su historia....
                     



            
                                Catapilco MeadoJun, 25
            
           Porque difícil que se haga de nuevo cine tan malo. Algo es algo.
                     



            
                                Quique EspinozaJun, 25
            
           Catapico, te estoy sacando la película. Tu odio por MEO ya muestra tintes amorosos, me da la impresión que estuviste enamorado de él y como no te pescó, ahora despotricas las 24 hrs en su contra. Recuerda que el odio no es otra cosa que despecho en contra de alguien al que quisiste....Maldiciones Catapilco despechado
                     





            
                                D.CabreraJun, 25
            
           Amermelado….hoy, las famosas tomas estudiantiles, son solo una herramienta política. Porque los fines e ideales estan escondidos bajo el anarquismo de estos movimientos que sobresalen por los desórdenes y desafíos a la autoridad y no por sus planteamientos. Sus ideas estan siendo eclipsadas por el violentismo y el desacato. Las demandas estudiantiles hace rato que no se escuchan. Solo actos de tomas y desorden….sus fundamentos yacen escondidos quien sabe dónde. ¿Tu sabes pendejo cuales...
                     



            
                                D.CabreraJun, 25
            
           son la gran diferencia entre lo que plantea la izquierda bacheletista y la derecha?...la gran y única diferencia es que la derecha no quiere gastarse 2.000 millones de dólares en pagar la educación del 20 más rico del país y Bachelet si quiere hacerlo.. esa es la diferencia de la educación gratis para todos que propone la gorda. Entonces, no serán estas marchas falsas demostraciones de algo que esconde solo presión política y no una real muestra de planteamientos?....bendiciones
                     





            
                                pypoJun, 25
            
           Educar a los hijos de los ricos significa gastar 50 sacos de cobre al dia algo asi como 3.200 millones de pesos al año.Eso es lo que defiende la derecha no quiere regalarle 50 sacos de cobre a los ricos que estudian.Pero la derecha nunca a querido cobrarle impuestos a los cupriferos los tiene libres de ellos hasta el 2.020 y eso significa que la derecha le regala MILLONES DE SACOS DE COBRE AL DIA a los empresarios RICOS EXTRANJEROS. Esa es la diferencia ente la alianza y la concerta.La derecha...
                     



            
                                pypoJun, 25
            
           defiende 50 sacos y regala millones de sacos de cobre al dia.La concerta que siempre a querido cobrarle impuestos a los cupriferos quiere que a chile le lleguen MILLONES DE SACOS DE CCOBRE AL DIA y de esos gastar solo 50 sacos de cobre al dia para pagarle la educacion a los hijos de los ricos pero quedaria un sobrante de miedo pa seguir educando gratis a los estudiantes del resto del mundo y todavia sobraria para dar salud gratis a todos los chilenos metiendo al AUGE todas las enfermedades que...
                     



            
                                pypoJun, 25
            
           existen y las que estan por aparecer y no solo  80 enfermedades que hoy estan el auge y que se pagan con el poco cobre que es de chile y que lo extrae CODELCO.
                     



            
                                ZocotrocotrocoJun, 25
            
           Una cosa es escucharlos, cosa que todos han hecho, y otra cosa es hacerles caso.
                     

		


               
                    Para comentar necesitas estar logueado. Ingresa con:
                    
                    	
				
							
										
														Enter your WordPress.com blog URL
		
			http://.wordpress.com 
			Proceed
		
	“Le digo al rector que no confunda las cosas; una es autonomía universitaria y otra es pretender que la universidad sea santuario de delincuentes”
